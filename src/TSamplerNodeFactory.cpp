#include "TSamplerNodeFactory.hpp"
#include <cassert>
#include <algorithm>

using std::cout; using std::endl;

template <class Num_Type>
SamplerNodeFactory<Num_Type>* SamplerNodeFactory<Num_Type>::getFactory(uint32_t memLimit){
	static SamplerNodeFactory<Num_Type> instance;
	return &instance;
}

template <class Num_Type>
void SamplerNodeFactory<Num_Type>::forgetAllUsedNodes(){
	usedNodes.clear();
}

template <class Num_Type>
void SamplerNodeFactory<Num_Type>::recycleAllUsedNodes(){
	freeNodes.insert(freeNodes.end(),usedNodes.begin(),usedNodes.end());
	usedNodes.clear();
}

template <class Num_Type>
void SamplerNodeFactory<Num_Type>::recycleSamplerNode(SamplerNode<Num_Type>* node){
	usedNodes.erase(node);
	freeNodes.push_back(node);
}

template <class Num_Type>
SamplerNode<Num_Type>* SamplerNodeFactory<Num_Type>::createRoot(uint32_t cmprsdLvl){
	SamplerNode<Num_Type>* tmp = getFreeNode();
	if(tmp!=NULL){
		tmp->parents.clear(); tmp->thenElse.clear();
		
		tmp->compressedLevel = cmprsdLvl;
		tmp->rootPathCount = pow(2,cmprsdLvl);
	} else{
		tmp = new SamplerNode<Num_Type>(pow(2,cmprsdLvl),cmprsdLvl);
	}
	usedNodes.insert(tmp);
	return tmp;
}

template <class Num_Type>
SamplerNode<Num_Type>* SamplerNodeFactory<Num_Type>::createChild(SamplerNode<Num_Type>* parent, bool tE, uint32_t lvlDiff, uint32_t cmprsdLvl){
	SamplerNode<Num_Type>* tmp = getFreeNode();
	if(tmp!=NULL){
		tmp->parents.clear(); tmp->thenElse.clear();
		
		tmp->parents.push_back(parent); tmp->thenElse.push_back(tE);
		tmp->compressedLevel = cmprsdLvl;
		tmp->rootPathCount = parent->rootPathCount * pow(2,lvlDiff);
	} else{
		tmp = new SamplerNode<Num_Type>(parent,parent->rootPathCount*pow(2,lvlDiff),cmprsdLvl,tE);
	}
	usedNodes.insert(tmp);
	return tmp;
}

template <class Num_Type>
SamplerNode<Num_Type>* SamplerNodeFactory<Num_Type>::copy(SamplerNode<Num_Type>* other){
	SamplerNode<Num_Type>* tmp = getFreeNode();
	if(tmp!=NULL){
		tmp->parents=other->parents; tmp->thenElse=other->thenElse;
		
		tmp->compressedLevel = other->compressedLevel;
		tmp->rootPathCount = other->rootPathCount;
	} else{
		tmp = new SamplerNode<Num_Type>(other->parents,other->rootPathCount,other->compressedLevel,other->thenElse);
	}
	usedNodes.insert(tmp);
	return tmp;
}

template <class Num_Type>
SamplerNode<Num_Type>* SamplerNodeFactory<Num_Type>::getFreeNode(){
	if (freeNodes.empty()) return NULL;
	else {
		SamplerNode<Num_Type>* front = freeNodes.front();
		freeNodes.pop_front();
		return front;
	}
}

template <class Num_Type>
void SamplerNodeFactory<Num_Type>::clearSentinel(uint32_t cmprsdLvl){
	sentinel.parents.clear(); sentinel.thenElse.clear();
	sentinel.rootPathCount = 0;
	sentinel.compressedLevel = cmprsdLvl;
}

template <class Num_Type>
void SamplerNodeFactory<Num_Type>::ensureSamplingArraySize(uint32_t minSize){
	uint32_t currSize = samplingArray.size();
	while(currSize<minSize){
		samplingArray.push_back(1);
		//sA2.push_back(0); 
		currSize++;
	}
}

template <class Num_Type>
std::pair<std::pair<SamplerNode<Num_Type>*,bool>,uint32_t> SamplerNodeFactory<Num_Type>::sampleParent(SamplerNode<Num_Type>* nodeSN, vector<double_t> weights, RandomBits *rb){
	ensureSamplingArraySize(nodeSN->parents.size());
	runningTotal = 0;
	// double rT = 0;
	{ //braces to limit definition of i. i might be used later in loops and don't want double definition
		uint32_t i = 0;
		for (auto& parent : nodeSN->parents){
			samplingArray[i] = parent->rootPathCount;
			// sA2[i] = ((DoubleNumType*)parent->rootPathCount)->number;
			if(!weights.empty()) samplingArray[i] *= weights.at(i);
			// if(!weights.empty()) sA2[i] *= (weights.at(i));
			assert(samplingArray[i]>0);
			int levelDiff = nodeSN->compressedLevel-parent->compressedLevel;
			// printf("levels: %u %u levelDiff: %d\n",nodeSN->compressedLevel,parent->compressedLevel,levelDiff);
			assert(levelDiff>=1);
			if ( levelDiff >= 2){
				samplingArray[i] *= pow(2,levelDiff-1);
				// sA2[i] *= pow(2,levelDiff-1);  
				//-1 because we want to weight eachnode at level just above currNode ('parent' in diag below) 
				//by the number of paths it feeds into the currNode.
				//So if diff in levels is 2 (between 'grandparent' and 'currNode' below),
				//then multiplier should be 2^1, if diff is 3 then multiplier should be 2^2 and so on
				// grandparent -> parent => currNode   -> means one outedge, => means both outedges
			}
			runningTotal += samplingArray[i];
			// rT += sA2[i];
			samplingArray[i] = runningTotal;
			// sA2[i] = rT;
			//samplingArray[i]->print(); cout<<" "<<std::flush;
			i++;
		}
	}
	r = runningTotal*rb->getRandReal(std::uniform_real_distribution<Num_Type>(0,1));
	
	//lower_bound returns position of first element in array that is greater than or equal to r
	typename vector<Num_Type>::iterator pos = std::lower_bound(samplingArray.begin(),samplingArray.begin()+nodeSN->parents.size(),r);
	// vector<double>::iterator pos = std::lower_bound(sA2.begin(),sA2.begin()+nodeSN->parents.size(),rn);
	// uint32_t low = pos - sA2.begin();
	uint32_t low = pos - samplingArray.begin();
	//assert(nodeSN->parents.size()==nodeSN->thenElse.size());
	return std::make_pair(std::make_pair(nodeSN->parents.at(low),nodeSN->thenElse.at(low)),low);
}

template <class Num_Type>
bool SamplerNodeFactory<Num_Type>::noUsedNodes(){
	return usedNodes.empty();
}

template <class Num_Type>
SamplerNodeFactory<Num_Type>::SamplerNodeFactory(): sentinel(0,1){
	runningTotal = 0;
	r = 0;
}