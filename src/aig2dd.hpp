#ifndef AIG2DD_H_
#define AIG2DD_H_

#include <iostream>
#include <vector>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include "mtr/mtr.h"
#include "mtr/mtrInt.h"
#include "cuddObj.hh"
extern "C"{
	#include "aiger.h"
	#include "dddmp.h"
	#include "dddmpInt.h"
}
using std::vector;
using std::string;
using std::unordered_set;
using std::unordered_map;
class DDWrapper{
	public:
		virtual DDWrapper* Complement() = 0;
		virtual DDWrapper* And(DDWrapper*) = 0;
		virtual DDWrapper* ExistAbstract(DDWrapper*) = 0;
		virtual void* getNode()=0;
		virtual void printStats(uint8_t verbosity) = 0;
		virtual int Traverse(vector<bool>,vector<bool>,vector<bool>, int, int) = 0;
};

class ADDWrapper : public DDWrapper{
	public:
		ADDWrapper(ADD node_): node(node_){}
		//ADDWrapper(): node(){}
		ADDWrapper* Complement() override;
		ADDWrapper* And(DDWrapper*) override;
		ADDWrapper* ExistAbstract(DDWrapper*) override;
		void* getNode() override;
		void printStats(uint8_t verbosity) override;
		int Traverse(vector<bool>,vector<bool>,vector<bool>, int, int) override;
		ADD node;
};

class BDDWrapper : public DDWrapper{
	public:
		BDDWrapper(BDD node_): node(node_){}
		//BDDWrapper(): node(){}
		BDDWrapper* Complement() override;
		BDDWrapper* And(DDWrapper*) override;
		BDDWrapper* ExistAbstract(DDWrapper*) override;
		void* getNode() override;
		void printStats(uint8_t verbosity) override;
		int Traverse(vector<bool>,vector<bool>,vector<bool>, int,int) override;
		BDD node;
};

class CNFLit;
class CNFManager{
	public:
		CNFManager(){maxVar=0;}
		CNFLit getFalse();
		CNFLit getTrue();
		bool isTrue(CNFLit c);
		bool isFalse(CNFLit c);
		//No getTrue. True is to be represented as NULL
		CNFLit getPosLit(uint32_t, uint32_t);
		CNFLit AND(CNFLit, CNFLit);
		std::pair<CNFLit,CNFLit> getANDChildren(CNFLit parent);
		uint32_t getNumOriginalVars();

		static const uint32_t OPI = 0;
		static const uint32_t CURRSTATEVAR = 1;
		static const uint32_t NEXTSTATEVAR = 2;
		int32_t maxVar;
	// private:
		unordered_map<uint32_t,uint32_t> varMap;//if var is auxvar then maps to first clause where it is defined. otherwise
		// it maps to the type (opi, currstate,nextstate) defined by the consts
		vector<vector<int32_t>> clauses;
		unordered_set<uint32_t> auxVars;
		unordered_set<uint32_t> OPIs;
		unordered_set<uint32_t> currStateVars;
		unordered_set<uint32_t> nextStateVars;
		unordered_map<uint32_t, bool> auxVals;
		// uint32_t numNonAuxVars = 0;
};


//the bool onezero is only used for the special case of true and false literals. For actual variables, the sign of varnum
//determines whether the literal is uncomplemented or complemented
typedef struct CNFLit{
	CNFLit(int32_t varNum_, CNFManager* cnfMgr_){
		assert(varNum_!=0);
		varNum=varNum_;
		cnfMgr = cnfMgr_;
	}
	CNFLit(bool oneZero_, CNFManager* cnfMgr_){
		varNum = 0;
		oneZero = oneZero_;
		cnfMgr = cnfMgr_;
	}
	CNFLit(int32_t varNum_, bool oneZero_, CNFManager* cnfMgr_){
		varNum = varNum_;
		oneZero = oneZero_;
		cnfMgr = cnfMgr_;
	}
	CNFLit operator -(){
		CNFLit c(-varNum,!oneZero,cnfMgr);//if varnum is nonzero then onezero will be ignored anyway. so safe
		return c;
	}
	CNFLit AND(CNFLit other){
		// std::cout<<"Calling AND on CNFLit with manager address:"<<cnfMgr<<std::endl;
		return cnfMgr->AND(*this,other);
	}
	bool oneZero; // need to represent constants true and false. This is done with this bit, and setting node to zero
	//onezero is ignored if node!=0
	int32_t varNum;
	CNFManager* cnfMgr;
}CNFLit;
class CNFWrapper : public DDWrapper{
	public:
		CNFWrapper(CNFLit node_): node(node_){}
		CNFWrapper(CNFWrapper* other):node(other->node){} 
		CNFWrapper* Complement() override;
		CNFWrapper* And(DDWrapper*) override;
		CNFWrapper* ExistAbstract(DDWrapper*) override;
		void* getNode() override;
		void printStats(uint8_t verbosity) override;
		int store(char* outFile,bool setState0, uint32_t traceLength, vector<string> comments);
		int Traverse(vector<bool>,vector<bool>,vector<bool>, int,int) override;
		CNFLit node;
};

class Aig2dd{
	public:
		Aig2dd(char* inFile_, char* outFile_, char* checkFile_, Cudd_ReorderingType reoType_, uint32_t outputType_,
		bool setState0_, uint32_t traceLength_,int contiguousAll, uint8_t interleaved_, double_t startTime);
		void* aig2dd();
		void sanity_check(DDWrapper*);
	private:
		DdNode* aig2bdd_rec(aiger_and*);
		unsigned lit2andInd(unsigned lit);
		void aig2dd_rec(aiger_and* andNode);
		DDWrapper* parseAndGateIns(unsigned ilit, unsigned short* iRefFlag);
		void printVecs(vector<bool>,vector<bool>,vector<bool>, int, int);
		int* getInterleavedOrder(uint32_t,uint32_t);
		char* inFile, *outFile, *checkFile;
		Cudd_ReorderingType reoType; 
		uint32_t outputType;
		//ABDD class which is parent of bdd and add doesnt contain right abstractions of common functions
		// such as bddAnd and addTimes. So creating separate vectors instead
		//vector<ADD>* addHash; 
		//vector<BDD>* bddHash;
		vector<DDWrapper*> vars;
		vector<DDWrapper*> nodeHash;
		vector<std::pair<std::pair<uint32_t,uint32_t>,uint32_t>> contiguityRanges;
		DDWrapper* zero, *one;
		aiger* a;
		Cudd mgr; CNFManager cnfMgr;
		DdManager* dd;
		bool setState0; // only for CNF formulas
		uint32_t traceLength; // only for CNF formulas. Unrolling DDs will be done in sample_trace
		uint8_t interleaved;
		double_t startTime, readAigTime, convTime, sanCheckTime, abstrTime, writeTime;
		vector<string> comments;
		uint32_t nodesFilled, nodeHashSize;
};
#endif