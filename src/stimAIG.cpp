/***************************************************************************
Copyright (c) 2019, Aditya A. Shrotri, Rice University, USA.
Copyright (c) 2006-2011, Armin Biere, Johannes Kepler University, Austria.
Copyright (c) 2011, Siert Wieringa, Aalto University, Finland.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/

#include "stimAIG.hpp"
#ifndef DFAST
	#define DFAST 1
#endif

StimAig::StimAig(char* fileName){
	model = aiger_init();
	error = aiger_open_and_read_from_file (model, fileName);
	if (error){
		printf("Error reading AIG model from file. %s. Exiting..\n", error);
		exit(1);
	}
	current = (unsigned char*)calloc (model->maxvar + 1, sizeof (current[0]));
	aiger_reencode (model);	/* otherwise simulation incorrect */
}

StimAig::~StimAig(){
	free (current);
	aiger_reset (model);
}

bool StimAig::deref (unsigned lit){
	bool res = current[aiger_lit2var (lit)];
	res ^= aiger_sign (lit);
	#if DFAST < 2
		if (lit == 0)
			assert (res == 0);
		if (lit == 1)
			assert (res == 1);
	#endif
	return res;
}

/* the function can take vectors of type unsigned char as input, which makes it easier to read from stdin.
   aigsim's code (adopted here) was written so as to be able to handle 0/1 chars and not just bools. But to 
   prevent ambiguity, we explicitly use vector<bool>. Note that there are problems with vector<bool> since its
   packed representation means that it is not a traditional container, and so it is not possible to use the 
   address of an element in the vector. However, in this function we do not use addresses and so it works fine.
   Calling function will have to be careful with using vector<bool>
*/
void StimAig::stimulate(const vector<bool>& inputVec, const vector<bool>& stateVec, vector<bool>& resL, vector<bool>& resO){
	assert(resL.size()==model->num_latches);
	assert(resO.size()==model->num_outputs);
	/* Set CIs */
	assert(inputVec.size()== model->num_inputs);
	assert(stateVec.size()== model->num_latches);
	
	unsigned j, l, r; bool tmp;
	for ( j = 0; j < model->num_latches; j++) {
		aiger_symbol *symbol = model->latches + j;
		current[symbol->lit/2] = stateVec[j];
	}
	for (j = 1; j <= model->num_inputs; j++){
		current[j] = inputVec[j-1];
	}	
	/* Simulate AND nodes.*/
	aiger_and* andss;
	for (j = 0; j < model->num_ands; j++){
		andss = model->ands + j;
		l = deref (andss->rhs0);
		r = deref (andss->rhs1);
		tmp = l & r;
		tmp |= l & (r << 1);
		tmp |= r & (l << 1);
		current[andss->lhs / 2] = tmp;
	}
	/* Then calculate next state values of latches in  parallel.*/
	for (j = 0; j < model->num_latches; j++){
		aiger_symbol *symbol = model->latches + j;
		resL[j] = deref (symbol->next);
	}
	/*Calculate outputs*/
	for (j = 0; j < model->num_outputs; j++){
		aiger_symbol *symbol = model->outputs + j;
		resO[j] = deref (symbol->next);
	}
}

int StimAig::getNumInputs(){
	return model->num_inputs;
}

int StimAig::getNumLatches(){
	return model->num_latches;
}

int StimAig::getNumPOs(){
	return model->num_outputs;
}