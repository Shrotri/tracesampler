#ifndef UTILS_H_
#define UTILS_H_

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>

#include "cudd.h"
#include "dddmp.h"
#include "dddmpInt.h"
#include "Timers.h"
typedef struct dddmpVarInfo {
	/*
	*  Local Information
	*/

	int nDdVars;       /* Local Manager Number of Variables */
	char **rootNames;  /* Root Names */

	/*
	*  Header File Information
	*/

	Dddmp_DecompType ddType;

	int nVars;      /* File Manager Number of Variables */
	int nSuppVars;  /* File Structure Number of Variables */

	int varNamesFlagUpdate;      /* 0 to NOT Update */
	char **suppVarNames;
	char **orderedVarNames;

	int varIdsFlagUpdate;        /* 0 to NOT Update */
	int *varIds;                 /* File ids - nSuppVars size */
	int *varIdsAll;              /* ALL ids - nVars size */

	int varComposeIdsFlagUpdate; /* 0 to NOT Update */
	int *varComposeIds;          /* File permids - nSuppVars size */
	int *varComposeIdsAll;       /* ALL permids - nVars size */

	int varAuxIdsFlagUpdate;     /* 0 to NOT Update */
	int *varAuxIds;              /* File auxids - nSuppVars size */
	int *varAuxIdsAll;           /* ALL auxids  - nVars size */

	int nRoots;
} dddmpVarInfo_t;

#define STATE_SEQ_10 "10"
#define STATE_SEQ_01 "01"
using std::string;
using std::vector;

void readDD(char* ddFileName, char* ddMetaFileName, DdManager** man, DdNode** dd, uint32_t& numLatches, 
	uint32_t& numInputs, uint32_t& nStateVars, uint32_t& varStartIndex, bool* useContiguity, 
	string* stateSeq);
void writeDDToFile(DdManager *ddMgr, char *ddname, DdNode *f, Dddmp_DecompType ddType, char **varnames, int *auxids, 
	int mode, Dddmp_VarInfoType varinfo, char *outFile, uint32_t ni, uint32_t nl, const char* stateSequence, 
	vector<std::pair<std::pair<uint32_t,uint32_t>,uint32_t>>& contiguityRanges, vector<string> comments);

DdNode* getAllZeroBDDState(DdManager*, DdNode** sVars, uint32_t nStateVars);
DdNode* makeCube(uint32_t i, DdManager* dd, DdNode** varList, uint32_t n);

void convertToADD(DdManager* man, DdNode** bdd, bool verbose);
void switchStates(DdManager* man, DdNode** dd, Dddmp_DecompType ddType, DdNode** sVars, uint32_t nStateVars, 
	uint32_t varStartIndex);

DdNode* computeReachableSet(DdManager* dd, DdNode* bdd, DdNode* initialSet, DdNode* currStateVarsCube, 
	DdNode** sVars, uint32_t nStateVars, uint32_t nSteps, uint32_t maxDepth);
DdNode** computeExactReachableSets(DdManager* dd, DdNode** bdd, DdNode* initialSet, DdNode** sVars, 
	uint32_t nStateVars, uint32_t nSteps, uint32_t maxDepth, bool useUptoNSet=false);

DdNode* restrictS0ToReachableStates(DdManager* dd, DdNode** bdd, DdNode* initialSet, DdNode** sVars, 
	uint32_t nStateVars, uint32_t nSteps, uint32_t maxDepth, bool returnReachSet);
DdNode* restrictS0ORS1ToReachableStates(DdManager* dd, DdNode** bdd, DdNode* initialSet, DdNode** sVars, 
	uint32_t nStateVars, uint32_t nSteps, uint32_t maxDepth, bool returnReachSet);
DdNode* andS0ToReachableStates(DdManager* dd, DdNode** bdd, DdNode* initialSet, DdNode** sVars, 
	uint32_t nStateVars, uint32_t nSteps, uint32_t maxDepth, bool returnReachSet);


void printPerm(DdManager*);
void printDoubleLine();
string printTimeTaken(string forWhat, double_t timeTaken, uint8_t numNewLines);

void dumpDot(DdManager *dd,DdNode *f, string fname);
int Cudd_StringSummary(DdManager * dd, DdNode * f, int n, /**< number of variables for minterm computation */
  string* out /**pointer to string that will contain output*/);
#endif