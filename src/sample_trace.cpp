#include <sys/types.h>
#include <sys/time.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <random>
#include <mtr/mtr.h>
#include <mtr/mtrInt.h>

using std::ifstream;
using std::stoi;
using std::cout;
using std::endl;
using std::stringstream;
using std::sort;
#include "sample_trace.hpp"
#include "utils.hpp"
#include "cuddObj.hh"

#ifndef DFAST
	#define DFAST 1
#endif

using namespace ST;

void State::set(bool bit, uint32_t i){
	assert(bits.at(i)==-1); //sampling is generally not the bottleneck so leaving asserts in place
	bits[i] = bit;
}
void State::setNoCheck(bool bit, uint32_t i){
	printf("\nSet called on state %p at pos %d!\n",this,i);
	bits[i] = bit;
}

void State::printState(){
	cout<<"State "<<this<<":";
	for(uint32_t i = 0; i<bits.size();i++){
		printf("%d",bits[i]);
	}
}

bool State::get(uint32_t i){
	assert(bits.at(i)!=-1); //sampling is generally not the bottleneck so leaving asserts in place
	//printf("%d ", bits[i]);
	return bits[i]==1;
}
void State::reset(){
	for (uint32_t i = 0; i<bits.size();i++){
		bits[i] = -1;
	}
}

State* Trace::getState(uint32_t i){
	return &states.at(i);
}

void Trace::clear(){
	for (uint32_t i = 0; i<numStates; i++){
		states[i].reset();
	}
}

TraceSampler::TraceSampler(char* ddFileName_, char* ddMetaFileName_, uint32_t traceLength_, 
	bool checkSamples_, int32_t useReachableSet_, double_t startTime_): ddFileName(ddFileName_), ddMetaFileName(ddMetaFileName_), 
	traceLength(traceLength_), checkSamples(checkSamples_), useReachableSet(useReachableSet_), startTime(startTime_) {
	
	//calculate n from tracelength
	assert(traceLength>=2);
	n = ceil(log2(traceLength));
	adds.resize(n+1);
	if(pow(2,n)>traceLength){
		notPowerOf2 = true;
	}
	rb.SeedEngine();
	rb.SeedEngine2();
	snFactory = SamplerNodeFactory::getFactory(0,SamplerNodeFactory::dbl);
}

void TraceSampler::loopAfterN(){
	ADD& a = adds[0];
	/* Create variable sets */
	
	cout<<"Adding counter to system, since tracelength is not a power of two. Doing Step: 1 "<<std::flush;
	DdNode** curVars = new DdNode*[n];
	DdNode** nexVars = new DdNode*[n];
	DdNode** s0Vars = new DdNode*[2*nStateVars+2*n];
	for(uint32_t i = 0; i<n; i++){
		curVars[i] = Cudd_bddNewVar(a.dd);
		assert(Cudd_NodeReadIndex(curVars[i])==i+varStartIndex+2*nStateVars);
	}
	for(uint32_t i = 0; i<n; i++){
		nexVars[i] = Cudd_bddNewVar(a.dd);
		assert(Cudd_NodeReadIndex(nexVars[i])==i+varStartIndex+2*nStateVars+n);
	}
	for(uint32_t i = 0; i<2*nStateVars+2*n; i++){
		s0Vars[i] = Cudd_bddIthVar(a.dd,i+varStartIndex);
		assert(Cudd_NodeReadIndex(s0Vars[i])==i+varStartIndex);
	}
	cout<<"2 "<<std::flush;
	/* Add transitions i-1 => i up to N to counter */
	DdNode *temp, *temp2, *cntr = Cudd_ReadLogicZero(a.dd);
	Cudd_Ref(cntr);
	for(uint32_t i = 1; i<=traceLength; i++){
		DdNode* cur = makeCube(i-1,a.dd,curVars,n);
		DdNode* nex = makeCube(i,a.dd,nexVars,n);

		DdNode* trans = Cudd_bddAnd(a.dd,cur,nex);
		Cudd_Ref(trans); Cudd_RecursiveDeref(a.dd,cur); Cudd_RecursiveDeref(a.dd,nex);
		
		cntr = Cudd_bddOr(a.dd,cur = cntr,trans);
		Cudd_Ref(cntr); Cudd_RecursiveDeref(a.dd,cur); Cudd_RecursiveDeref(a.dd,trans);
	}

	//dumpDot(a.dd,cntr,"scratch/cntr.dot");
	//dumpDot(a.dd,a.add,string("scratch/orig.dot"));
	//cout<<"a.add summary:\n";
	//Cudd_PrintSummary(a.dd,a.add,2*nStateVars,1);
	
	a.add = Cudd_bddAnd(a.dd,temp=a.add,cntr);
	Cudd_Ref(a.add); Cudd_RecursiveDeref(a.dd,temp); Cudd_RecursiveDeref(a.dd,cntr);
	
	//dumpDot(a.dd,a.add,string("scratch/multed.dot"));

	cout<<"3 "<<std::flush;
	/* Z0==0 implies X0==start-state */
	DdNode *c0, *c1;
	c0 = makeCube(0, a.dd, curVars,n);
	c1 = getAllZeroBDDState(a.dd,s0Vars,nStateVars);
	
	temp = Cudd_bddOr(a.dd,Cudd_Not(c0),c1);
	Cudd_Ref(temp); Cudd_RecursiveDeref(a.dd,c0); Cudd_RecursiveDeref(a.dd,c1);

	a.add = Cudd_bddAnd(a.dd,temp2 = a.add, temp);
	Cudd_Ref(a.add); Cudd_RecursiveDeref(a.dd,temp); Cudd_RecursiveDeref(a.dd,temp2);
	
	cout<<"4 "<<std::flush;
	/* t = t OR (Z0==N AND Z1==N AND X0=X1)*/
	c0 = makeCube(traceLength,a.dd,curVars,n);
	c1 = makeCube(traceLength,a.dd,nexVars,n);
	DdNode* eq = Cudd_ReadOne(a.dd);
	Cudd_Ref(eq);
	for(uint32_t i = 0; i<nStateVars;i++){
		temp = Cudd_bddXnor(a.dd,Cudd_bddIthVar(a.dd,varStartIndex+i),Cudd_bddIthVar(a.dd,varStartIndex+nStateVars+i));
		Cudd_Ref(temp);
		
		eq = Cudd_bddAnd(a.dd,temp2 = eq,temp);
		Cudd_Ref(eq); Cudd_RecursiveDeref(a.dd,temp2); Cudd_RecursiveDeref(a.dd,temp);
	}
	eq = Cudd_bddAnd(a.dd,temp = eq, c1);
	Cudd_Ref(eq); Cudd_RecursiveDeref(a.dd,temp);
	
	eq = Cudd_bddAnd(a.dd,temp = eq, c0);
	Cudd_Ref(eq); Cudd_RecursiveDeref(a.dd,temp);
	
	a.add = Cudd_bddOr(a.dd,temp = a.add,eq);
	Cudd_Ref(a.add); Cudd_RecursiveDeref(a.dd,temp); Cudd_RecursiveDeref(a.dd,eq);

	cout<<"5 "<<std::flush;
	/* Rename vars so that new index order (after varstartindex) is cntr0-X0-cntr1-X1 */
	int* permut = new int[2*n+2*nStateVars+varStartIndex];
	for(uint32_t i = 0; i<varStartIndex;i++){
		permut[i] = i;
	}
	//WRONG: The i-th entry of permut holds the index of the variable that is to substitute the i-th variable.
	//RIGHT: Its the other way around
	for(uint32_t i = varStartIndex; i<varStartIndex+n;i++){
		//permut[i] = i+2*nStateVars;
		permut[i+2*nStateVars] = i;
	}
	for(uint32_t i = varStartIndex+n; i<varStartIndex+n+nStateVars;i++){
		//permut[i] = i - n;
		permut[i - n] = i;
	}
	for(uint32_t i = varStartIndex+n+nStateVars; i<varStartIndex+2*n+nStateVars;i++){
		//permut[i] = i + nStateVars;
		permut[i + nStateVars] = i;
	}
	for(uint32_t i = varStartIndex+2*n+nStateVars; i<varStartIndex+2*n+2*nStateVars;i++){
		//permut[i] = i - 2*n;
		permut[i- 2*n] = i;
	}
	DdNode* newDD = Cudd_bddPermute(a.dd,a.add,permut);
	Cudd_Ref(newDD); Cudd_RecursiveDeref(a.dd,a.add);
	
	a.add = newDD;
	
	//dumpDot(a.dd,a.add,string("scratch/permuted.dot"));
	cout<<" Done!\n";
	delete permut;
	delete curVars;
	delete nexVars;
	delete s0Vars;
}

void TraceSampler::buildDataStructures(){
	ADD& a = adds[0];
	string stateSeq;
	a.precompileSampleDAG= true; //use grouping constraints if present in meta file.
	cout<<"Reading DD from file.. "<<std::flush;
	readDD(ddFileName, ddMetaFileName, &a.dd, &a.add, numLatches, numInputs, nStateVars, 
		varStartIndex, &a.precompileSampleDAG, &stateSeq);
	cout<<"Done reading!\n";
	readTime = cpuTimeTotal();
	printTimeTaken("ReadDD",readTime-startTime,2);
	cout<<"Enabling autoreordering..\n";
	Cudd_AutodynEnable(adds[0].dd,CUDD_REORDER_GROUP_SIFT);
	adds[n].precompileSampleDAG = true; //can precompile nth add even if no contiguity constraints
	for (uint32_t i = 1; i<n; i++) adds[i].precompileSampleDAG = a.precompileSampleDAG;
	assert(stateSeq.compare(STATE_SEQ_01)==0);
	if(notPowerOf2){
		loopAfterN();
		nStateVars += n;
	}
	// s[i*nStateVars]--s[((i+1)*nStateVars)-1] are the state bits for state 2^(i-1), i \in [0,n-1]
	sVars = (DdNode**) calloc((n+2)*nStateVars, sizeof(DdNode*)); //2^n transitions mean n+1 bdds -- 1,2,4,..2^n
	DdNode* temp;
	for(uint32_t i = varStartIndex; i<varStartIndex+2*nStateVars;i++){
		sVars[i-varStartIndex] = Cudd_bddIthVar(a.dd,i);
	}
	//if using non-power of two, then initialSet must have all zeroes for the counter part.
	//this happens automatically when using getAllZeroBDDState. For other initialSets, will have to be careful.
	initialSet = getAllZeroBDDState(a.dd,sVars,nStateVars);
	
	t = new Trace(traceLength+1,nStateVars);
	autodynTime = cpuTimeTotal();
	printTimeTaken("DynReorder after read",autodynTime- readTime,2);
	if (notPowerOf2){cout<<"Note: DynReorder time also included time for adding couter since tracelength is not a power of two!\n";}
	if(useReachableSet == 2){  
	//if(useReachableSet==1 || useReachableSet == 2){
		//compute for 2^n and not just tracelength steps since we may have to sample states from beyond tracelength
		//restrict state0 to tracelength-1 steps, so state1 will automatically be tracelength steps which means no need to
		//explicitly restrict it
		exactReachSets = (DdNode**) calloc(1,sizeof(DdNode*));
		exactReachSets[0] = restrictS0ToReachableStates(a.dd,&a.add,initialSet,sVars,nStateVars,pow(2,n)-1,2*nStateVars, true);
		cout<<"For reachable set, ";
		convertToADD(adds[0].dd,&exactReachSets[0], true);
	} else if(useReachableSet==3){
		exactReachSets = computeExactReachableSets(a.dd,&a.add,initialSet,sVars,nStateVars,1<<n,2*nStateVars, true);
	}
	reachComputeTime = cpuTimeTotal();
	printTimeTaken("Computing Reach Set(s)",reachComputeTime - autodynTime,2);
	//convert to add (svars will be populated with addvars in unroll)
	cout<<"For base bdd (adds[0]), ";
	convertToADD(a.dd,&a.add, true);
	convertToADDTime = cpuTimeTotal();
	printTimeTaken("Conversion to ADD",convertToADDTime - reachComputeTime,2);
	#if DFAST < 2
	if(Cudd_DebugCheck(a.dd)){cout<<"Debug check found inconsistencies in CUDD before unroll. Exiting..\n"; exit(0);}
	#endif
	debugcheck1Time = cpuTimeTotal();
	printTimeTaken("Debug-Check1",debugcheck1Time - convertToADDTime,2);
	#if DFAST < 2
	unroll(true);
	#else
	unroll(false);
	#endif
	unrollTime = cpuTimeTotal();
	printTimeTaken("Unroll",unrollTime - debugcheck1Time,2);
	#if DFAST < 2
	if(Cudd_DebugCheck(a.dd)){cout<<"Debug check found inconsistencies in CUDD after unroll. Exiting..\n"; exit(0);}
	#endif
	debugcheck2Time = cpuTimeTotal();
	printTimeTaken("Debug-Check2",debugcheck2Time - unrollTime,2);
	cout<<"Perms after unroll:\n";
	printPerm(adds[0].dd);
	createAuxStructures();
	allAuxStructsTime = cpuTimeTotal();
	printTimeTaken("Construct All Aux Structs",allAuxStructsTime - debugcheck2Time,2);
	createSamplingDAGs();
	allDAGsTime = cpuTimeTotal();
	printTimeTaken("Construct All Sampling DAGs",allDAGsTime - allAuxStructsTime,2);
	cout<<"Finished building all Datastructures!\n";
}

void TraceSampler::unroll(bool countModels){
	cout<<"Unrolling..\n";
	
	DdNode *newDD, *temp, *twoStateDD;
	// we dont use insert function of unordered_map because it doesn't replace an existing value
	//insert_or_assign is only available in c++17
	for (int i = varStartIndex; i<2*nStateVars+varStartIndex; i++){
		sVars[i-varStartIndex] = Cudd_addIthVar(adds[0].dd,i);
		Cudd_Ref(sVars[i-varStartIndex]);
		//s[0] will get correctly overwritten inside loop below.
		adds[1].varMap[sVars[i-varStartIndex]->index] = pair<uint32_t,uint8_t>(i-varStartIndex-nStateVars,1);
	}
	//the switching of curr and next state vars is done in readBDD. See utils.cpp
	/* 
	* 2statedd = adds[0]
	* start for loop
		* new dd = swap &s[0] and &s[i] in 2statedd
		* create nState new vars and assign to s[i+1]
		* new dd = swap &s[0] and &s[i+1] in new dd
		* adds[i] = multiply new dd and 2statedd
		* 2statedd = exist abst s[i] from adds[i]
	*/
	twoStateDD = adds[0].add; // we ref this only if checkSamples is true as it will be used later
	if(checkSamples){
		Cudd_Ref(twoStateDD);
	}
	uint8_t swap01First = 0; // 0 = use addPermut, 1=swap01first 2=swap12first
	int* permut; // incase we use Cudd_addPermut for swapping vars, this will hold the permutation;
	if(swap01First==0){
		permut=(int*)malloc(((n+2)*nStateVars+varStartIndex)*sizeof(int));
		for(uint32_t i = 0; i<(n+2)*nStateVars; i++){
			permut[i]=i; //not reading index of sVar variable since not all are created yet
		}
	}
	// In the loop below,
	//add[0].add will get derefed at label ADD0_DEREF. It will be unusable after that if not refed above.
	for(int i = 1; i<n+1; i++){  //adds[0] is already computed
		adds[i].sVarSIndex = 0;
		adds[i].sVarSPIndex = i*nStateVars;
		adds[i].sVarSDPIndex = (i+1)*nStateVars;
		adds[i].dd = adds[0].dd;
		cout<<"Creating add number "<<i<<endl;
		/*For creating add (0,i/2,i) we need to multiply add(0,i/2) * add(i/2,i). For creating add(i/2,i)
		from add(0,i/2), we can either (A) swap 0<->i/2 vars first giving add(i/2,0) then swap 0<->i vars 
		giving add (i/2,i), or (B) swap (i/2)<->i first giving add (0,i) then swap 0<->i/2  giving add (i/2,i) 
		Option (A) can be selected by setting swap01First to true below.
		Which one is more efficient depends on size of intermediate ADD. Testing on 1-2 examples was inconclusive
		and probably more testing is needed. It is not straightforward since vars corresponding to states 
		0,n-1 and n have no contiguity constraints on them so the intermediate add for iteration close to n-1/n
		may not blow up as much as other adds. */
		
		if (swap01First==1){
			newDD = Cudd_addSwapVariables(adds[i].dd,twoStateDD,&(sVars[0]),&(sVars[i*nStateVars]),nStateVars);
			Cudd_Ref(newDD);
		}
		//cout<<"Swapped vars. Creating new vars\n";
		for (int j = (i+1)*nStateVars, k= 0; j<(i+2)*nStateVars; j++,k++){
			sVars[j] = Cudd_addNewVar(adds[i].dd);
			Cudd_Ref(sVars[j]);
			adds[i].varMap[sVars[k]->index] = pair<uint32_t,uint8_t>(k,0);
			adds[i].varMap[sVars[j]->index] = pair<uint32_t,uint8_t>(k,2);
			//for i = n, %(n+1) ensures we dont go out of bound. adds[0] will be overwritten in createAuxStructures
			adds[(i+1)%(n+1)].varMap[sVars[j]->index] = pair<uint32_t,uint8_t>(k,1);
		}
		//no need to enforce contiguity on nth and n-1th statevars, since they will only be present in the nth add
		//as samplevars, and we sample nth add in full anyway.*/
		if (i<n-1 && adds[0].precompileSampleDAG){
			Cudd_MakeTreeNode(adds[i].dd,varStartIndex+(i+1)*nStateVars,nStateVars,MTR_DEFAULT);
		}
		
		if(swap01First==1){
			newDD = Cudd_addSwapVariables(adds[i].dd,(temp = newDD),&(sVars[0]),&(sVars[(i+1)*nStateVars]),nStateVars);
			Cudd_Ref(newDD); Cudd_RecursiveDeref(adds[i].dd,temp);
		} else if(swap01First==2){
			newDD = Cudd_addSwapVariables(adds[i].dd,twoStateDD,&(sVars[i*nStateVars]),&(sVars[(i+1)*nStateVars]),nStateVars);
			Cudd_Ref(newDD);
			newDD = Cudd_addSwapVariables(adds[i].dd,(temp = newDD),&(sVars[0]),&(sVars[i*nStateVars]),nStateVars);
			Cudd_Ref(newDD); Cudd_RecursiveDeref(adds[i].dd,temp);
		} else if(swap01First==0) {
			for(uint32_t j = 0; j<nStateVars;j++){
				//set i-1 to itself.
				permut[Cudd_NodeReadIndex(sVars[j+(i-1)*nStateVars])] = Cudd_NodeReadIndex(sVars[j+(i-1)*nStateVars]);
				//0 to get i
				permut[Cudd_NodeReadIndex(sVars[j])] = Cudd_NodeReadIndex(sVars[j+(i)*nStateVars]); 
				//i to get i+1
				permut[Cudd_NodeReadIndex(sVars[j+(i)*nStateVars])] = Cudd_NodeReadIndex(sVars[j+(i+1)*nStateVars]);
				//i+1 to get 0. Technically not required, but we need a valid permutation
				permut[Cudd_NodeReadIndex(sVars[j+(i+1)*nStateVars])] = Cudd_NodeReadIndex(sVars[j]);
			}
			cout<<"Created permut. Permuting..\n";			
			newDD = Cudd_addPermute(adds[i].dd,twoStateDD,permut);
			cout<<"Done permut! "<<newDD<<"\n";
			Cudd_Ref(newDD);
		} else{
			printf("swap01First set to unrecognized value. Exiting..\n");
			exit(1);
		}
		
		if (useReachableSet==2 && i<n){	
			DdNode* temp2;
			twoStateDD = Cudd_addRestrict(adds[0].dd,temp2=twoStateDD,exactReachSets[0]);
			Cudd_Ref(twoStateDD); Cudd_RecursiveDeref(adds[0].dd,temp2);	
		} else if ( (useReachableSet==1 || useReachableSet==2) && i==n) {
			convertToADD(adds[0].dd,&initialSet, false);	
			cout<<"twoStateDD statistics before state0 AND initialSet:\n";
			Cudd_PrintSummary(adds[i].dd,twoStateDD,Cudd_SupportSize(adds[i].dd, twoStateDD),1);
			//DdNode* tmpRestrict = Cudd_addRestrict(adds[i].dd, adds[i].add, initialSet);
			DdNode* tmpRestrict = Cudd_addApply(adds[i].dd,Cudd_addTimes, twoStateDD, initialSet);
			Cudd_Ref(tmpRestrict); Cudd_RecursiveDeref(adds[i].dd,twoStateDD);
			twoStateDD = tmpRestrict;
			cout<<"twoStateDD statistics after state0 AND initialSet:\n";
			Cudd_PrintSummary(adds[i].dd,twoStateDD,Cudd_SupportSize(adds[i].dd, twoStateDD),1);
		}
		if(useReachableSet==2){
			DdNode* temp2;
			newDD = Cudd_addRestrict(adds[0].dd,temp2=newDD,exactReachSets[0]);
			Cudd_Ref(newDD); Cudd_RecursiveDeref(adds[0].dd,temp2);
		}
		
		if(useReachableSet==3){
			//convert to add, change vars, deref
			convertToADD(adds[0].dd,&exactReachSets[(i-1)*3], false);
			if (i<n)
			//if(false)
				twoStateDD = Cudd_addRestrict(adds[0].dd,temp=twoStateDD,exactReachSets[(i-1)*3]);
			else
				twoStateDD = Cudd_addApply(adds[0].dd,Cudd_addTimes, temp=twoStateDD, exactReachSets[(i-1)*3]); 
			Cudd_Ref(twoStateDD); Cudd_RecursiveDeref(adds[0].dd,temp); 
			Cudd_RecursiveDeref(adds[0].dd,exactReachSets[(i-1)*3]);
			
			convertToADD(adds[0].dd,&exactReachSets[(i-1)*3 + 1], false);
			exactReachSets[(i-1)*3 + 1] = Cudd_addSwapVariables(adds[0].dd,temp=exactReachSets[(i-1)*3 + 1],&(sVars[0]),
				&(sVars[(i)*nStateVars]),nStateVars);
			Cudd_Ref(exactReachSets[(i-1)*3 + 1]); Cudd_RecursiveDeref(adds[0].dd,temp); 
			//if(i<n)
			//if (false)
			if (1) //since we AND in previous case for i==n, no need to AND here 
				newDD = Cudd_addRestrict(adds[i].dd,temp = newDD,exactReachSets[(i-1)*3 + 1]);
			else
				newDD = Cudd_addApply(adds[0].dd,Cudd_addTimes, temp=newDD, exactReachSets[(i-1)*3 + 1]);
			Cudd_Ref(newDD); Cudd_RecursiveDeref(adds[i].dd,temp);
			Cudd_RecursiveDeref(adds[0].dd,exactReachSets[(i-1)*3+1]);
			/*
			convertToADD(adds[0].dd,&exactReachSets[(i-1)*3 + 2]);
			exactReachSets[(i-1)*3 + 2] = Cudd_addSwapVariables(adds[0].dd,temp=exactReachSets[(i-1)*3 + 2],&(sVars[0]),
				&(sVars[(i+1)*nStateVars]),nStateVars);
			Cudd_Ref(exactReachSets[(i-1)*3 + 2]); Cudd_RecursiveDeref(adds[0].dd,temp); 
			//if(i<n)
			if (false)
				newDD = Cudd_addRestrict(adds[i].dd,temp = newDD,exactReachSets[(i-1)*3 + 2]);
			else
				newDD = Cudd_addApply(adds[0].dd,Cudd_addTimes, temp=newDD, exactReachSets[(i-1)*3 + 2]);
			Cudd_Ref(newDD); Cudd_RecursiveDeref(adds[i].dd,temp);
			*/
			Cudd_RecursiveDeref(adds[0].dd,exactReachSets[(i-1)*3+2]);
		}
		//cout<<"Taking product.. "<<newDD<<" "<<twoStateDD<<"\n";
		adds[i].add = Cudd_addApply(adds[i].dd, Cudd_addTimes, newDD, twoStateDD);
		//cout<<"Product done!\n"<<adds[i].add<<"\n";
		Cudd_Ref(adds[i].add);
		//cout<<"Refed!\n";
		//cout<<twoStateDD<<" "<<newDD<<"\n";
		ADD0_DEREF: Cudd_RecursiveDeref(adds[i].dd,twoStateDD);
		Cudd_RecursiveDeref(adds[i].dd,newDD);
		temp = Cudd_ReadOne(adds[i].dd);
		Cudd_Ref(temp);
		//cout<<"Constructing cube for EAbstract..\n";
		for(int j = 0; j<nStateVars; j++) {
			//cout<<j<<" "<<temp<<" "<<(sVars[i*nStateVars+j])<<"\n";
			temp = Cudd_addApply(adds[i].dd, Cudd_addTimes, (twoStateDD = temp), (sVars[i*nStateVars+j]));
			Cudd_Ref(temp);
			Cudd_RecursiveDeref(adds[i].dd, twoStateDD);
			//cout<<temp<<"\n";
		}
		cout<<"Exist abstracting..\n";
		if (i<n) { // do not need to exist abstract add[n] unless want to countModels
			//cout<<"add i "<<adds[i].add<<"\n";
			twoStateDD = Cudd_addExistAbstract(adds[i].dd, adds[i].add, temp);	
			//cout<<twoStateDD<<"\n";
			Cudd_Ref(twoStateDD);
		} else{
			if(countModels){
				for(int j = 0; j<nStateVars; j++) {
					//cout<<j<<" "<<temp<<" "<<(sVars[i*nStateVars+j])<<"\n";
					temp = Cudd_addApply(adds[i].dd, Cudd_addTimes, (twoStateDD = temp), (sVars[j]));
					Cudd_Ref(temp);
					Cudd_RecursiveDeref(adds[i].dd, twoStateDD);
					//cout<<temp<<"\n";
				}
				for(int j = 0; j<nStateVars; j++) {
					//cout<<j<<" "<<temp<<" "<<(sVars[i*nStateVars+j])<<"\n";
					temp = Cudd_addApply(adds[i].dd, Cudd_addTimes, (twoStateDD = temp), (sVars[(i+1)*nStateVars+j]));
					Cudd_Ref(temp);
					Cudd_RecursiveDeref(adds[i].dd, twoStateDD);
					//cout<<temp<<"\n";
				}
				twoStateDD = Cudd_addExistAbstract(adds[i].dd, adds[i].add, temp);	
				Cudd_Ref(twoStateDD);
				cout<<"Count is: "<<Cudd_V(twoStateDD)<<"\n";
				Cudd_RecursiveDeref(adds[i].dd,twoStateDD);
			} else {Cudd_RecursiveDeref(adds[i].dd,temp);}	
		}
	}
	if(swap01First==0){
		free(permut);
	}
	//cout<<"number of basemanager vars: "<<Cudd_ReadSize(baseManager)<<endl;
	printPerm(adds[0].dd);
	
	//varmap is created during unroll since indices dont change.
	//Rest are dependent on var order that is dynamic. So generated in createAuxStructures
}

void TraceSampler::createAuxStructure(uint32_t addNum){
	//compressedPerm only includes levels for variables that are to be sampled
	//that means that for nth add, it will include all vars in the add
	//for other adds it will only be the s' vars
	class Indexer{
		public:
		uint32_t level;
		uint32_t index;
		bool operator < (const Indexer& str) const{
        	return (level < str.level);
    	}
	};
	ADD& a = adds[addNum];
	cout<<"Creating aux structures for ADD number "<<addNum<<endl;
	Cudd_AutodynDisable(a.dd);
	vector<Indexer> ind_level_map(nStateVars);
	//generate perm o(n)
	for(int i = 0; i<nStateVars; i++){
		uint32_t currIndex = sVars[i+a.sVarSPIndex]->index;
		ind_level_map[i].index = currIndex;
		ind_level_map[i].level = Cudd_ReadPerm(a.dd,currIndex);
	}
	if (addNum==n){
		ind_level_map.resize(3*nStateVars);
		for(int i = 0; i<nStateVars; i++){
			uint32_t currIndex = sVars[i+a.sVarSIndex]->index;
			ind_level_map[i+nStateVars].index = currIndex;
			ind_level_map[i+nStateVars].level = Cudd_ReadPerm(a.dd,currIndex);
		}
		
		for(int i = 0; i<nStateVars; i++){
			uint32_t currIndex = sVars[i+a.sVarSDPIndex]->index;
			ind_level_map[i+2*nStateVars].index = currIndex;
			ind_level_map[i+2*nStateVars].level = Cudd_ReadPerm(a.dd,currIndex);
		}
		a.compressedInvPerm = new int[3*nStateVars];
		a.sampleStructDepth = 3*nStateVars + 1; //since sample structure should include leaves
	} else{
		a.compressedInvPerm = new int[nStateVars];
	}
	cout<<"Sorting..\n";
	//sort nlogn
	sort(ind_level_map.begin(),ind_level_map.end());
	
	//compress and insert o(n) 
	a.startSampleVarLevel = ind_level_map[0].level;
	a.endSampleVarLevel = ind_level_map[nStateVars-1].level; // changed below for nth add
	for(int i = 0; i< nStateVars; i++){
		a.compressedPerm[ind_level_map[i].index] = i;
		a.compressedInvPerm[i] = ind_level_map[i].index;
	}
	if (addNum==n){
		for(int i = nStateVars; i< 3*nStateVars; i++){
			a.compressedPerm[ind_level_map[i].index] = i;
			a.compressedInvPerm[i] = ind_level_map[i].index;
		}
		a.endSampleVarLevel = ind_level_map[3*nStateVars-1].level;
	} else{	// if not nth add
		a.sampleStructDepth = nStateVars + 1; //both preleaves and leaves should be part of sample struct
	}
}

void TraceSampler::createAuxStructures(){
	if (checkSamples){
		for (uint32_t i = 0; i<nStateVars; i++){
			adds[0].varMap[sVars[i]->index] = pair<uint32_t, uint8_t> (i,0); // 0 = currentstate
			adds[0].varMap[sVars[i+nStateVars]->index] = pair<uint32_t, uint8_t> (i,2); // 2 = nextState. not kept 1
			//so that we can use TRAVERSE with allowCase1 false for checkTrace
		}
	}
	for (int i = 1; i<=n; i++){
		createAuxStructure(i);
	}
}

void TraceSampler::createSamplingDAG(uint32_t addNum){
	ADD& a = adds[addNum];
	assert(a.sampleStructDepth > 0);
	vector<unordered_map<DdNode*, SamplerNode*>> sampleHashes(a.sampleStructDepth);
	uint32_t numRoot = 0;
	for (auto& rootPair: a.rootSNs){
		SamplerNode* root = rootPair.second;
		numRoot++;
		// /*DBG*/ cout<<"Doing "<<numRoot<<" out of "<<a.rootSNs.size()<<endl;
		// /*DBG*/ cout<<"Sampler node addresses:"<<root<<":"<<rootPair.first<<" "<<std::flush;
		sampleHashes.clear();
		sampleHashes.resize(a.sampleStructDepth);
		sampleHashes.at(root->getCompressedLevel()).emplace(rootPair.first,root);
		for (int i = 0; i<a.sampleStructDepth-1; i++){ //Process the last level separately
			// /*DBG*/ cout<<"\nDoing level "<<i<<endl;
			for (auto& hashedPair : sampleHashes.at(i)){
				DdNode* node = hashedPair.first;
				SamplerNode* nodeSN = hashedPair.second;
				DdNode* t = Cudd_T(node); 
				unsigned tLevel = a.sampleStructDepth-1; //last lvel
				if (a.compressedPerm.find(t->index) != a.compressedPerm.end()){
					tLevel = a.compressedPerm.at(t->index);
				}
				std::unordered_map<DdNode*,SamplerNode*>::const_iterator newNode = sampleHashes.at(tLevel).find(t);
				// NumType* pathIncr = nodeSN->rootPathCount->multiplyByPowerOfTwo(tLevel-(i+1));
				if (newNode == sampleHashes[tLevel].end()){
					// SamplerNode* tSN = new SamplerNode(t, nodeSN, pathIncr, tLevel, true); //since this is the then node
					SamplerNode* tSN = snFactory->createChild(nodeSN,true,tLevel-(i+1),tLevel);
					// /*DBG*/ cout<<i<<":"<<tSN<<":"<<t<<" "<<std::flush;
					sampleHashes.at(tLevel).emplace(t,tSN);
				} else{
					SamplerNode* nNode = newNode->second;
					// nNode->parents.push_back(nodeSN);
					// nNode->rootPathCount->addSelf(pathIncr);
					// delete pathIncr;
					// nNode->thenElse.push_back(true); // since this is the then node
					nNode->addParent(nodeSN,true,tLevel-(i+1));
				}
			
				DdNode* e = Cudd_E(node); 
				unsigned eLevel = a.sampleStructDepth-1; //last lvel
				if (a.compressedPerm.find(e->index) != a.compressedPerm.end()){
					eLevel = a.compressedPerm.at(e->index); 
				}
				// pathIncr = nodeSN->rootPathCount->multiplyByPowerOfTwo(eLevel-(i+1));
				std::unordered_map<DdNode*,SamplerNode*>::const_iterator newNode2 = sampleHashes[eLevel].find(e);
				if (newNode2 == sampleHashes[eLevel].end()){
					// SamplerNode* eSN = new SamplerNode(e, nodeSN, pathIncr, eLevel,false);// since this is the else case
					SamplerNode* eSN = snFactory->createChild(nodeSN,false,eLevel-(i+1),eLevel);
					// /*DBG*/ cout<<i<<":"<<eSN<<":"<<e<<" "<<std::flush;
					sampleHashes.at(eLevel).emplace(e,eSN);
				} else{
					SamplerNode* nNode = newNode2->second;
					// nNode->parents.push_back(nodeSN);
					// nNode->rootPathCount->addSelf(pathIncr);
					// delete pathIncr;
					// nNode->thenElse.push_back(false); // since this is the else case
					nNode->addParent(nodeSN,false,eLevel-(i+1));
				}
			}
		}
		for (auto& leaf : sampleHashes[a.sampleStructDepth-1]){
			a.allSamplingPreLeaves[rootPair.first].push_back(leaf);
			//DBG cout<<a.sampleStructDepth-1<<":"<<leaf.second<<":"<<leaf.first<<" ";
		}
		if (a.allSamplingPreLeaves[rootPair.first].size()==0){
			cout<<"WARNING: No (pre)leaf found for a rootSN for add number "<<addNum;
			cout<<" samplingStructDepth-1 = "<<a.sampleStructDepth-1<<" compressedVarLevel of rootSN is "
			<<root->getCompressedLevel()<<" sampleHash size at last level is "<<sampleHashes[a.sampleStructDepth-1].size();
			cout<<" usePreLeaves is "<<a.usePreleaves<<" isConstNode: "<<Cudd_IsConstant(rootPair.first)<<" Exiting..\n";
			exit(1);
		}
		//DBG cout<<"\n";
	}
}

void TraceSampler::createSamplingDAGs_rec(ADD& a, DdNode* currNode, unordered_set<DdNode*>* sampleRootSet){
	if(Cudd_IsConstant(currNode)){
		assert(Cudd_V(currNode)==0); // while recursing to find rootSNs, typically we should not encounter a non-0 
		//leaf without seeing any sample vars
		return;
	}
	uint32_t currLevel = Cudd_ReadPerm(a.dd,currNode->index);
	if(currLevel>=a.startSampleVarLevel){
		sampleRootSet->insert(currNode);
	} else {
		DdNode* t = Cudd_T(currNode);
		createSamplingDAGs_rec(a, t, sampleRootSet);
		DdNode* e = Cudd_E(currNode);
		createSamplingDAGs_rec(a, e, sampleRootSet);
	}
}

void TraceSampler::createSamplingDAGs(){
	cout<<"Creating sampling structure..\n";
	for (int i = 1; i<=n; i++){
		cout<<"ADD "<<i<<" out of "<<n<<endl;
		ADD& a = adds[i];
		if(!a.precompileSampleDAG) {
			cout<<"Not precompiling\n";
			continue;
		}
		unordered_set<DdNode*> sampleRootSet;
		DdNode* currNode = a.add;
		uint32_t currLevel = Cudd_ReadPerm(a.dd,currNode->index);
		if(currLevel>=adds[i].startSampleVarLevel){
			cout<<"Only one root!\n";
			uint32_t cmprsdLevel = a.sampleStructDepth-1;
			if (a.compressedPerm.find(currNode->index) != a.compressedPerm.end()){
				cmprsdLevel = a.compressedPerm.at(currNode->index);
			}
			a.rootSNs.emplace(currNode,snFactory->createRoot(cmprsdLevel));
		} else {
			cout<<"Recursing to find roots..\n";
			DdNode* t = Cudd_T(currNode);
			createSamplingDAGs_rec(a, t, &sampleRootSet);
			DdNode* e = Cudd_E(currNode);
			createSamplingDAGs_rec(a, e, &sampleRootSet);
			//cout<<"ADD "<<i<<" has roots: ";
			for (DdNode* root: sampleRootSet){
				uint32_t cmprsdLevel = a.sampleStructDepth-1;
				if (a.compressedPerm.find(root->index) != a.compressedPerm.end()){
					cmprsdLevel = a.compressedPerm.at(root->index);
				}
				a.rootSNs.emplace(root,snFactory->createRoot(cmprsdLevel));
				//cout<<root<<" ";
			}
			//cout<<"\n";
		}
		cout<<"Found "<<a.rootSNs.size()<<" roots, creating sampling DAG(s)\n";
		createSamplingDAG(i);
		snFactory->forgetAllUsedNodes();
	}
}

#define TRAVERSE(_cn_,_cl_,_ci_,_add_,_s_,_sp_,_sdp_,_allowCase1_,_cond_,_defErrMsg_) \
	{while (_cond_){ \
		/*DBG printf("%p ",_cn_);*/ \
		pair<uint32_t,uint8_t>& _p_ = _add_.varMap.at(_ci_); \
		bool _bit_; \
		switch(_p_.second){ \
			case 0: \
				_bit_ = _s_->get(_p_.first); \
				break; \
			case 1: \
				if (_allowCase1_) { \
					_bit_ = _sp_->get(_p_.first); \
					break; \
				} else{ \
					cout<<_defErrMsg_; \
					printf("Case1 not allowed. Value in varmap is neither 0=s nor 2=s''. It is %u " \
					" for index %u. Exiting..\n",_p_.second,_ci_); \
					exit(1); \
				} \
				break; \
			case 2: \
				_bit_ = _sdp_->get(_p_.first); \
				break; \
			default: \
				cout<<_defErrMsg_; \
				printf("Case1 not allowed. Value in varmap is neither 0=s nor 2=s''. It is %u " \
					" for index %u. Exiting..\n",_p_.second,_ci_); \
				exit(1); \
		} \
		/*DBG printf("%d ",_bit_);*/ \
		_cn_ = _bit_? Cudd_T(_cn_) : Cudd_E(_cn_); \
		_ci_ = _cn_->index; \
		_cl_ = Cudd_ReadPerm(_add_.dd,_ci_); \
	} /*DBGprintf("\n");*/}

/* for general case, where each add may be different wrt where s' is located and consequently how many sampling 
 * structures 
 * there are, the general skeleton of the next procedure will be 
 * 		propagate intial -- traverse from some starting root to get to a root node which can be searched in a 
 * 							hash table to get the preleaves
 * 		get leaves -- propagate remaining to get the leafset from preleaves
 * 		sample from structure -- the resulting samplestructure is now complete and we can sample s' from it
 * 
 * Depending on which variable ordering is used in the add, some of the above phases may be empty
 * for example if s' is in beginning then propagate initial will be empty.
 * 
 * Can also think of having all s,s',s'' variables interspersed and constructing the samplingstructure on the fly say 
 * for the
 * f_n f_n-1 .. adds which are not going to be sampled very frequently. But this is not accommodated for now.
 */ 
void TraceSampler::sampleFromADD(uint32_t addNum, State* s, State* sP, State* sDP){
	ADD& a = adds[addNum];
	// /*DBG*/ cout<<"\nsampling addnum:"<<addNum<<" with samplestructdepth "<<a.sampleStructDepth<<std::flush;
	// Cudd_DebugCheck(a.dd);
	/*
		if not precompiled
			tempadd = a.add
			reduce a.add by asnmt and set a.add since createSamplingDAG will assume a.add is the add to compile from
			or not.. just put it in rootSNs after rootSNs.clear
			ensure usednodes is empty in snfactory so that all nodes created next can be recycled in a go
			call createsamplingdag for addnum
			rest of samplefromadd should be the same.. but check.. initialtraverse should return immediately as root
			node should be at startsamplevarlevel. finaltraverse should also exit immediately since well be at leaf level
			sampling should be same. 
			restore a.add if changed. deref the node. clear rootsn. check checksample to see if it will go through.
			dont forget to recycle nodes at the end of samplefromadd
	*/
	DdNode* tmpADDRoot = 0;
	if (!a.precompileSampleDAG){
		assert(snFactory->noUsedNodes());
		DdNode *tmp, *cube = Cudd_ReadOne(a.dd);
		Cudd_Ref(cube);
		for(uint32_t i = 0; i<nStateVars; i++){
			DdNode* v = sVars[a.sVarSIndex+i];
			if(!s->get(i)){
				v = Cudd_addCmpl(a.dd,v);
				Cudd_Ref(v);
			}
			cube = Cudd_addApply(a.dd,Cudd_addTimes,tmp = cube,v);
			Cudd_Ref(cube); if(!s->get(i))Cudd_RecursiveDeref(a.dd,v); Cudd_RecursiveDeref(a.dd,tmp);
		}
		for(uint32_t i = 0; i<nStateVars; i++){
			DdNode* v = sVars[a.sVarSDPIndex+i];
			if(!sDP->get(i)){
				v = Cudd_addCmpl(a.dd,v);
				Cudd_Ref(v);
			}
			cube = Cudd_addApply(a.dd,Cudd_addTimes,v, tmp = cube);
			Cudd_Ref(cube); if(!sDP->get(i))Cudd_RecursiveDeref(a.dd,v); Cudd_RecursiveDeref(a.dd,tmp); 
		}	
		tmp = Cudd_Cofactor(a.dd,a.add,cube);
		Cudd_Ref(tmp); Cudd_RecursiveDeref(a.dd,cube);
		assert(a.rootSNs.empty());
		// cout<<"Cofactor done! index of root is "<<Cudd_NodeReadIndex(tmp)<<" is leaf?"<<Cudd_IsConstant(tmp)<<endl;
		uint32_t cmprsdLvl = a.compressedPerm.at(tmp->index);
		// cout<<"cmprsdLevel of root of cofactor is "<<cmprsdLvl<<endl;
		SamplerNode* nodeSN = snFactory->createRoot(cmprsdLvl);
		a.rootSNs.emplace(tmp,nodeSN);
		createSamplingDAG(addNum);
		tmpADDRoot = a.add; a.add = tmp; //initial traverse will start from root of a.add, which has to be the cofactor
		//and not the original a.add 
		//tmp needs to be derefed, rootsns need to be cleared and samplernodes need to be recycled at the end of sample.
	}
	
	//traverse till level
	uint32_t currIndex = a.add->index;
	uint32_t currLevel = Cudd_ReadPerm(a.dd,currIndex); //get absolute level not compressed
	DdNode* currNode = a.add;
	//DBG cout<<"Traversing initial:\n";
	TRAVERSE(currNode,currLevel,currIndex,a,s,sP,sDP,0,currLevel<a.startSampleVarLevel,"Error during intial traverse");
	//DBG cout<<"Done initial traverse. Reached node "<<currNode<<"\n";
	DdNode* dagRoot = currNode;
	assert(a.rootSNs.find(currNode)!=a.rootSNs.end());
	SamplerNode* dagRootSN = a.rootSNs.at(dagRoot);
	vector<std::pair<DdNode*,SamplerNode*>>& pleaves = a.allSamplingPreLeaves.at(currNode);

	//get leaves from preleaves
	vector<SamplerNode*> temp;
	
	//DBG cout<<"Preleaves of "<<dagRoot<<":\n";
	//DBG for (SamplerNode* pLeaf: pleaves){
	//DBG 	cout<<pLeaf<<":"<<pLeaf->node<<" ";
	//DBG }
	//DBG cout<<"\n";
	unordered_map<DdNode*, SamplerNode*> sampleHash;
	//cout<<"preleaves size:"<<pleaves.size()<<" "<<std::flush;
	// DBG cout<<"\npLeaf and its parents' cmpLvl:\n"<<std::flush;
	for (auto& pLeafPair: pleaves){
		//for each preleaf, we will find the leaf by traversing. We create a samplerNode for this leaf and make it
		//point to all the parents of the preleaf. Thus conceptually the leaf will replace (ignore) the preleaf
		//and while sampling we will directly go from the leaf to the ancestor (parent)) of the preleaf without
		//encountering the preleaf at all. It maybe possible that a leaf has the same ancestor twice in the 
		//parent array since we are copying all the ancestors from the preleaf. But this is fine -- we are also
		//copying the thenElse array, and the rootPathCounts will take care of sampling the ancestor with the right 
		//weight
		SamplerNode* pLeaf = pLeafPair.second;
		// DBG cout<<pLeaf->getCompressedLevel()<<": "<<std::flush;
		// DBG pLeaf->printParentCmpLvls();
		DdNode* currNode = pLeafPair.first;
		currIndex = currNode->index;
		//DBG cout<<"Traversing final:\n";
		TRAVERSE(currNode,currLevel,currIndex,a,s,sP,sDP,0,!Cudd_IsConstant(currNode),"Error while finding leaves from preleaves.");
		//DBG cout<<"Done traverse final. Reached node with value "<<Cudd_V(currNode)<<"\n";
		//even if the preleaf was a leaf, we create a new SamplerNode for it below, since we destroy all the
		//SamplerNodes we create for leaves as they are not used again
		//cout<<currNode<<":"<<Cudd_V(currNode)<<" "<<std::flush;
		if (Cudd_V(currNode)==0) continue; //skip 0 leaves
		std::unordered_map<DdNode*,SamplerNode*>::const_iterator newNode = sampleHash.find(currNode);
		if (newNode == sampleHash.end()){
			// copy all the parents and thenElse, as conceptually, the preleaf will be replaced by the leaf
			// SamplerNode* currNodeSN = new SamplerNode(currNode, pLeaf->parents, pLeaf->rootPathCount->copy(), pLeaf->compressedLevel, 
			// pLeaf->thenElse);
			SamplerNode* currNodeSN = snFactory->copy(pLeaf);//WARNING: currNodeSN has the same ddnode as pleaf and not currNode
			sampleHash.emplace(currNode,currNodeSN);
		} else{
			SamplerNode* nNode = newNode->second;
			// nNode->parents.insert(nNode->parents.end(),pLeaf->parents.begin(),pLeaf->parents.end());//append all parents
			// nNode->rootPathCount->addSelf(pLeaf->rootPathCount);
			// nNode->thenElse.insert(nNode->thenElse.end(),pLeaf->thenElse.begin(),pLeaf->thenElse.end());//append thenElse values
			nNode->append(pLeaf);
		}
	}
	if (sampleHash.size()==0){
		cout<<"WARNING: No leaf nonzero leaf found after final traverse for add number "<<addNum;
		cout<<" samplingStructDepth-1 = "<<a.sampleStructDepth-1<<" compressedVarLevel of rootSN is "
		<<(a.rootSNs.find(dagRoot))->second->getCompressedLevel()<<" dagRoot address is:"<<dagRoot;
		cout<<" preleavessize:"<<pleaves.size()<<" usePreLeaves is "<<a.usePreleaves<<
		" dagRoot isConstNode: "<<Cudd_IsConstant(dagRoot)<<" Exiting..\n";
		exit(1);
	}
	
	snFactory->clearSentinel(a.sampleStructDepth); //leaf level is a.sampleStructDepth-1 so for sentinel level +1 
	vector<double_t> weights;
	for (auto& leaf : sampleHash){
		weights.push_back(Cudd_V(leaf.first));
		snFactory->sentinel.addParent(leaf.second,false,0);
	}
	//cout<<"\n";
	
	//sample from samplestructure
	//first sample a leaf
	// cout<<"Sampling a leaf\n";
	std::pair<std::pair<SamplerNode*, bool>,uint32_t> parentSample = snFactory->sampleParent(&(snFactory->sentinel),weights,&rb);
	SamplerNode* currNodeSN = parentSample.first.first;
	assert(weights.at(parentSample.second)!=0);
	//DBG*/ cout<<"Reverse printing state sample..\n";
	int32_t cnCmprsdLvl = currNodeSN->getCompressedLevel();
	// cout<<"Sampling parents..\n";
	//iteratively sample parents
	while(cnCmprsdLvl>0){
		//DBG printf("%p ",currNodeSN->node);
		//named chosenAncestor to emphasize that there might be intermediate skipped nodes, 
		//that we need to explicitly sample
		SamplerNode* chosenAncestor;
		int32_t caCmprsdLvl = -1; //compressedLevel for chosenAncestor, initially set to -1
		
		if(currNodeSN != dagRootSN){
			auto parentSample = snFactory->sampleParent(currNodeSN,vector<double_t>(),&rb);;
			chosenAncestor = parentSample.first.first;
			//DBG*/ cout<<chosenAncestor<<":"<<chosenAncestor->node<<" ";
			caCmprsdLvl = chosenAncestor->getCompressedLevel();
			//set the state value for chosen ancestor
			bool tE = parentSample.first.second;
			//DBG*/ cout<<tE<<" \n";
			uint32_t varIndex = a.compressedInvPerm[caCmprsdLvl];
			// pair<uint32_t,uint8_t>& p = a.varMap.at(chosenAncestor->node->index);
			pair<uint32_t,uint8_t>& p = a.varMap.at(varIndex);
			switch (p.second){
				case 0:
					assert(addNum == n); // only relevant if sampling full i.e. for nth add
					s->set(tE,p.first);
					break;
				case 1:
					sP->set(tE,p.first);
					break;
				case 2:
					assert(addNum == n); // only relevant if sampling full i.e for nth add
					sDP->set(tE,p.first);
					break;		
				default:
					cout<<"Error during sampling traverse. Value in varmap  \
					is neither 0=s nor 1=s''. It is "<<p.second<<". Exiting\n";
					exit(1);
					break;
			}
			currNodeSN = chosenAncestor;
		} else{
			//DBG if (a.tNodesByRoot.at(dagRoot).find(currNodeSN)==a.tNodesByRoot.at(dagRoot).end()){
			//DBG 	cout<<"NOTFOUND"<<currNodeSN<<":";
			//DBG }
		}
		//sample skipped nodes/vars, if any
		string bits = rb.GenerateRandomBits(cnCmprsdLvl-caCmprsdLvl-1);
		//we have already sampled a bit for chosenAncestorLevel. currnodesnlevel would have been smapled in prev round
		bool once = true;
		for(int i = caCmprsdLvl+1; i<cnCmprsdLvl; i++){
			//*DBG*/ if (once){
			//*DBG*/ 	cout<<"extra "<<std::flush;
			//*DBG*/ 	once = false;
			//*DBG*/ }
			uint32_t varIndex = a.compressedInvPerm[i];
			pair<uint32_t,uint8_t>& p = a.varMap.at(varIndex);
			bool bit = bits.at(i-caCmprsdLvl-1)=='1';
			//DBG cout<<bit<<" ";
			switch (p.second){
				case 0:
					assert(addNum == n); // only relevant if sampling full i.e. for nth add
					s->set(bit,p.first);
					break;
				case 1:
					sP->set(bit,p.first);
					break;
				case 2:
					assert(addNum == n); // only relevant if sampling full i.e for nth add
					sDP->set(bit,p.first);
					break;		
				default:
					cout<<"Error during skipped vars part of sampling traverse. \
					Value in varmap is neither 0=s nor 1=s''. It is "<<p.second<<". Exiting\n";
					exit(1);
					break;
			}
		}
		cnCmprsdLvl = caCmprsdLvl;	
	}
	//DBG*/ cout<<"\nDone printing reverse sample\n";
	//destroy / delete dynamically allocated objects
	for(auto& leaf: sampleHash){snFactory->recycleSamplerNode(leaf.second);}
	if(!a.precompileSampleDAG){
		Cudd_RecursiveDeref(a.dd,a.add);
		a.add = tmpADDRoot;
		a.rootSNs.clear();
		snFactory->recycleAllUsedNodes();
		a.allSamplingPreLeaves.clear();
	}
	/*
	NOTE: Cant delete DDnodes after sampling structure is created as DdNodes may be shared among adds and maybe part
	of s or s'' in a different add. 
	*/
}

Trace& TraceSampler::drawSample(){
	t->clear();
	//DBG cout<<"Sampling states 0, 2^"<<(n-1)<<" and 2^"<<n<<" from ADD"<< n<<"\n";
	State* s = t->getState(0);
	State* sP = t->getState(pow(2,n-1));
	State* sDP;
	if (notPowerOf2){
		/*Allocating and dellocating these aux States dynamically, because easier and cleaner to do
		  and not much overhead since there can be at most ~log(traceLength)=n such aux States to be
		  allocated and deallocated per trace sample
		*/
		sDP = new State(nStateVars);
	} else {
		sDP = t->getState(pow(2,n));
	}
	sampleFromADD(n, s, sP, sDP);
	double_t checkVal = checkSample(n,s, sP, sDP);
	if (checkVal==0){
		cout<<"While checking sample for add "<<n<<" with "
		"states "<<0<<" "<<pow(2,n-1)<<" "<<pow(2,n)<<" reached leaf with value "<<checkVal<<". Exiting..\n";
		exit(1);
	} else {
		//DBG cout<<"Check Sample passed!\n";
	}
	drawSample_rec(n-1, 0, pow(2,n-2), pow(2,n-1), NULL, NULL);
	if (!notPowerOf2){
		drawSample_rec(n-1, pow(2,n-1), pow(2,n-1)+pow(2,n-2), pow(2,n), NULL, NULL);
	} else {
		if (traceLength>=pow(2,n-1)+pow(2,n-2)){
			drawSample_rec(n-1, pow(2,n-1), pow(2,n-1)+pow(2,n-2), pow(2,n), NULL, sDP);
		} else{
			State* sPDP = new State(nStateVars);
			drawSample_rec(n-1, pow(2,n-1), pow(2,n-1)+pow(2,n-2), pow(2,n), sPDP, sDP);
			free(sPDP);
		}
		free(sDP);
	}
	if (checkSamples){ //not invalidating in DFAST, since sampling is reasonably fast currently and not a bottleneck
		//DBG cout<<"checking trace..\n";
		if(!checkTrace()){
			printf("Check trace failed :(. Exiting..\n");
			exit(1);
		}
	}
	return *t;
}

void TraceSampler::drawSample_rec(uint32_t addNum, uint32_t s_, uint32_t sP_, uint32_t sDP_, State* sP__, State* sDP__){
	if (addNum == 0){
		return;
	}
	State* s = t->getState(s_);
	State* sP, *sDP;
	if (sDP__==NULL) {
		assert(sP__==NULL); //if end state is not aux then middle state cannot be auxiliary
		sP = t->getState(sP_);
		sDP = t->getState(sDP_);
	} else{
		sDP = sDP__;
		if (sP__==NULL) sP = t->getState(sP_); 
		else sP = sP__;
	}
	sampleFromADD(addNum, s, sP, sDP);
	double_t checkVal = checkSample(addNum, s, sP, sDP);
	if (checkVal==0){
		cout<<"While checking sample for add "<<addNum<<" with "
		"states "<<s_<<" "<<sP_<<" "<<sDP_<<" reached leaf with value "<<checkVal<<". Exiting..\n";
		exit(1);
	} else {
		//DBG cout<<"Check Sample passed!\n";
	}
	if (sP__==NULL){
		drawSample_rec(addNum-1, s_, (s_+sP_)/2, sP_, NULL, NULL);
		if (sDP__ == NULL){
			drawSample_rec(addNum-1, sP_, (sP_+sDP_)/2, sDP_, NULL, NULL);
		} else{
			if (traceLength>=(sP_+sDP_)/2){
				drawSample_rec(addNum-1, sP_, (sP_+sDP_)/2, sDP_, NULL, sDP__);
			} else{
				State* sPDP = new State(nStateVars);
				drawSample_rec(addNum-1, sP_, (sP_+sDP_)/2, sDP_, sPDP, sDP__);
				free (sPDP);
			}
		}
	} else{
		if (traceLength>=(s_+sP_)/2){
			drawSample_rec(addNum-1, s_, (s_+sP_)/2, sP_, NULL, sP__);
		} else{
			State* sSP = new State(nStateVars);
			drawSample_rec(addNum-1, s_, (s_+sP_)/2, sP_, sSP, sP__);
			free (sSP);
		}
		//sP__ is not NULL meaning sP is already auxiliary (not part of trace). Therefore no need to
		// do second recursive call 
	}
}

double_t TraceSampler::checkSample(uint32_t addNum, State* s, State* sP, State* sDP){
	ADD& a = adds[addNum];
	DdNode* currNode = a.add;
	uint32_t currIndex = currNode->index;
	uint32_t currLevel = Cudd_ReadPerm(a.dd,currIndex);
	TRAVERSE(currNode,currLevel,currIndex,a,s,sP,sDP,1,!Cudd_IsConstant(currNode),"Error during checkSample traverse");
	return (Cudd_V(currNode));
}

bool TraceSampler::checkTrace(){
	ADD& a = adds[0];
	State* sP = 0; // dummy var used in call to Traverse. AllowCase1 is 0 so this should never be accessed
	for(uint32_t i = 0; i<traceLength; i++){
		//cout<<"Checking state "<<i<<" and "<<i+1<<"\n";
		State* s = t->getState(i);
		State* sDP = t->getState(i+1);
		DdNode* currNode = a.add;
		uint32_t currIndex = currNode->index;
		uint32_t currLevel = Cudd_ReadPerm(a.dd,currIndex);		
		TRAVERSE(currNode,currLevel,currIndex,a,s,sP,sDP,0,!Cudd_IsConstant(currNode),"Error during checkTraverse traverse");
		//cout<<"Traverse reached value: "<<Cudd_V(temp)<<"\n";
		if(Cudd_V(currNode)==0) return 0;
	}
	//DBG cout<<"Check trace passed!\n";
	return 1;
}

void TraceSampler::writeTraceToFile(FILE* ofp){
	for(uint32_t i = 0; i<traceLength+1; i++){
		State* s = t->getState(i);
		for(uint32_t j = notPowerOf2? n:0; j<nStateVars; j++){
			fprintf(ofp,"%d ",s->get(j));
		}
		fprintf(ofp,"\t");
	}
	fprintf(ofp,"\n");
}

uint32_t TraceSampler::getNumStateVars(){
	return nStateVars;
}

int main (int argc, char *argv[]){
	double startTime = cpuTimeTotal();
	cout<< "Starting sample_trace at ";
	print_wall_time(); cout<<endl;

	if (argc!=7 && argc!=8){
		printf("USAGE: sample_trace <in.bdd> <in.bdd.meta> <traceLength> <checkTrace=1/0> <numTraces>"
		 " <useReachableSet=0/1/2/3> [<sampleFile>] \n");
		printf("useReachableSet 0=dont use 1=only set start state 2=use monolithic '<= n-step' reachset\n");
		printf("3=use exact-n-step reachsets\n");
		printf("<traceLength>: length of trace to be sampled\n");
		exit(1);
	}
	char *iFName = argv[1], *mFName = argv[2];
	int traceLength = atoi(argv[3]);
	if(traceLength<=1){cout<<"Tracelength:"<<traceLength<<" should be greater than 1. Exiting..\n";exit(0);}
	uint32_t numTraces = atoi(argv[5]);
	if(numTraces<=1){cout<<"numTraces:"<<numTraces<<" should be greater than 1. Exiting..\n";exit(0);}
	TraceSampler *t;
	int32_t useReachableSet, checkTrace;
	useReachableSet=argv[6][0]-'0';
	if(useReachableSet<0 || useReachableSet >4) {cout<<"Unrecognized useReachableSet:"<<useReachableSet<<
	". Exiting..\n"; exit(0);}
	if (argv[4][0]=='1'){
		checkTrace = true;
	} else if (argv[4][0]=='0'){
		checkTrace = false;
	}
	else { cout<<"Unrecognized checkTrace:"<<argv[4][0]<<". Exiting..\n";exit(0);}

	printDoubleLine();
	#if DFAST == 0
		cout<<"DFAST is 0 (Debug Mode).\n";
	#elif DFAST == 1
		cout<<"DFAST is 1 (Regular Mode).\n";
	#elif DFAST == 2
		cout<<"DFAST is 2 (Fast Mode).\n";
	#else
		int dfast_val = DFAST;
		cout<<"DFAST has unrecognized value:"<<dfast_val<<". Exiting..\n";
		exit(0);
	#endif
	cout<<"sample_trace invoked with in.bdd"<<string(iFName)<<" in.bdd.meta:"<<string(mFName)<<" traceLength:"<<traceLength
	<<" checkTrace:"<<checkTrace<<" numTraces:"<<numTraces<<" useReachSet:"<<useReachableSet;
	bool npt = pow(2,floor(log2(traceLength))) != traceLength;
	uint32_t n = ceil(log2(traceLength));
	if(argc==7){cout<<" sampleFile:stdout\n";} else{ cout<<" sampleFile:"<<string(argv[7])<<endl;}
	printDoubleLine();
	if (npt) cout<<"Tracelength is not a power of two!\n"; else cout<<"Tracelength is a power of two!\n";
	t = new TraceSampler(iFName,mFName,traceLength, checkTrace, useReachableSet,startTime);
	t->buildDataStructures();
	double sampleTime = cpuTimeTotal(); uint32_t nTSteps = 10, nT = 1;
	
	if(argc==7){
		if (npt){
			cout<<"WARNING: tracelength is not a power of 2. Printing counter values as part of each state." 
			"To get traces without the counter part, write traces to file by providing a sample file in arguments.\n";
		}
		for (uint32_t i =1 ;i<=numTraces;i++){
			Trace& tr = t->drawSample();
			cout<<"Priting trace "<<i<<" out of "<<numTraces<<" ..\n";
			for (uint32_t j = 0; j<traceLength+1; j++){
				State* s = tr.getState(j);
				s->printState();
				printf("\t");
			}
			printf("\n");
			if(i==nT){
				string ss = std::to_string(i)+"-samples";
				printTimeTaken(ss,cpuTimeTotal()-sampleTime,2);
				nT *= nTSteps;
			}
		}
	} else{
		FILE* ofp = fopen(argv[7],"w");
		if (npt){
			cout<<"WARNING: tracelength is not a power of 2. Not printing counter values as part of each state." 
			"To get traces with the counter part, omit sample file in arguments, to get traces on stdout.\n";
			fprintf(ofp,"%s %u %u %u\n",iFName, traceLength,t->getNumStateVars()-n,numTraces);	
		} else{
			fprintf(ofp,"%s %u %u %u\n",iFName, traceLength,t->getNumStateVars(),numTraces);
		}
		for (uint32_t i =1 ; i<=numTraces; i++){
			Trace& tr = t->drawSample();
			if (((i-1)%(std::max((numTraces/10),1u)))==1) cout<<"Writing trace "<<i<<" out of "<<numTraces<<" to file..\n";
			t->writeTraceToFile(ofp);
			if(i==nT){
				string ss = std::to_string(i)+"-samples";
				printTimeTaken(ss,cpuTimeTotal()-sampleTime,2);
				nT *= nTSteps;
			}
		}
		fclose(ofp);
	}
	string ss = "Sampletime-Total";
	printTimeTaken(ss,cpuTimeTotal()-sampleTime,2);
	printDoubleLine();
	cout<<endl<<endl<< "sample_trace ended at ";
	print_wall_time(); cout<<endl;
	printTimeTaken(string("Total"),(cpuTimeTotal()-startTime),1);
}
