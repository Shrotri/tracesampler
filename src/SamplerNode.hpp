#ifndef SAMPLERNODE_H_
#define SAMPLERNODE_H_

#include <vector>
#include <cmath>
#include "NumType.hpp"
extern "C"{
	#include "cudd.h"
}

using std::vector;


class SamplerNode{
	public:	
		void addParent(SamplerNode* parent, bool tE, uint32_t lvlDiff);
		void append(SamplerNode* other);
		uint32_t getCompressedLevel();
		void printParentCmpLvls();
	private:
		friend class SamplerNodeFactory;
		//cant have a vector of references by C++ design. Can use reference_wrapper, but will go with pointers instead
		vector<SamplerNode*> parents;
		vector<bool> thenElse; //stores whether the current node is reached through then / else edge from parent
		// thenElse can enable DdNodes in Cudd corresponding to the SamplingStructure to be deleted / dealloced
		//after the SamplingStructure has been constructed, for saving space. NO! this might not work since the DdNodes
		//may be shared with other ADDs where they are part of s or s''. Despite that, it may not be a big overhead to
		//store thenElse since vector of bools is compressed
		NumType* rootPathCount;
		unsigned compressedLevel;

		SamplerNode( vector<SamplerNode*> parents_, NumType* rootPathCount_, uint32_t compressedLevel_,
			vector<bool> thenElse_);
		SamplerNode( SamplerNode* parent_, NumType* rootPathCount_, uint32_t compressedLevel_,
			bool thenElse_);
		SamplerNode( NumType* rootPathCount_, uint32_t compressedLevel_);
		SamplerNode();
		SamplerNode(const SamplerNode& other);
		~SamplerNode();
};


#endif
