/***************************************************************************
Copyright (c) 2019, Aditya A. Shrotri, Rice University, USA.
Copyright (c) 2006-2011, Armin Biere, Johannes Kepler University, Austria.
Copyright (c) 2011, Siert Wieringa, Aalto University, Finland.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/
#ifndef STIMAIG_H_
#define STIMAIG_H_

extern "C" {
  #include "aiger.h"
}
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include <ctype.h>
#include <vector>

using std::vector;
class StimAig {
	public:
		StimAig(char* fileName);
		~StimAig();
		void stimulate(const vector<bool>& inputVec, const vector<bool>& stateVec,
		 vector<bool>& resL, vector<bool>& resO);
		int getNumInputs();
		int getNumLatches();
		int getNumPOs();
	private:
		unsigned char *current;
		aiger *model;
		const char* error;
		
		bool deref (unsigned lit);
};

#endif