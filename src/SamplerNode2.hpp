#ifndef SAMPLERNODE2_H_
#define SAMPLERNODE2_H_

#include <vector>
#include <cmath>
#include "cudd.h"
#include "NumType.hpp"

using std::vector;


class SamplerNode{
	public:	
		SamplerNode(DdNode* node_, vector<SamplerNode*> parents_, NumType* rootPathCount_, uint32_t compressedLevel_,
			vector<bool> thenElse_): 
		node(node_), compressedLevel(compressedLevel_), rootPathCount(rootPathCount_), parents(parents_), 
			thenElse(thenElse_){}
		SamplerNode(DdNode* node_, SamplerNode* parent_, NumType* rootPathCount_, uint32_t compressedLevel_,
			bool thenElse_): 
		node(node_), compressedLevel(compressedLevel_), rootPathCount(rootPathCount_), parents(1,parent_), 
			thenElse(1,thenElse_){}
		SamplerNode(DdNode* node_, NumType* rootPathCount_, uint32_t compressedLevel_):
			node(node_), compressedLevel(compressedLevel_), rootPathCount(rootPathCount_){}
		~SamplerNode(){
			delete rootPathCount;
		}
		DdNode* node;
		//cant have a vector of references by C++ design. Can use reference_wrapper, but will go with pointers instead
		vector<SamplerNode*> parents;
		vector<bool> thenElse; //stores whether the current node is reached through then / else edge from parent
		// thenElse can enable DdNodes in Cudd corresponding to the SamplingStructure to be deleted / dealloced
		//after the SamplingStructure has been constructed, for saving space. NO! this might not work since the DdNodes
		//may be shared with other ADDs where they are part of s or s''. Despite that, it may not be a big overhead to
		//store thenElse since vector of bools is compressed
		NumType* rootPathCount;
		unsigned compressedLevel;	
};


#endif
