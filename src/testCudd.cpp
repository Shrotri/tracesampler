#include <iostream>
#include "cudd.h"
#include "cuddInt.h"
#include "cuddObj.hh"
#include "RandomBits.hpp"

using std::cout;
using std::endl;
using std::string;

void cudd(){
	DdManager* dd = Cudd_Init(0,0,CUDD_UNIQUE_SLOTS,CUDD_CACHE_SLOTS,0); /* Initialize a new BDD manager. */
	DdNode* var = Cudd_addIthVar(dd,2);
	Cudd_Ref(var);
	DdNode* t = Cudd_T(var);
	DdNode* e = Cudd_E(var);
	cout<<"then value: "<<Cudd_V(t)<<" else val: "<<Cudd_V(e)<<endl;
	cout<<"Doing cmpl..\n";
	var = Cudd_addCmpl(dd,var);
	Cudd_Ref(var);
	t = Cudd_T(var);
	e = Cudd_E(var);
	cout<<"then value: "<<Cudd_V(t)<<" else val: "<<Cudd_V(e)<<endl;
	DdNode* res = Cudd_addIte(dd,Cudd_addIthVar(dd,0),Cudd_addIthVar(dd,1),var);
	Cudd_Ref(res);
	printf("level of ite = %d\n",Cudd_ReadPerm(dd,res->index));
}
DdNode* makeCube(uint32_t i, DdManager* dd, DdNode** varList, uint32_t n){
	DdNode *temp, *cube = Cudd_ReadOne(dd);
	Cudd_Ref(cube);
	for (uint32_t j = 0; j<n; j++){
		uint32_t k = i%2;
		DdNode* var;
		bool flag = false;
		if(k==1) {
			var = varList[j];
		}
		else{
			var = Cudd_addCmpl(dd,varList[j]);
			Cudd_Ref(var);
			flag = true;
		}
		cube = Cudd_addApply(dd,Cudd_addTimes,temp = cube, var);
		if(flag) {Cudd_RecursiveDeref(dd,var);}
		Cudd_Ref(cube);	Cudd_RecursiveDeref(dd,temp);
		i = i/2;
	}
	return (cube);
}
void dumpDot(DdManager *dd,DdNode *f, string fname){
	DdNode* g = f;
	printf("Writing dot file %s\n",fname.c_str());
	FILE* fpd = fopen(fname.c_str(),"w");
	if (fpd==NULL){
		printf("Could not open dot file %s. Exiting..\n",fname.c_str());
		exit(1);
	}
    Cudd_DumpDot( dd, 1, &g, NULL, NULL, fpd);
	fclose(fpd);
}
void test1(char** argv){
	printf("%lu %lu %lu %lu\n",sizeof(double_t),sizeof(long double),sizeof(double),sizeof(float));
	RandomBits rb;
	rb.SeedEngine();
	uint32_t numTraces = strtol(argv[1],NULL,10);
	uint32_t numBits = strtol(argv[2],NULL,10);
	FILE* ofp = fopen(argv[3],"w");
	fprintf(ofp,"%s %u %u %u\n","testCudd", 8,8,numTraces);
	for (uint32_t i =0 ;i<numTraces;i++){
		if ((i%(std::max((numTraces/10),1u)))==1) cout<<"Writing trace "<<i<<" out of "<<numTraces<<" to file..\n";
		string s = rb.GenerateRandomBits(numBits);
		fprintf(ofp,"%s\n",s.c_str());
	}
	fclose(ofp);
}
int main(int argc, char** argv){
	DdManager* dd = Cudd_Init(0,0,CUDD_UNIQUE_SLOTS,CUDD_CACHE_SLOTS,0);
	DdNode* x0 = Cudd_addNewVar(dd); DdNode* x1 = Cudd_addNewVar(dd); DdNode* x0p = Cudd_addNewVar(dd); DdNode* x1p =Cudd_addNewVar(dd);
	Cudd_Ref(x0); Cudd_Ref(x1); Cudd_Ref(x0p); Cudd_Ref(x1p);
	DdNode* vl[8];
	vl[0] = x0; vl[1] = x1; vl[2] = x0p; vl[3] = x1p;
	DdNode* t0 = Cudd_ReadZero(dd);
	Cudd_Ref(t0);
	Cudd_AutodynEnable(dd,CUDD_REORDER_EXACT);

	DdNode* ass = makeCube(7,dd,vl,4);
	DdNode* temp;
	t0 = Cudd_addApply(dd,Cudd_addOr,temp = t0,ass);
	Cudd_Ref(t0); Cudd_RecursiveDeref(dd,temp); Cudd_RecursiveDeref(dd,ass);

	ass = makeCube(5,dd,vl,4);
	t0 = Cudd_addApply(dd,Cudd_addOr,temp = t0,ass);
	Cudd_Ref(t0); Cudd_RecursiveDeref(dd,temp); Cudd_RecursiveDeref(dd,ass);

	ass = makeCube(14,dd,vl,4);
	t0 = Cudd_addApply(dd,Cudd_addOr,temp = t0,ass);
	Cudd_Ref(t0); Cudd_RecursiveDeref(dd,temp); Cudd_RecursiveDeref(dd,ass);

	ass = makeCube(6,dd,vl,4);
	t0 = Cudd_addApply(dd,Cudd_addOr,temp = t0,ass);
	Cudd_Ref(t0); Cudd_RecursiveDeref(dd,temp); Cudd_RecursiveDeref(dd,ass);

	ass = makeCube(8,dd,vl,4);
	t0 = Cudd_addApply(dd,Cudd_addOr,temp = t0,ass);
	Cudd_Ref(t0); Cudd_RecursiveDeref(dd,temp); Cudd_RecursiveDeref(dd,ass);

	ass = makeCube(0,dd,vl,4);
	t0 = Cudd_addApply(dd,Cudd_addOr,temp = t0,ass);
	Cudd_Ref(t0); Cudd_RecursiveDeref(dd,temp); Cudd_RecursiveDeref(dd,ass);

	vl[4] = Cudd_addNewVar(dd); Cudd_Ref(vl[4]); vl[5] = Cudd_addNewVar(dd); Cudd_Ref(vl[5]);
	DdNode* t0p = Cudd_addSwapVariables(dd,t0,vl,vl+2,2);
	Cudd_Ref(t0p);
	t0p = Cudd_addSwapVariables(dd,temp = t0p,vl,vl+4,2);
	Cudd_Ref(t0p); Cudd_RecursiveDeref(dd,temp);

	DdNode* t1 = Cudd_addApply(dd,Cudd_addTimes,t0,t0p);
	Cudd_Ref(t1); Cudd_RecursiveDeref(dd,t0p);
	
	DdNode* vcube = Cudd_addApply(dd,Cudd_addTimes,vl[2],vl[3]);
	Cudd_Ref(vcube);
	DdNode* t1two = Cudd_addExistAbstract(dd,t1,vcube);
	Cudd_RecursiveDeref(dd,vcube);
	
	vl[6] = Cudd_addNewVar(dd); Cudd_Ref(vl[6]); vl[7] = Cudd_addNewVar(dd); Cudd_Ref(vl[7]);
	DdNode* t1twop = Cudd_addSwapVariables(dd,t1two,vl,vl+4,2);
	Cudd_Ref(t1twop);
	t1twop = Cudd_addSwapVariables(dd,temp = t1twop,vl,vl+6,2);
	Cudd_Ref(t1twop); Cudd_RecursiveDeref(dd,temp);

	DdNode* t2 = Cudd_addApply(dd,Cudd_addTimes,t1two,t1twop);
	Cudd_Ref(t2); Cudd_RecursiveDeref(dd,t1twop); Cudd_RecursiveDeref(dd,t1two);
	
	dumpDot(dd,t0,string("scratch/t0.dot"));
	dumpDot(dd,t1,string("scratch/t1.dot"));
	dumpDot(dd,t2,string("scratch/t2.dot"));
}