#ifndef GMPNUMTYPE_H_
#define GMPNUMTYPE_H_

#include "RandomBits.hpp"
#include "NumType.hpp"
#include "gmp.h"
#include "gmpxx.h"

class MPZNumType : public NumType{
	public:
		static void initRandom(RandomBits* rb, uint32_t nBits);
		
		MPZNumType(double_t);
		MPZNumType(mpz_class num_);
		~MPZNumType() override;
		MPZNumType* copy() override;
		MPZNumType* multiply(NumType*) override;
		void multiplySelf(NumType*) override;
		void multiplyByDoubleSelf(double_t) override;
		MPZNumType* add(NumType*) override;
		void addSelf(NumType*) override;
		MPZNumType* multiplyByPowerOfTwo(uint32_t) override;
		void multiplyByPowerOfTwoSelf(uint32_t) override;
		MPZNumType* divideBy(NumType*) override;
		void divideBySelf(NumType*) override;
		void addPowerOfTwoSelf(uint32_t) override;
		void addProductWithPowerOfTwoSelf(NumType*, uint32_t) override;
		void set(NumType* other) override;
		void setPowerOfTwo(uint32_t) override;
		void setRandom(NumType* other,RandomBits *) override;

		MPZNumType* getRandom(RandomBits *) override;

		void print() override;

		bool lessThan(NumType*) override;
		bool isPositive() override;
		bool isNonNegative() override;
		bool isCloseToOne(double_t) override;
		bool isZero() override;
	private:
		mpz_class number;
		static gmp_randstate_t randState;
		static bool initDone;
};

#endif