#ifndef TSAMPLERNODE_H_
#define TSAMPLERNODE_H_

#include <vector>
#include <cmath>
#include <cstdint>
#include <cstdio>

extern "C"{
	#include "cudd.h"
}

using std::vector;


template <class Num_Type>
class SamplerNode{
	public:	
		void addParent(SamplerNode<Num_Type>* parent, bool tE, uint32_t lvlDiff);
		void append(SamplerNode<Num_Type>* other);
		uint32_t getCompressedLevel();
		void printParentCmpLvls();
	private:
		template <class Num_Type2>
		friend class SamplerNodeFactory;
		//cant have a vector of references by C++ design. Can use reference_wrapper, but will go with pointers instead
		vector<SamplerNode<Num_Type>*> parents;
		vector<bool> thenElse; //stores whether the current node is reached through then / else edge from parent
		// thenElse can enable DdNodes in Cudd corresponding to the SamplingStructure to be deleted / dealloced
		//after the SamplingStructure has been constructed, for saving space. NO! this might not work since the DdNodes
		//may be shared with other ADDs where they are part of s or s''. Despite that, it may not be a big overhead to
		//store thenElse since vector of bools is compressed
		Num_Type rootPathCount;
		unsigned compressedLevel;

		SamplerNode( vector<SamplerNode<Num_Type>*> parents_, Num_Type rootPathCount_, uint32_t compressedLevel_,
			vector<bool> thenElse_);
		SamplerNode( SamplerNode<Num_Type>* parent_, Num_Type rootPathCount_, uint32_t compressedLevel_,
			bool thenElse_);
		SamplerNode( Num_Type rootPathCount_, uint32_t compressedLevel_);
		SamplerNode();
		SamplerNode(const SamplerNode<Num_Type>& other);
		~SamplerNode();
};

template class SamplerNode<double_t>;
template class SamplerNode<long double>;

#endif
