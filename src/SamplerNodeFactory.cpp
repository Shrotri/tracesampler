#include "SamplerNodeFactory.hpp"
#include <cassert>
#include <algorithm>

using std::cout; using std::endl;

bool compareNumType(NumType*a, NumType* b){return a->lessThan(b);}

SamplerNodeFactory* SamplerNodeFactory::getFactory(uint32_t memLimit, SamplerNodeFactory::NUMTYPE SamplerNodeType_){
	static SamplerNodeFactory instance(SamplerNodeType_);
	return &instance;
}
void SamplerNodeFactory::forgetAllUsedNodes(){
	usedNodes.clear();
}
void SamplerNodeFactory::recycleAllUsedNodes(){
	freeNodes.insert(freeNodes.end(),usedNodes.begin(),usedNodes.end());
	usedNodes.clear();
}
SamplerNode* SamplerNodeFactory::createSamplerNode(){
	//TODO
}
void SamplerNodeFactory::recycleSamplerNode(SamplerNode* node){
	usedNodes.erase(node);
	freeNodes.push_back(node);
}

SamplerNode* SamplerNodeFactory::createRoot(uint32_t cmprsdLvl){
	SamplerNode* tmp = getFreeNode();
	if(tmp!=NULL){
		tmp->parents.clear(); tmp->thenElse.clear();
		
		tmp->compressedLevel = cmprsdLvl;
		tmp->rootPathCount->setPowerOfTwo(cmprsdLvl);
	} else{
		NumType* rpc = one->multiplyByPowerOfTwo(cmprsdLvl);
		tmp = new SamplerNode(rpc,cmprsdLvl);
	}
	usedNodes.insert(tmp);
	return tmp;
}

SamplerNode* SamplerNodeFactory::createChild(SamplerNode* parent, bool tE, uint32_t lvlDiff, uint32_t cmprsdLvl){
	SamplerNode* tmp = getFreeNode();
	if(tmp!=NULL){
		tmp->parents.clear(); tmp->thenElse.clear();
		
		tmp->parents.push_back(parent); tmp->thenElse.push_back(tE);
		tmp->compressedLevel = cmprsdLvl;
		tmp->rootPathCount->set(parent->rootPathCount);
		tmp->rootPathCount->multiplyByPowerOfTwoSelf(lvlDiff);
	} else{
		NumType* rpc = parent->rootPathCount->multiplyByPowerOfTwo(lvlDiff);
		tmp = new SamplerNode(parent,rpc,cmprsdLvl,tE);
	}
	usedNodes.insert(tmp);
	return tmp;
}

SamplerNode* SamplerNodeFactory::copy(SamplerNode* other){
	SamplerNode* tmp = getFreeNode();
	if(tmp!=NULL){
		tmp->parents=other->parents; tmp->thenElse=other->thenElse;
		
		tmp->compressedLevel = other->compressedLevel;
		tmp->rootPathCount->set(other->rootPathCount);
	} else{
		tmp = new SamplerNode(other->parents,other->rootPathCount->copy(),other->compressedLevel,other->thenElse);
	}
	usedNodes.insert(tmp);
	return tmp;
}

SamplerNode* SamplerNodeFactory::getFreeNode(){
	if (freeNodes.empty()) return NULL;
	else {
		SamplerNode* front = freeNodes.front();
		freeNodes.pop_front();
		return front;
	}
}

void SamplerNodeFactory::clearSentinel(uint32_t cmprsdLvl){
	sentinel.parents.clear(); sentinel.thenElse.clear();
	sentinel.rootPathCount->set(zero);
	sentinel.compressedLevel = cmprsdLvl;
}

void SamplerNodeFactory::ensureSamplingArraySize(uint32_t minSize){
	uint32_t currSize = samplingArray.size();
	while(currSize<minSize){
		samplingArray.push_back(one->copy());
		//sA2.push_back(0); 
		currSize++;
	}
}

std::pair<std::pair<SamplerNode*,bool>,uint32_t> SamplerNodeFactory::sampleParent(SamplerNode* nodeSN, vector<double_t> weights, RandomBits *rb){
	ensureSamplingArraySize(nodeSN->parents.size());
	runningTotal->set(zero);
	// double rT = 0;
	{ //braces to limit definition of i. i might be used later in loops and don't want double definition
		uint32_t i = 0;
		for (auto& parent : nodeSN->parents){
			samplingArray[i]->set(parent->rootPathCount);
			// sA2[i] = ((DoubleNumType*)parent->rootPathCount)->number;
			if(!weights.empty()) samplingArray[i]->multiplyByDoubleSelf(weights.at(i));
			// if(!weights.empty()) sA2[i] *= (weights.at(i));
			assert(samplingArray[i]->isPositive());
			int levelDiff = nodeSN->compressedLevel-parent->compressedLevel;
			// printf("levels: %u %u levelDiff: %d\n",nodeSN->compressedLevel,parent->compressedLevel,levelDiff);
			assert(levelDiff>=1);
			if ( levelDiff >= 2){
				samplingArray[i]->multiplyByPowerOfTwoSelf(levelDiff-1);
				// sA2[i] *= pow(2,levelDiff-1);  
				//-1 because we want to weight eachnode at level just above currNode ('parent' in diag below) 
				//by the number of paths it feeds into the currNode.
				//So if diff in levels is 2 (between 'grandparent' and 'currNode' below),
				//then multiplier should be 2^1, if diff is 3 then multiplier should be 2^2 and so on
				// grandparent -> parent => currNode   -> means one outedge, => means both outedges
			}
			runningTotal->addSelf(samplingArray[i]);
			// rT += sA2[i];
			samplingArray[i]->set(runningTotal);
			// sA2[i] = rT;
			//samplingArray[i]->print(); cout<<" "<<std::flush;
			i++;
		}
	}
	r->setRandom(runningTotal,rb);
	// double rn = rT*rb->getRandReal(std::uniform_real_distribution<double_t>(0,1));
	
	//lower_bound returns position of first element in array that is greater than or equal to r
	vector<NumType*>::iterator pos = std::lower_bound(samplingArray.begin(),samplingArray.begin()+nodeSN->parents.size(),r, compareNumType);
	// vector<double>::iterator pos = std::lower_bound(sA2.begin(),sA2.begin()+nodeSN->parents.size(),rn);
	// uint32_t low = pos - sA2.begin();
	uint32_t low = pos - samplingArray.begin();
	//assert(nodeSN->parents.size()==nodeSN->thenElse.size());
	return std::make_pair(std::make_pair(nodeSN->parents.at(low),nodeSN->thenElse.at(low)),low);
}

bool SamplerNodeFactory::noUsedNodes(){
	return usedNodes.empty();
}

SamplerNodeFactory::SamplerNodeFactory(SamplerNodeFactory::NUMTYPE SamplerNodeType_): SamplerNodeType(SamplerNodeType_),
	sentinel(NULL,1){
	
	switch (SamplerNodeType){
		case SamplerNodeFactory::dbl :
			zero = new DoubleNumType(0.0);
			one = new DoubleNumType(1.0);
			break;
		case SamplerNodeFactory::long_dbl :
			zero = new LongDoubleNumType(0.0);
			one = new LongDoubleNumType(1.0);
			break;
		case SamplerNodeFactory::gmpz :
			zero = new MPZNumType(0);
			one = new MPZNumType(1);
			break;
		default:
			cout<<"Unrecognized SamplerNode Type: "<<SamplerNodeType<<" exiting..\n";
			exit(0);
			break;
	}
	sentinel.rootPathCount = zero->copy();
	runningTotal = zero->copy();
	r = zero->copy();
}