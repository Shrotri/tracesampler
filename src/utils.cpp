#include "mtr/mtr.h"
#include "utils.hpp"
#include "cuddObj.hh"
extern "C"{
	#include "dddmp.h"
	#include "dddmpInt.h"
	#include "mtr/mtrInt.h"
}


using std::cout;
using std::endl; 
using std::ifstream;
using std::string;
using std::stringstream;

/*it maybe necessary to add contiguity constraints during readDD since once reordering takes place, it can be hard
 to specify the range of vars to enforce contiguity over. Documentation is also unclear about whether range of vars
 in funcs like cudd_maketreenode all correspond to indices or ordering (even though low parameter is an index).
 Therefore easiest way is to add contiguity constraints before any reordering takes place when indices and orders 
 match up */
/*Contiguity constraints if any are recorded by writeDD in meta file. if useContiguity is true, then the constraints 
  (if present) in meta file are added after creating manager. if useContiguity is false then no constraints are added
  even if present in meta file. Contiguity is also an out parameter. it is set to true if contiguity constraints were
  found in the file and added, or false o/w. it is useful if its i/p value was true, but no constraints were found in
  the meta file, which would make its o/p value false*/
void readDD(char* ddFileName, char* ddMetaFileName, DdManager** man, DdNode** dd, uint32_t& numLatches, 
	uint32_t& numInputs, uint32_t& nStateVars, uint32_t& varStartIndex, bool* useContiguity, string* stateSeq){
	
	cout<<"Opening file "<<ddFileName<<std::flush;
	FILE* fp = fopen(ddFileName,"r");
	if(fp == NULL){
		cout<<"Error opening input file. Exiting..\n";
		exit(1);
	}
	cout<<".. File opened.\nReading header now.. "<<std::flush;
	Dddmp_DecompType ddType;
	int retValue, nRoots, nVars, nSuppVars;
	int *tmpVarIds = NULL;
	int *tmpVarAuxIds = NULL;
	int *tmpVarComposeIds = NULL;
	char **tmpOrderedVarNames = NULL;
	char **tmpSuppVarNames = NULL;

	retValue = Dddmp_cuddHeaderLoad (&ddType, &nVars, &nSuppVars,
	&tmpSuppVarNames, &tmpOrderedVarNames, &tmpVarIds, &tmpVarComposeIds,
	&tmpVarAuxIds, &nRoots, ddFileName, fp);
	fclose(fp);
	if(retValue == 0){
		cout<<"Error reading header of file. Exiting..\n";
		exit(1);
	}
	cout<<"Read header of file. DD has "<<nVars<<" variables and "<<nSuppVars<<" supportvars.. "<<std::flush;
	if(ddType==DDDMP_ADD){
		cout<<"File detected to be ADD\n";
	} else if (ddType==DDDMP_BDD){
		cout<<"File detected to be BDD\n";
	} else {
		cout<<"File type not recognized: "<<ddType<<" Exiting..\n";
		exit(1);
	}
	cout<<"Reading meta file.. "<<std::flush;
	ifstream fpm;
	fpm.open(ddMetaFileName);
	string line;
	char mode;
	if (getline(fpm, line)){
		stringstream sline(line);
		sline >> mode;
	} else{
		cout<<"Could not read meta file (first line). Exiting..\n";
		exit(1);
	}
	if (mode=='a' || mode =='A'){
		mode = 'A';
		cout<<"Meta says DDFile is encoded as Text.. "<<std::flush;
	} else if (mode == 'b' || mode == 'B'){
		mode = 'B';
		cout<<"Meta says DDFile is encoded as Binary.. "<<std::flush;
	} else{
		printf("Mode not recognized: %c (should be 'A' or 'B'). Defaulting to 'A'(text) for ADDs and"
		" 'B' binary for BDDs..\n", mode);
		if (ddType==DDDMP_ADD){
			mode = 'A';
		} else{
			mode = 'B';
		}
	}
	if (getline(fpm, line)){
		stringstream sline(line);
		sline >> numInputs;
		sline >> numLatches;
		nStateVars = numLatches;
		varStartIndex = numInputs - numLatches;
		assert(nVars == nStateVars + nStateVars + varStartIndex);
	} else{
		cout<<"Could not read meta file (2nd line). Exiting..\n";
		exit(1);
	}
	int32_t* invPerm = new int32_t[nVars];
	if (getline(fpm, line)){
		stringstream sline(line);
		for (int i =0; i <nVars; i++ ){
			sline>>invPerm[i];
		}
	} else{
		cout<<"Could not read meta file (3rd line). Exiting..\n";
		exit(1);
	}
	string ddStatsMeta;
	if (getline(fpm, line)){
		ddStatsMeta = line;
		ddStatsMeta.append("\n");
	} else{
		cout<<"Could not read meta file (4th line). Exiting..\n";
		exit(1);
	}
	if (getline(fpm, line)){
		*stateSeq = line;
	} else{
		cout<<"Could not read meta file (5th line). Exiting..\n";
		exit(1);
	}
	uint32_t numContiguityRanges;
	if(getline(fpm, line)){
		assert(line[0]=='C' && line[1]=='R');
		stringstream sline(line.substr(2));
		sline >> numContiguityRanges;
		assert(numContiguityRanges>=0);
	} else {
		cout<<"Could not read meta file (Contiguity ranges header). Exiting..\n";
		exit(1);
	}
	vector<std::pair<std::pair<uint32_t,uint32_t>,uint32_t>> contiguityRanges(numContiguityRanges);
	for(uint32_t i = 0; i<numContiguityRanges; i++){
		if(getline(fpm, line)){
			stringstream sline(line);
			uint32_t low, rangeSize, rangeType;
			sline >> low;
			sline >> rangeSize;
			sline >> rangeType;
			contiguityRanges.at(i) = std::make_pair(std::make_pair(low,rangeSize),rangeType);
		} else {
			cout<<"Could not read meta file. Expected "<<numContiguityRanges<<" contiguity ranges but"
			" found only "<<i<<". Exiting..\n";
			exit(1);
		}	
	}
	fpm.close();
	cout<<"Processed meta file.\nPreparing manager for reading.. "<<std::flush;
	*man = Cudd_Init(0,0,CUDD_UNIQUE_SLOTS,CUDD_CACHE_SLOTS,0); /* Initialize a new BDD manager. */
	Cudd_AutodynDisable(*man); // disable for now while reading bdd/add
	//ADD variables are different from BDD vars. So create add vars explicitly
	DdNode* v;
	if(ddType==DDDMP_ADD){
		for (int i = 0; i< nVars; i++){
			v = Cudd_addNewVar(*man);
			Cudd_Ref(v);
		}
	} else{
		for (int i = 0; i< nVars; i++){
			v = Cudd_bddNewVar(*man);
		}
	}
	if (*useContiguity){
		cout<<"I/P val of useContiguity is True. Adding "<<numContiguityRanges<<" contiguity constraints found in meta.. "<<std::flush;
		for (uint32_t i = 0; i<numContiguityRanges; i++){
			Cudd_MakeTreeNode(*man,contiguityRanges.at(i).first.first,contiguityRanges.at(i).first.second,contiguityRanges.at(i).second);
		}
		if(numContiguityRanges == 0){
			cout<<"Since no contiguity ranges found in meta file, setting value of useContiguity to false\n";
			*useContiguity = false;
		}
	} else{
		cout<<"useContiguity is false. so not bothering with contiguity\n";
	}
	if(!Cudd_ShuffleHeap(*man, invPerm)){
		cout<<"Reordering variables failed. Exiting..\n";
		exit(1);
	}
	dddmpVarInfo_t varInfo;
	fp = fopen(ddFileName,"r");
	cout<<"Reading dd.. "<<std::flush;
	if (ddType==DDDMP_ADD){
		*dd = Dddmp_cuddAddLoad (*man, DDDMP_VAR_MATCHIDS, varInfo.orderedVarNames,
			varInfo.varIdsAll, varInfo.varComposeIdsAll, mode, ddFileName, fp);
		if (*dd==NULL) {
			fprintf (stderr, "Dddmp Test Error : %s is not loaded from file. Exiting..\n",
			ddFileName);
			exit(1);
		}
		Cudd_Ref(*dd);
		cout<<"ADD Read successfully!\n";
	} else {
		DdNode* bdd = Dddmp_cuddBddLoad (*man, DDDMP_VAR_MATCHIDS, varInfo.orderedVarNames,
			varInfo.varIdsAll, varInfo.varComposeIdsAll, mode, ddFileName, fp);
		if (bdd==NULL) {
			fprintf (stderr, "Dddmp Test Error : %s is not loaded from file. Exiting..\n",
			ddFileName);
			exit(1);
		}
		Cudd_Ref(bdd);
		cout<<"BDD Read successfully!\n";
		*dd = bdd;
	}
	string ddStatsActual;
	if(Cudd_StringSummary(*man,*dd,Cudd_SupportSize(*man, *dd),&ddStatsActual)==0){
		cout<<"Error getting string summary of generated dd. Exiting..\n";
		cout<<ddStatsMeta<<ddStatsActual<<"\n";
		exit(1);
	} 
	if (ddStatsActual.compare(ddStatsMeta)==0){
		cout<<"Sanity check passed! ddStats are same in meta file and the actual loaded dd. ddStats:"<<std::flush;
		cout<<ddStatsMeta;
	} else{
		cout<<"Sanity check failed! ddStats are different in meta file and loaded bdd. Exiting..\n";
		cout<<ddStatsMeta<<ddStatsActual<<"\n";
		exit(1);
	}
	fclose(fp);
	printPerm(*man);
}

void writeDDToFile(DdManager *ddMgr, char *ddname, DdNode *f, Dddmp_DecompType ddType, char **varnames, int *auxids, 
	int mode, Dddmp_VarInfoType varinfo, char *outFile, uint32_t ni, uint32_t nl, const char* stateSequence,
	vector<std::pair<std::pair<uint32_t,uint32_t>,uint32_t>>& contiguityRanges, vector<string> comments){
	int retVal;

	if (mode=='A'||mode=='a'){
		mode = 'A';
	}
	else if (mode =='B' || mode =='b'){
		mode = 'B';
	}
	else{
		printf("Mode %c not recognized (only A/B allowed).\n",mode);
		if (ddType == DDDMP_ADD){
			mode = 'A';
			printf("Since ddType is ADD, setting mode to A (text) by default."
			" Binary not supported for ADDs in current version of DDDMP\n");
		} else{
			mode = 'B';
			printf("Since ddType is BDD, setting mode to B (binary) by default.\n");
		}	
	}
	if(ddType == DDDMP_ADD){
		if(mode!=DDDMP_MODE_BINARY) printf("WARNING: ADDs cannot be written in binary format in"
		" current version of DDDMP. Attempting anyway since explicity requested..\n");
		retVal = Dddmp_cuddAddStore(ddMgr,ddname, f, varnames, auxids, mode, varinfo, outFile, NULL);
	} else{
		retVal = Dddmp_cuddBddStore(ddMgr,ddname, f, varnames, auxids, mode, varinfo, outFile, NULL);
	}
	if (retVal == 0){  //DDDMP_FAILURE is 0
		printf("DDDMP store DD failed. Exiting..\n");
		exit(1);
	}
	//write variable size and indices to meta file
	string mFname;
	mFname.append(outFile);
	mFname.append(".meta");
	printf("Writing meta information to file %s",mFname.c_str());
	FILE* fpm = fopen(mFname.c_str(),"w");
	if (fpm==NULL){
		printf("Could not open meta file %s. Exiting..\n",mFname.c_str());
		exit(1);
	}
	retVal = fprintf(fpm,"%c\n",mode);
	if(retVal==EOF){
		printf("Could not write meta data (mode binary/text): %c. Exiting..\n",mode);
		exit(1);
	}
	retVal = fprintf(fpm,"%u %u\n",ni, nl);
	if(retVal==EOF){
		printf("Could not write meta data: %u %u.\n Exiting..\n",ni, nl);
		exit(1);
	}
	printf("Writing invPerm order to meta file\n");
	for ( int i = 0; i < Cudd_ReadSize(ddMgr); i++){
		fprintf(fpm,"%d ",ddMgr->invperm[i]);
	}
	fprintf(fpm,"\n");
	printf("Writing dd stats to meta..\n");
	Cudd_SetStdout(ddMgr,fpm);
	Cudd_PrintSummary(ddMgr,f,Cudd_SupportSize(ddMgr,f),0);
	printf("Writing state sequence %s..\n",stateSequence);
	fprintf(fpm,"%s\n",stateSequence);
	printf("Writing contiguity ranges..\n");
	fprintf(fpm,"CR %lu\n",contiguityRanges.size());
	for(uint32_t i = 0; i<contiguityRanges.size();i++){
		fprintf(fpm,"%u %u %u\n",contiguityRanges.at(i).first.first,contiguityRanges.at(i).first.second,
			contiguityRanges.at(i).second);
	}
	fclose(fpm);
	if(!comments.empty()){
		cout<<"Writing "<<comments.size()<<" comments to meta..\n";
		std::ofstream out(mFname,std::ofstream::app);
		for (auto& comment: comments){
			out << "c "<<comment<<"\n";
		}
		out.close();
	} else{ cout<<"No comments passed.\n";}
	Cudd_SetStdout(ddMgr,stdout);
	cout<<"Not writing dot file.. "<<std::flush;
	/*
	mFname.clear();
	mFname.append(outFile);
	mFname.append(".dot");
	printf("Writing dot file %s\n",mFname.c_str());
	FILE* fpd = fopen(mFname.c_str(),"w");
	if (fpd==NULL){
		printf("Could not open dot file %s. Exiting..\n",mFname.c_str());
		exit(1);
	}
    Cudd_DumpDot( ddMgr, 1, &f, NULL, NULL, fpd);
	fclose(fpd);
	*/
}

void dumpDot(DdManager *dd,DdNode *f, string fname){
	DdNode* g = Cudd_BddToAdd(dd,f);
	Cudd_Ref(g);
	printf("Writing dot file %s\n",fname.c_str());
	FILE* fpd = fopen(fname.c_str(),"w");
	if (fpd==NULL){
		printf("Could not open dot file %s. Exiting..\n",fname.c_str());
		exit(1);
	}
    Cudd_DumpDot( dd, 1, &g, NULL, NULL, fpd);
	fclose(fpd);
	Cudd_RecursiveDeref(dd,g);
}

void convertToADD(DdManager* man, DdNode** bdd, bool verbose){
		if (verbose) cout<<"Converting bdd to add..\n";
		DdNode* temp = Cudd_BddToAdd(man,*bdd);
		if(temp==NULL){
			fprintf (stderr, "Error convertring bdd to add. Exiting..\n");
			exit(1);
		}
		Cudd_Ref(temp);
		Cudd_RecursiveDeref(man,*bdd);
		*bdd = temp;
		if (verbose) cout<<"ADD stats (after conversion):\n";
		if (verbose) Cudd_PrintSummary(man,*bdd,Cudd_SupportSize(man, *bdd),1);
}

void switchStates(DdManager* man, DdNode** dd, Dddmp_DecompType ddType, DdNode** sVars, uint32_t nStateVars, uint32_t varStartIndex){
	/*
	aig2dd returns a DD with variable indices in the sequence originputs, next state =1 , current state = 0.
	To make things cleaner, we want variable indices to be in order originputs, current state 0, next state 1,
	state 2, state 4 ... state 2^n. So performing this swap before returning. 
	*/
	printf("Switching current state and next state vars to make sequential %p %p %p %p %d\n", man,*dd,&(sVars[0]),&(sVars[nStateVars]),nStateVars);
	DdNode* swtch_temp;
	if(ddType == DDDMP_ADD){
		swtch_temp= Cudd_addSwapVariables(man,*dd,&(sVars[0]),&(sVars[nStateVars]),nStateVars);
	}
	else{
		swtch_temp= Cudd_bddSwapVariables(man,*dd,&(sVars[0]),&(sVars[nStateVars]),nStateVars);
	}
	Cudd_Ref (swtch_temp);
	Cudd_RecursiveDeref(man, *dd);
	*dd = swtch_temp;
	cout<<"DD stats after switch:\n";
	Cudd_PrintSummary(man,*dd,Cudd_SupportSize(man, *dd),1);
}

DdNode* computeReachableSet(DdManager* dd, DdNode* bdd, DdNode* initialSet, DdNode* currStateVarsCube, 
	DdNode** sVars, uint32_t nStateVars, uint32_t traceLength, uint32_t maxDepth){
	//rs = is, rs = rs \/ swap(abstr0(rs * add0))
	//take care not to mix add and bdd vars
	//convert to add only after clipping add0 with RS
	cout<<"Computing reachable set.. \n";
	DdNode* reachableSet = initialSet, *temp, *temp2;
	Cudd_Ref(reachableSet); /*we dont want initialSet to get derefed since it will be used later. 
	So refed reachableset since it will get derefed below*/
	cout<<"Doing iterations: "<<std::flush;
	for(uint32_t i = 1; i<=traceLength;i++){
		if ((i%10==1)||(traceLength-i<10)) cout<<i<<" "<<std::flush;
		temp= Cudd_bddClippingAndAbstract(dd,reachableSet,bdd,currStateVarsCube,maxDepth,1);
		Cudd_Ref(temp);
		temp = Cudd_bddSwapVariables(dd,temp2 = temp,&(sVars[0]),&(sVars[nStateVars]),nStateVars);
		Cudd_Ref(temp); Cudd_RecursiveDeref(dd,temp2);
		reachableSet = Cudd_bddOr(dd,temp2=reachableSet,temp);
		Cudd_Ref(reachableSet); Cudd_RecursiveDeref(dd,temp2); Cudd_RecursiveDeref(dd,temp);
		//At end of ith iteration, reachableset is set of states reachable in <=i steps from initial state
	}
	cout<<"\nDone computing reachable set!\n";
	return reachableSet;
}

DdNode** computeExactReachableSets(DdManager* dd, DdNode** bdd, DdNode* initialSet, DdNode** sVars, 
	uint32_t nStateVars, uint32_t nSteps, uint32_t maxDepth, bool useUptoNSet){
	uint32_t n = log2(nSteps);
	cout<<"n:"<<n<<" nsteps:"<<nSteps<<" 1<<n:"<<(1<<n)<<endl;
	assert((1<<n)==nSteps); //check nSteps is a power of 2
	//compute for 2^n and not just tracelength steps since we may have to sample states from beyond tracelength
	//ers = is, ers = swap(abstr0(rs * add0))
	//take care not to mix add and bdd vars
	/*
	j l,r   i  
	0 1,0 = 0,2,4,6,..    i%2^l = 0        
	1 1,1 = 1,3,5,7,..    i%2^l = 2^(l-1)
	2 1,2 = 2,4,6,8,...   i>0 and i%2^l = 0
	3 2,0 = 0,4,8,12,...  i%2^l = 0
	4 2,1 = 2,6,10,14,..  i%2^l = 2^(l-1)
	5 2,2 = 4,8,12,...	  i>0 and i%2^l = 0
	6 3,0 = 0,8,16,...	  i%8 = 0
	7 3,1 = 4,12,20,...   i%8 = 2^(l-1)
	8 3,2 = 8,16,24,...   i>0 and i%2^l = 0
	9 4,0 = 0,16,32...
	10 4,1 = 8,24,40,...
	11 4,2 = 16,32,48...
	
	l = (j/3)+1  r = j%3 ,   
	restricting will need sets with r=0,1 (or just r=0 is also fine, but it will be less effective). 
	r=2 is not required but ostensibly computing it will not have major overhead since it differs from
	r=0 by just the initial state. so keeping it for now. see notes.txt for explanation of how restricting
	can cause spurious transitions to be added during iterative squaring which dictates what combinations of
	restrict sets are valid. 
	*/
	if (useUptoNSet){
		cout<<"Computing <=N set as well\n";
	} else{
		cout<<"Not computing <=N set\n";
	}
	DdNode *currStateVarCube = Cudd_ReadOne(dd) , *temp;
	Cudd_Ref(currStateVarCube);
	for(uint32_t i = 0; i<nStateVars;i++){
		currStateVarCube = Cudd_bddAnd(dd,temp = currStateVarCube, sVars[i]);
		Cudd_Ref(currStateVarCube); Cudd_RecursiveDeref(dd,temp);
	}
	DdNode** exactSets = (DdNode**)calloc(n*3+1,sizeof(DdNode*));
	for(uint32_t i =0; i<=3*n; i++){
		exactSets[i] = Cudd_ReadLogicZero(dd);
		Cudd_Ref(exactSets[i]);
	}
	cout<<"Computing exact reachable sets.. \n";
	DdNode* exactReachableSet = initialSet;
	Cudd_Ref(exactReachableSet); /*we dont want initialSet to get derefed since it will be used later. 
	So refed reachableset since it will get derefed below*/
	//0th iteration
	for(uint32_t i = 0; i<n; i++){
		exactSets[3*i] = Cudd_bddOr(dd,temp = exactSets[3*i],exactReachableSet); 
		Cudd_Ref(exactSets[3*i]); Cudd_RecursiveDeref(dd,temp);
		cout<<"exactSet["<<3*i<<"] summary :"<<std::flush;
		Cudd_PrintSummary(dd,exactSets[3*i],Cudd_SupportSize(dd, exactSets[3*i]),1);
	}
	if (useUptoNSet){
		exactSets[3*n] = Cudd_bddOr(dd,temp = exactSets[3*n],exactReachableSet); 
		Cudd_Ref(exactSets[3*n]); Cudd_RecursiveDeref(dd,temp);
		cout<<"<=N set summary :"<<std::flush;
		Cudd_PrintSummary(dd,exactSets[3*n],Cudd_SupportSize(dd, exactSets[3*n]),1);
	} 
	for(uint32_t i = 1; i<=nSteps;i++){
		exactReachableSet = Cudd_bddClippingAndAbstract(dd,temp = exactReachableSet,*bdd,currStateVarCube,maxDepth,1);
		Cudd_Ref(exactReachableSet); Cudd_RecursiveDeref(dd,temp);
		exactReachableSet = Cudd_bddSwapVariables(dd,temp = exactReachableSet,&(sVars[0]),&(sVars[nStateVars]),nStateVars);
		Cudd_Ref(exactReachableSet); Cudd_RecursiveDeref(dd,temp);
		cout<<"exactReachSet summary at iteration "<<i<<":"<<std::flush;
		Cudd_PrintSummary(dd,exactReachableSet,Cudd_SupportSize(dd, exactReachableSet),1);
		//At end of ith iteration, exactreachableset is set of states reachable in exactly i steps from initial state

		for (uint32_t j = 0; j<n*3; j++){
			uint32_t l = (j/3) + 1; uint32_t r = j%3;
			if (r%2==0 && i%(1<<l)==0){
				if (r==0 && i == nSteps) continue;
				exactSets[j] = Cudd_bddOr(dd,temp = exactSets[j],exactReachableSet);
				Cudd_Ref(exactSets[j]); Cudd_RecursiveDeref(dd,temp);
			} else if (r==1 && i%(1<<l)==(1<<(l-1))){
				exactSets[j] = Cudd_bddOr(dd,temp = exactSets[j],exactReachableSet);
				Cudd_Ref(exactSets[j]); Cudd_RecursiveDeref(dd,temp);
				break;
			}
		}
		//if (i<nSteps && useUptoNSet){ // since we will restrict state0 vars, we only need to compute upto tracelength-1
		if (useUptoNSet){
			exactSets[n*3] = Cudd_bddOr(dd,temp = exactSets[n*3],exactReachableSet);
			Cudd_Ref(exactSets[n*3]); Cudd_RecursiveDeref(dd,temp);
		}
	}
	for (uint32_t j = 0; j<=n*3; j++){
		cout<<"Summary "<<j<<":"<<std::flush;
		Cudd_PrintSummary(dd,exactSets[j],Cudd_SupportSize(dd, exactSets[j]),1);
	}
	cout<<"Done computing exact reachable sets!\n";
	if (useUptoNSet){
		cout<<"Restricting input BDD with <=N set..\n";
		DdNode* reachableSet = exactSets[3*n];
		cout<<"Reachable Set statistics:";
		Cudd_PrintSummary(dd,reachableSet,Cudd_SupportSize(dd, reachableSet),1);
		//restrict add0 to reachable set
		cout<<"BDD statistics before restrict:";
		Cudd_PrintSummary(dd,*bdd,Cudd_SupportSize(dd, *bdd),1);
		
		temp = Cudd_bddRestrict(dd, *bdd, reachableSet);
		Cudd_Ref(temp); Cudd_RecursiveDeref(dd,*bdd);
		*bdd = temp;

		cout<<"BDD statistics after restrict state0:";
		Cudd_PrintSummary(dd,*bdd,Cudd_SupportSize(dd, *bdd),1);
		Cudd_RecursiveDeref(dd,reachableSet);
	}
	return exactSets;
}

//uses bddAnd instead of restrict
DdNode* andS0ToReachableStates(DdManager* dd, DdNode** bdd, DdNode* initialSet, DdNode** sVars, 
	uint32_t nStateVars, uint32_t traceLength, uint32_t maxDepth, bool returnReachSet){
	
	DdNode *currStateVarCube = Cudd_ReadOne(dd) , *temp;
	Cudd_Ref(currStateVarCube); Cudd_Ref(initialSet);
	for(uint32_t i = 0; i<nStateVars;i++){
		currStateVarCube = Cudd_bddAnd(dd,temp = currStateVarCube, sVars[i]);
		Cudd_Ref(currStateVarCube); Cudd_RecursiveDeref(dd,temp);
	}
	
	DdNode* reachableSet = computeReachableSet(dd, *bdd, initialSet, currStateVarCube,sVars, nStateVars,traceLength, maxDepth);
	cout<<"Reachable Set statistics:\n";
	Cudd_PrintSummary(dd,reachableSet,Cudd_SupportSize(dd, reachableSet),1);
	//restrict add0 to reachable set
	cout<<"BDD statistics before ANDing:\n";
	Cudd_PrintSummary(dd,*bdd,Cudd_SupportSize(dd, *bdd),1);
	
	/*restrict state-0 vars*/
	temp = Cudd_bddAnd(dd, *bdd, reachableSet);
	Cudd_Ref(temp); Cudd_RecursiveDeref(dd,*bdd);
	*bdd = temp;

	cout<<"BDD statistics after ANDing state0 (instead of restricting):\n";
	Cudd_PrintSummary(dd,*bdd,Cudd_SupportSize(dd, *bdd),1);
	
	//deref nonessential dds
	Cudd_RecursiveDeref(dd,currStateVarCube); 
	//intial set will be used later to restrict addn. So dont deref it.
	if( returnReachSet){
		//avoid converting to add till actually required (since it can blow up)
		//convertToADD(dd,&reachableSet);
		return(reachableSet);
	}
	else{
		Cudd_RecursiveDeref(dd,reachableSet);
		return 0;
	}
}

//uses bddRestrict but only on state0 after computing upto traceLength. (Callers job to send traceLength 1 less than
//intended since s0 upto tracelength-1 means s1 will be correctly upto tracelength)
DdNode* restrictS0ToReachableStates(DdManager* dd, DdNode** bdd, DdNode* initialSet, DdNode** sVars, 
	uint32_t nStateVars, uint32_t traceLength, uint32_t maxDepth, bool returnReachSet){
	
	DdNode *currStateVarCube = Cudd_ReadOne(dd) , *temp;
	Cudd_Ref(currStateVarCube); Cudd_Ref(initialSet);
	for(uint32_t i = 0; i<nStateVars;i++){
		currStateVarCube = Cudd_bddAnd(dd,temp = currStateVarCube, sVars[i]);
		Cudd_Ref(currStateVarCube); Cudd_RecursiveDeref(dd,temp);
	}
	DdNode* reachableSet = computeReachableSet(dd, *bdd, initialSet, currStateVarCube,sVars, nStateVars,traceLength, 
		maxDepth);
	cout<<"Reachable Set statistics:\n";
	Cudd_PrintSummary(dd,reachableSet,Cudd_SupportSize(dd, reachableSet),1);
	//restrict add0 to reachable set
	cout<<"BDD statistics before restrict:\n";
	Cudd_PrintSummary(dd,*bdd,Cudd_SupportSize(dd, *bdd),1);
	
	temp = Cudd_bddRestrict(dd, *bdd, reachableSet);
	Cudd_Ref(temp); Cudd_RecursiveDeref(dd,*bdd);
	*bdd = temp;

	cout<<"BDD statistics after restrict state0:\n";
	Cudd_PrintSummary(dd,*bdd,Cudd_SupportSize(dd, *bdd),1);
	//deref nonessential dds
	Cudd_RecursiveDeref(dd,currStateVarCube); //Cudd_RecursiveDeref(dd,reachableSet);
	//intial set will be used later to restrict addn. So dont deref it.
	if( returnReachSet){
		//avoid converting to add till actually required (since it can blow up)
		//convertToADD(dd,&reachableSet);
		return(reachableSet);
	}
	else{
		Cudd_RecursiveDeref(dd,reachableSet);
		return 0;
	}
}

//uses bddRestrict after computing reachableset upto tracelength and takes OR of reachset on state0,state1 
DdNode* restrictS0ORS1ToReachableStates(DdManager* dd, DdNode** bdd, DdNode* initialSet, DdNode** sVars, 
	uint32_t nStateVars, uint32_t traceLength, uint32_t maxDepth, bool returnReachSet){
	
	DdNode *currStateVarCube = Cudd_ReadOne(dd) , *temp;
	Cudd_Ref(currStateVarCube); Cudd_Ref(initialSet);
	for(uint32_t i = 0; i<nStateVars;i++){
		currStateVarCube = Cudd_bddAnd(dd,temp = currStateVarCube, sVars[i]);
		Cudd_Ref(currStateVarCube); Cudd_RecursiveDeref(dd,temp);
	}
	DdNode* reachableSet0 = computeReachableSet(dd, *bdd, initialSet, currStateVarCube,sVars, nStateVars,traceLength, maxDepth);
	cout<<"Reachable Set statistics:\n";
	Cudd_PrintSummary(dd,reachableSet0,Cudd_SupportSize(dd, reachableSet0),1);
	//restrict add0 to reachable set
	cout<<"BDD statistics before restrict:\n";
	Cudd_PrintSummary(dd,*bdd,Cudd_SupportSize(dd, *bdd),1);
	
	/*restrict state-0,1 vars */
	DdNode* reachableSet1 = Cudd_bddSwapVariables(dd,reachableSet0,&(sVars[0]),&(sVars[nStateVars]),nStateVars);
	Cudd_Ref(reachableSet1);
	DdNode* reachableSet = Cudd_bddOr(dd,reachableSet0,reachableSet1);
	Cudd_Ref(reachableSet); Cudd_RecursiveDeref(dd,reachableSet0); Cudd_RecursiveDeref(dd,reachableSet1);
	temp = Cudd_bddRestrict(dd, *bdd, reachableSet);
	Cudd_Ref(temp); Cudd_RecursiveDeref(dd,*bdd);
	*bdd = temp;

	cout<<"BDD statistics after restrict states-0 OR 1:\n";
	Cudd_PrintSummary(dd,*bdd,Cudd_SupportSize(dd, *bdd),1);
	//deref nonessential dds
	Cudd_RecursiveDeref(dd,currStateVarCube); //Cudd_RecursiveDeref(dd,reachableSet);
	//intial set will be used later to restrict addn. So dont deref it.
	if( returnReachSet){
		//avoid converting to add till actually required (since it can blow up)
		//convertToADD(dd,&reachableSet);
		return(reachableSet);
	}
	else{
		Cudd_RecursiveDeref(dd,reachableSet);
		return 0;
	}
}

DdNode* getAllZeroBDDState(DdManager* dd, DdNode** sVars, uint32_t nStateVars){
	DdNode* initialSet = Cudd_ReadOne(dd), *temp;
	Cudd_Ref(initialSet);
	for(uint32_t i = 0; i<nStateVars;i++){
		initialSet = Cudd_bddAnd(dd,temp = initialSet, Cudd_Not(sVars[i]));
		Cudd_Ref(initialSet); Cudd_RecursiveDeref(dd,temp);
	}
	return initialSet;
}

DdNode* makeCube(uint32_t i, DdManager* dd, DdNode** varList, uint32_t n){
	DdNode *temp, *cube = Cudd_ReadOne(dd);
	Cudd_Ref(cube);
	for (uint32_t j = 0; j<n; j++){
		uint32_t k = i%2;
		cube = Cudd_bddAnd(dd,temp=cube, k==1? varList[j]: Cudd_Not(varList[j]));//TODO: check 1/0 condition
		Cudd_Ref(cube);	Cudd_RecursiveDeref(dd,temp);
		i = i/2;
	}
	return (cube);
}

void printPerm(DdManager* man){
	printf("Perm:\n");
    for (int i = 0; i<man->size; i++){
        Cudd_ReadPerm(man,i);
        printf("%d ",man->perm[i]);
    }
    printf("\n");
    printf("InvPerm:\n");
    for (int i = 0; i<man->size; i++){
        printf("%d ",man->invperm[i]);
    }
    printf("\nVar IDs:\n");
    for (int i = 0; i<man->size; i++){
        printf("%d ",man->vars[i]->index);
    }
    printf("\n");
}

void printDoubleLine(){
	cout<<"===================================================================================\n";
}

string printTimeTaken(string forWhat, double_t timeTaken, uint8_t numNewLines){
	stringstream s;
	s << "Time Taken "<<forWhat<<":"<<timeTaken<<" ";
	cout<<"Time Taken "<<forWhat<<":"<<timeTaken<<" "<<std::flush;
	for(uint8_t i = 0; i<numNewLines; i++){
		cout<<"\n";
	}
	return s.str();
}

int Cudd_StringSummary(
  DdManager * dd /**< manager */,
  DdNode * f /**< %DD to be summarized */,
  int n, /**< number of variables for minterm computation */
  string* out /** output string*/)
{
	int mode = 0;/**We force integer (0) mode instead of exponential (1) so that we can use string
	instead of printing to stdout */
    DdNode *azero, *bzero;
    int	nodes, leaves, digits;
    int retval = 1;
    DdApaNumber count;

    if (dd == NULL) {
        return(0);
    }
    if (f == NULL) {
	//(void) fprintf(dd->out,": is the NULL DD\n");
	//(void) fflush(dd->out);
	out->append(": is the NULL DD\n");
        dd->errorCode = CUDD_INVALID_ARG;
	return(0);
    }
    azero = DD_ZERO(dd);
    bzero = Cudd_Not(DD_ONE(dd));
    if (f == azero || f == bzero){
        //(void) fprintf(dd->out,": is the zero DD\n");
        //(void) fflush(dd->out);
		out->append(": is the zero DD\n");
        return(1);
    }
    nodes = Cudd_DagSize(f);
    if (nodes == CUDD_OUT_OF_MEM) retval = 0;
    leaves = Cudd_CountLeaves(f);
    if (leaves == CUDD_OUT_OF_MEM) retval = 0;
    //(void) fprintf(dd->out,": %d nodes %d leaves ", nodes, leaves);
	std::ostringstream stringStream;
	stringStream << ": "<<nodes<<" nodes "<<leaves<<" leaves ";
    out->append(stringStream.str());
	count = Cudd_ApaCountMinterm(dd, f, n, &digits);
    if (count == NULL) {
	retval = 0;
    } else{
        char* mintermStr = Cudd_ApaStringDecimal(digits, count);
		out->append(mintermStr);
		retval = 1;
    }
    FREE(count);
    //(void) fprintf(dd->out, " minterms\n");
    //(void) fflush(dd->out);
    out->append(" minterms\n");
	return(retval);
} /* end of Cudd_PrintSummary */