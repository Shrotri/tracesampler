#ifndef TSAMPLERNODEFACTORY_H_
#define TSAMPLERNODEFACTORY_H_

#include <cstdint>
#include <unordered_set>
#include <deque>
#include <iostream>
#include "RandomBits.hpp"
#include "TSamplerNode.hpp"

using std::unordered_set;
using std::deque;
template <class Num_Type>
class SamplerNodeFactory{
	public:
		static SamplerNodeFactory<Num_Type>* getFactory(uint32_t memLimit);
		void forgetAllUsedNodes();
		void recycleAllUsedNodes();
		SamplerNode<Num_Type>* createRoot(uint32_t cmprsdLvl);
		SamplerNode<Num_Type>* createChild(SamplerNode<Num_Type>* parent, bool tE, uint32_t lvlDiff, uint32_t cmprsdLvl);
		SamplerNode<Num_Type>* copy(SamplerNode<Num_Type>* other);
		SamplerNode<Num_Type>* createSamplerNode(); // all constructors here
		void recycleSamplerNode(SamplerNode<Num_Type>*);
		void clearSentinel(uint32_t cmprsdLvl);
		std::pair<std::pair<SamplerNode<Num_Type>*,bool>,uint32_t> sampleParent(SamplerNode<Num_Type>* nodeSN, vector<double_t> weights, RandomBits *rb);
		bool noUsedNodes();
		
		SamplerNode<Num_Type> sentinel;
		
	private:
		unordered_set<SamplerNode<Num_Type>*> usedNodes; deque<SamplerNode<Num_Type>*> freeNodes;
		vector<Num_Type> samplingArray; Num_Type runningTotal, r;
		void ensureSamplingArraySize(uint32_t minSize);
		SamplerNode<Num_Type>* getFreeNode();
		SamplerNodeFactory();
    	SamplerNodeFactory(const SamplerNodeFactory<Num_Type> &) { }
    	SamplerNodeFactory<Num_Type> &operator=(const SamplerNodeFactory<Num_Type> &) { return *this; }
		
};

template class SamplerNodeFactory<double_t>;
template class SamplerNodeFactory<long double>;

#endif


/*

*/