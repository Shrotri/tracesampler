#include "SamplerNode.hpp"
#include <iostream>

SamplerNode::SamplerNode( vector<SamplerNode*> parents_, NumType* rootPathCount_, uint32_t compressedLevel_,
	vector<bool> thenElse_): 
compressedLevel(compressedLevel_), rootPathCount(rootPathCount_), parents(parents_), 
	thenElse(thenElse_){}
SamplerNode::SamplerNode( SamplerNode* parent_, NumType* rootPathCount_, uint32_t compressedLevel_,
	bool thenElse_): 
compressedLevel(compressedLevel_), rootPathCount(rootPathCount_), parents(1,parent_), 
	thenElse(1,thenElse_){}
SamplerNode::SamplerNode( NumType* rootPathCount_, uint32_t compressedLevel_):
	compressedLevel(compressedLevel_), rootPathCount(rootPathCount_){}
SamplerNode::SamplerNode(){}
SamplerNode::SamplerNode(const SamplerNode& other): parents(other.parents),thenElse(other.thenElse),
	compressedLevel(other.compressedLevel),rootPathCount(other.rootPathCount->copy()) {}
SamplerNode::~SamplerNode(){
	delete rootPathCount;
}

void SamplerNode::addParent(SamplerNode* parent, bool tE, uint32_t lvlDiff){
	parents.push_back(parent);
	thenElse.push_back(tE);
	rootPathCount->addProductWithPowerOfTwoSelf(parent->rootPathCount,lvlDiff);
}

void SamplerNode::append(SamplerNode* other){
	parents.insert(parents.end(),other->parents.begin(),other->parents.end());
	thenElse.insert(thenElse.end(),other->thenElse.begin(),other->thenElse.end());
	rootPathCount->addSelf(other->rootPathCount);
}

uint32_t SamplerNode::getCompressedLevel(){
	return compressedLevel;
}

void SamplerNode::printParentCmpLvls(){
	//printf("parent cmprsdlvls are:\n");
	for(auto& parent: parents){
		std::cout<<parent->compressedLevel<<" "<<std::flush;
	}
	std::cout<<"\n";
}