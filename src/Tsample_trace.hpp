#ifndef SAMPLE_TRACE_H_
#define SAMPLE_TRACE_H_

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include "cudd.h"
#include "dddmp.h"
#include "dddmpInt.h"
#include "RandomBits.hpp"
#include "TSamplerNodeFactory.hpp"

using std::vector;
using std::unordered_map;
using std::unordered_set;
using std::pair;
using std::string;


namespace ST{
class State{
	public:
		State(uint32_t nStateVars_): bits(nStateVars_,-1){  //-1 is unset default value
		}
		void setNoCheck(bool bit, uint32_t i);
		void set(bool bit, uint32_t i);
		bool get(uint32_t i);
		void reset();
		void printState();
	private:
		vector<int8_t> bits;
};

class Trace{
	public:
		Trace(uint32_t numStates_, uint32_t nStateVars_): numStates(numStates_), states(numStates_,State(nStateVars_)){}
		void putState(State* state, uint32_t i);
		State* getState(uint32_t i);
		void clear(); // resets all state values to default -1
	private:
		vector<State> states;
		uint32_t numStates;
};


template <class Num_Type>
class TraceSampler{
	public:
		TraceSampler(char* ddFileName_, char* ddMetaFileName_, uint32_t traceLength_, 
		bool checkSamples_, int32_t useReachableSet_);
		
		void buildDataStructures();
		Trace& drawSample();
		bool checkTrace();
		double_t checkSample(uint32_t, State*,State*,State*);
		void writeTraceToFile(FILE* ofp);
		uint32_t getNumStateVars();
		
	private:
		typedef struct ADD{
			DdNode* add;
			DdManager* dd;
			unordered_map<uint32_t, pair<uint32_t,uint8_t>> varMap; // map from add index to <latch number, {0,1,2}> where 0=s,1=s',2=s'' 
			unordered_map<DdNode*, SamplerNode<Num_Type>*> rootSNs; //vector in case of multiple roots when using reverse ordering. rootSNs[0] is always the sampler node corresponding to the root of the whole ADD.
			unordered_map<DdNode*, vector<std::pair<DdNode*,SamplerNode<Num_Type>*>>> allSamplingPreLeaves; //vector (of vector) in case of multiple roots when using reverse ordering
			unordered_map<uint32_t,uint32_t> compressedPerm;
			int* compressedInvPerm;
			bool usePreleaves; //if true, then the sampling structure is only upto preleaves. Need to find leaf for each preleaf before sampling
			uint32_t startSampleVarLevel, endSampleVarLevel, sampleStructDepth;
			uint32_t sVarSIndex, sVarSPIndex, sVarSDPIndex; //indices of s,s',s'' start variables in sVar array
			//stores all addresses in the samplestruct
			//unordered_map<DdNode*, unordered_set<SamplerNode*>> tNodesByRoot; //for debugging. stores nodes indexed by root node of dag
			bool precompileSampleDAG;
		}ADD;

		void createAuxStructures();
		void createAuxStructure(uint32_t);
		void unroll(bool);
		void createSamplingDAGs();
		void createSamplingDAGs_rec(ADD&, DdNode*, unordered_set<DdNode*>*);
		void createSamplingDAG(uint32_t addNum);
		void sampleFromADD(uint32_t addNum, State* s, State* sP, State* sDP);
		void drawSample_rec(uint32_t addNum, uint32_t s_, uint32_t sP_, uint32_t sDP_, State*, State* );		
		DdNode* computeReachableSet(DdNode* cube);

		char* ddFileName;
		char* ddMetaFileName;
		uint32_t n;
		uint32_t traceLength;
		bool checkSamples;

		uint32_t numLatches, numInputs, nStateVars, varStartIndex;
		vector<ADD> adds;
		Trace* t;
		DdNode** sVars;
		RandomBits rb;
		SamplerNodeFactory<Num_Type> *snFactory;

		bool hasExtraADD; // for cases when traceLength is not a power of two
		DdNode* initialSet;
		int32_t useReachableSet;
		DdNode** exactReachSets;
};
}//end namespace
#endif