#include "TSamplerNode.hpp"
#include <iostream>

template <class Num_Type>
SamplerNode<Num_Type>::SamplerNode( vector<SamplerNode<Num_Type>*> parents_, Num_Type rootPathCount_, uint32_t compressedLevel_,
	vector<bool> thenElse_): 
compressedLevel(compressedLevel_), rootPathCount(rootPathCount_), parents(parents_), 
	thenElse(thenElse_){}

template <class Num_Type>	
SamplerNode<Num_Type>::SamplerNode( SamplerNode<Num_Type>* parent_, Num_Type rootPathCount_, uint32_t compressedLevel_,
	bool thenElse_): 
compressedLevel(compressedLevel_), rootPathCount(rootPathCount_), parents(1,parent_), 
	thenElse(1,thenElse_){}

template <class Num_Type>
SamplerNode<Num_Type>::SamplerNode( Num_Type rootPathCount_, uint32_t compressedLevel_):
	compressedLevel(compressedLevel_), rootPathCount(rootPathCount_){}

template <class Num_Type>
SamplerNode<Num_Type>::SamplerNode(){}

template <class Num_Type>
SamplerNode<Num_Type>::SamplerNode(const SamplerNode<Num_Type>& other): parents(other.parents),thenElse(other.thenElse),
	compressedLevel(other.compressedLevel),rootPathCount(other.rootPathCount) {}

template <class Num_Type>
SamplerNode<Num_Type>::~SamplerNode(){}

template <class Num_Type>
void SamplerNode<Num_Type>::addParent(SamplerNode<Num_Type>* parent, bool tE, uint32_t lvlDiff){
	parents.push_back(parent);
	thenElse.push_back(tE);
	rootPathCount += parent->rootPathCount * pow(2,lvlDiff);
}

template <class Num_Type>
void SamplerNode<Num_Type>::append(SamplerNode<Num_Type>* other){
	parents.insert(parents.end(),other->parents.begin(),other->parents.end());
	thenElse.insert(thenElse.end(),other->thenElse.begin(),other->thenElse.end());
	rootPathCount += other->rootPathCount;
}

template <class Num_Type>
uint32_t SamplerNode<Num_Type>::getCompressedLevel(){
	return compressedLevel;
}

template <class Num_Type>
void SamplerNode<Num_Type>::printParentCmpLvls(){
	//printf("parent cmprsdlvls are:\n");
	for(auto& parent: parents){
		std::cout<<parent->compressedLevel<<" "<<std::flush;
	}
	std::cout<<"\n";
}