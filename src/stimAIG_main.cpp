#include "stimAIG.hpp"
#include <iostream>

using std::cout;
using std::endl;
using std::cin;
int main(int argc, char** argv){
	if (argc != 2){
		cout<<"USAGE: stimAIG <model.aig>"<<endl;
		exit(1);
	}
	char* fName = argv[1];
	StimAig s(fName);
	int ni = s.getNumInputs();
	int nl = s.getNumLatches();
	int no = s.getNumPOs();
	int i = 0;
	unsigned char tmp;
	vector<bool> inputVec(ni), stateVec(nl), resL(nl), resO(no);
	while (true){
		if (i == 0){
			cout<<ni<<" Inputs(0/1):"<<endl;
			for (int j = 0; j<ni;j++) {cin>>tmp; inputVec[j]=tmp-'0';}
			cout<<nl<<" Latches(0/1):"<<endl;
			for (int j = 0; j<nl;j++) {cin>>tmp; stateVec[j]=tmp-'0';}
			s.stimulate(inputVec,stateVec,resL, resO);
			cout<<"Next State:"<<endl;
			for (int j = 0; j<nl;j++) cout<<resL[j];
			cout<<"\nOutputs:"<<endl;
			for (int j = 0; j<no;j++) cout<<resO[j];
			cout<<endl;
			i++;
			continue;
		}
		cout<<ni<<" Inputs(0/1/2=prev ins):"<<endl;
		cin>>tmp;
		if (tmp<'2'){
			inputVec[0]=tmp-'0';
			for (int j = 1; j<ni;j++) {cin>>tmp; inputVec[j]=tmp-'0';}
		}		
		cout<<nl<<" Latches(0/1/2=nextstate of prev iteration):"<<endl;
		cin>>tmp;
		if (tmp<'2'){
			stateVec[0]=tmp-'0';
			for (int j = 1; j<nl;j++) {cin>>tmp; stateVec[j]=tmp-'0';}
		} else{
			stateVec.swap(resL);
		}
		s.stimulate(inputVec,stateVec,resL, resO);
		cout<<"Next State:"<<endl;
		for (int j = 0; j<nl;j++) cout<<resL[j];
		cout<<"\nOutputs:"<<endl;
		for (int j = 0; j<no;j++) cout<<resO[j];
		cout<<endl;
	}
}