#ifndef SAMPLERNODEFACTORY_H_
#define SAMPLERNODEFACTORY_H_

#include <cstdint>
#include <unordered_set>
#include <deque>
#include <iostream>
#include "SamplerNode.hpp"
#include "GMPNumType.hpp"

using std::unordered_set;
using std::deque;
class SamplerNodeFactory{
	public:
		enum NUMTYPE{
			dbl,long_dbl,gmpz
		};
		static SamplerNodeFactory* getFactory(uint32_t memLimit, SamplerNodeFactory::NUMTYPE SamplerNodeType_);
		void forgetAllUsedNodes();
		void recycleAllUsedNodes();
		SamplerNode* createRoot(uint32_t cmprsdLvl);
		SamplerNode* createChild(SamplerNode* parent, bool tE, uint32_t lvlDiff, uint32_t cmprsdLvl);
		SamplerNode* copy(SamplerNode* other);
		SamplerNode* createSamplerNode(); // all constructors here
		void recycleSamplerNode(SamplerNode*);
		void clearSentinel(uint32_t cmprsdLvl);
		std::pair<std::pair<SamplerNode*,bool>,uint32_t> sampleParent(SamplerNode* nodeSN, vector<double_t> weights, RandomBits *rb);
		bool noUsedNodes();
		
		SamplerNode sentinel;
		
	private:
		NumType *zero, *one;
		SamplerNodeFactory::NUMTYPE SamplerNodeType;
		unordered_set<SamplerNode*> usedNodes; deque<SamplerNode*> freeNodes;
		vector<NumType*> samplingArray; NumType *runningTotal, *r;
		vector<double> sA2;
		void ensureSamplingArraySize(uint32_t minSize);
		SamplerNode* getFreeNode();
		SamplerNodeFactory(SamplerNodeFactory::NUMTYPE SamplerNodeType_);
		SamplerNodeFactory();
    	SamplerNodeFactory(const SamplerNodeFactory &) { }
    	SamplerNodeFactory &operator=(const SamplerNodeFactory &) { return *this; }
		
};


#endif


/*

*/