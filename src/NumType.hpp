#ifndef NUMTYPE_H_
#define NUMTYPE_H_

#include <cstdint>
#include <cmath>
#include "RandomBits.hpp"

class NumType{
	public:
		virtual ~NumType() = 0;
		virtual NumType* copy() = 0;
		virtual NumType* multiply(NumType*) = 0;
		virtual void multiplySelf(NumType*) = 0;
		virtual void multiplyByDoubleSelf(double_t) = 0;
		virtual NumType* add(NumType*) = 0;
		virtual void addSelf(NumType*) = 0;
		virtual NumType* multiplyByPowerOfTwo(uint32_t) = 0;
		virtual void multiplyByPowerOfTwoSelf(uint32_t) = 0;
		virtual NumType* divideBy(NumType*) = 0;
		virtual void divideBySelf(NumType*) = 0;
		virtual void addPowerOfTwoSelf(uint32_t) = 0;
		virtual void addProductWithPowerOfTwoSelf(NumType*, uint32_t) =0;
		virtual void set(NumType* other) = 0;
		virtual void setPowerOfTwo(uint32_t) = 0;
		virtual void setRandom(NumType*,RandomBits *) = 0;

		virtual NumType* getRandom(RandomBits *) = 0; //returns a random number between 1 and this-number

		virtual bool lessThan(NumType*) = 0;
		virtual bool isPositive() = 0;
		virtual bool isNonNegative() = 0;
		virtual bool isCloseToOne(double_t) = 0;
		virtual bool isZero() = 0;

		virtual void print() = 0;
};


class DoubleNumType : public NumType{
	public:
		DoubleNumType(double_t);
		~DoubleNumType() override;
		DoubleNumType* copy() override;
		DoubleNumType* multiply(NumType*) override;
		void multiplySelf(NumType*) override;
		void multiplyByDoubleSelf(double_t) override;
		DoubleNumType* add(NumType*) override;
		void addSelf(NumType*) override;
		DoubleNumType* multiplyByPowerOfTwo(uint32_t) override;
		void multiplyByPowerOfTwoSelf(uint32_t) override;
		DoubleNumType* divideBy(NumType*) override;
		void divideBySelf(NumType*) override;
		void addPowerOfTwoSelf(uint32_t) override;
		void addProductWithPowerOfTwoSelf(NumType*, uint32_t) override;
		void set(NumType* other) override;
		void setPowerOfTwo(uint32_t) override;
		void setRandom(NumType*,RandomBits *) override;

		DoubleNumType* getRandom(RandomBits *) override;

		bool lessThan(NumType*) override;
		bool isPositive() override;
		bool isNonNegative() override;
		bool isCloseToOne(double_t) override;
		bool isZero() override;

		void print() override;
		// double_t number;
	private:
		double_t number;
		static std::uniform_real_distribution<double_t> unif;
};

class LongDoubleNumType : public NumType{
	public:
		LongDoubleNumType(double_t);
		~LongDoubleNumType() override;
		LongDoubleNumType* copy() override;
		LongDoubleNumType* multiply(NumType*) override;
		void multiplySelf(NumType*) override;
		void multiplyByDoubleSelf(double_t) override;
		LongDoubleNumType* add(NumType*) override;
		void addSelf(NumType*) override;
		LongDoubleNumType* multiplyByPowerOfTwo(uint32_t) override;
		void multiplyByPowerOfTwoSelf(uint32_t) override;
		LongDoubleNumType* divideBy(NumType*) override;
		void divideBySelf(NumType*) override;
		void addPowerOfTwoSelf(uint32_t) override;
		void addProductWithPowerOfTwoSelf(NumType*, uint32_t) override;
		void set(NumType* other) override;
		void setPowerOfTwo(uint32_t) override;
		void setRandom(NumType*,RandomBits *) override;

		LongDoubleNumType* getRandom(RandomBits *) override;

		bool lessThan(NumType*) override;
		bool isPositive() override;
		bool isNonNegative() override;
		bool isCloseToOne(double_t) override;
		bool isZero() override;

		void print() override;
	private:
		long double number;
		static std::uniform_real_distribution<long double> unif;
};

#endif