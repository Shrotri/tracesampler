#include <cassert>
#include "dd2cnf.hpp"
#include "utils.hpp"

using std::endl;
// will need this for unigen for experimental comparison, as well as for waps to see if tracesampler samples with 
// uniform distribution. Waps does not provide good support for projection. So will need cnf constructed from minterms
// for unigen, can use dddmp possibly if we can figure out mapping between bddids and cnfids, as well as aux vars
DD2CNF::DD2CNF(char* inDDFile_, char* inMetaFile_, uint32_t traceLength_, uint32_t setState0_, double_t startTime_):
	inDDFile(inDDFile_), inMetaFile(inMetaFile_), traceLength(traceLength_), setState0(setState0_), 
	startTime(startTime_){
	
	assert(traceLength>1);
	string stateSeq; bool useContiguity=false;
	cout<<"Reading DD from file.. "<<std::flush;
	readDD(inDDFile,inMetaFile,&dd,&node,numLatches,numInputs,nStateVars,varStartIndex, &useContiguity, &stateSeq); 
	cout<<"Done reading!\n";
	readTime = cpuTimeTotal();
	printTimeTaken("ReadDD",readTime-startTime,2);
	DdNode** sVars = (DdNode**) calloc(2*nStateVars, sizeof(DdNode*)); //2^n transitions mean n+1 bdds -- 1,2,4,..2^n
	for(uint32_t i = varStartIndex; i<varStartIndex+2*nStateVars;i++){
		sVars[i-varStartIndex] = Cudd_bddIthVar(dd,i);
	}
	//if(stateSeq.compare(STATE_SEQ_10)==0) switchStates(dd,&node,DDDMP_BDD,sVars ,nStateVars,varStartIndex);
	assert(stateSeq.compare(STATE_SEQ_01)==0);
	if(setState0==2){
		initialSet = getAllZeroBDDState(dd,sVars,nStateVars);
		//restrict state0 to tracelength-1 steps, so state1 will automatically be tracelength steps which means no need to
		//explicitly restrict it
		restrictS0ToReachableStates(dd,&node,initialSet,sVars,nStateVars,traceLength-1,2*nStateVars, false);
	}
	reachComputeTime = cpuTimeTotal();
	printTimeTaken("Computing Reach Set(s)",reachComputeTime-readTime,2);
	free(sVars);
	int* supIndices;
	int nSup = Cudd_SupportIndices(dd,node,&supIndices);
	for (int i = 0; i<nSup; i++){
		if(supIndices[i]<varStartIndex){
			ddHasPIs = true; // the dd depends on primary inputs. Put them in projection list when writing CNF
		}
	}
	Cudd_AutodynDisable(dd);
}

void DD2CNF::compileMinTermCNF(){
	//TODO: check if node is bdd
	assert(!ddHasPIs); //if dd has PIs in support then cannot use for exact uniform sampling as is, since those vars
	//should be existabstracted out first.

	//we will use Cudd_foreachcube for traversing over on-set cubes of the dd. So we complement dd to make them on-set
	node = Cudd_Not(node);
	//no need to ref deref since its a bdd
	DdGen* gen; int* cube;
	//other option is foreaechcube.. but description is confusing. Using foreachprime since it states that it finds a 
	//cover even though it can be redundant
	cout<<"Stats of complement of BDD (since we will be iterating over primes of complement)\n";
	Cudd_PrintSummary(dd,node,Cudd_SupportSize(dd, node),1);
	cout<<"Minterms (not primes) in uncomplemented node:"<<Cudd_CountMinterm(dd,Cudd_Not(node),Cudd_SupportSize(dd, node))<<"\n";
	cout<<"Countpaths-to-1 (not primes), uncomplemented:"<<Cudd_CountPathsToNonZero(Cudd_Not(node))<<" complemented:"<<
	Cudd_CountPathsToNonZero(node)<<"\n";
	uint32_t nCls = 0;
	Cudd_ForeachPrime(dd,node,node,gen,cube){
		//A cube is represented as an array of literals, which are integers in {0, 1, 2}; 
		//0 represents a complemented literal, 1 represents an uncomplemented literal, and 2 stands for don't care.
		//The size of the array equals the number of variables in the manager at the time Cudd_FirstPrime is called.
		//printf("Processing minterm\n");
		baseClauses.emplace_back();
		for (int q = 0; q < dd->size; q++) {
			switch (cube[q]) {
				//variables should be indexed from 1 in CNF dimacs.
				//varStartIndex will be the first variable to appear in bdd, since we ensured that PIs dont appear
				//so subtract varStartIndex - 1 from q for CNF index. 
				case 0:
					assert(q>=varStartIndex);
					//this is complemented literal case. But a clause is the negation of a cube. So we store
					//the unnegated version
					(baseClauses.end()-1)->push_back((q-varStartIndex+1));
					break;
				case 1:
					assert(q>=varStartIndex);
					//store negated version for above reason
					(baseClauses.end()-1)->push_back(-(q-varStartIndex+1)); 
					break;
				case 2:
					continue;
					break;
				default:
					(void) printf("Unknown literal %d found while enumerating cubes. Exiting.\n",cube[q]);
					exit(1);
			}
		}
		#if DFAST < 2
		if((++nCls)*traceLength > 1000000) {
			cout<<"No. of Base clauses * traceLength has exceeded 1 million. Exiting..\n";
			exit(0);
		}
		#endif 
	}
	gen=NULL; cube=NULL;
	printf("Base CNF num clauses: %lu\n",baseClauses.size());
	if (setState0==2){
		printf("Creating clauses setting state0 to initialSet\n");
		Cudd_ForeachPrime(dd,Cudd_Not(initialSet),Cudd_Not(initialSet),gen,cube){
			startStateClauses.emplace_back();
			for (int q = 0; q < dd->size; q++) {
				switch (cube[q]) {
					//variables should be indexed from 1 in CNF dimacs.
					//varStartIndex will be the first variable to appear in bdd, since we ensured that PIs dont appear
					//so subtract varStartIndex - 1 from q for CNF index. 
					case 0:
						assert(q>=varStartIndex);
						//this is complemented literal case. But a clause is the negation of a cube. So we store
						//the unnegated version
						(startStateClauses.end()-1)->push_back((q-varStartIndex+1));
						break;
					case 1:
						assert(q>=varStartIndex);
						//store negated version for above reason
						(startStateClauses.end()-1)->push_back(-(q-varStartIndex+1)); 
						break;
					case 2:
						continue;
						break;
					default:
						(void) printf("Unknown literal %d found while enumerating cubes. Exiting.\n",cube[q]);
						exit(1);
				}
			}
		}
	} else if(setState0==1){
		printf("Adding clauses setting state 0 to 0\n");	
		for (uint32_t i=0; i<nStateVars; i++){
			vector<int> clause0;
			clause0.push_back(-(i+1)); //set all to false
			startStateClauses.push_back(clause0);
		}
	}
	printf("StartStateClauses size: %lu\n",startStateClauses.size());
	compileCNFTime = cpuTimeTotal();
	printTimeTaken("compileCNFTime",compileCNFTime-reachComputeTime,2);	
}

uint8_t DD2CNF::writeCNF(char* outCNFFile){
	//writing for minterm base cnf. need to generalize to base cnfs with auxiliary and input vars.
	vector<vector<int>> fullCNF (baseClauses);
	if(setState0>0){
		assert(startStateClauses.size()>0);
		fullCNF.insert(fullCNF.end(),startStateClauses.begin(),startStateClauses.end());
	}
	printf("Unrolling..\nIters: ");
	for(uint32_t i = 1; i<traceLength; i++){
		cout<<i<<" "<<std::flush;
		vector<vector<int>> baseCopy (baseClauses);
		for (auto& clause : baseCopy){
			for (uint32_t j = 0; j<clause.size();j++){
				clause.at(j) += clause.at(j) < 0? -i*nStateVars : i*nStateVars;
			}
		}
		//i \in {1,...,traceLength-1}
		//at the end of ith iteration, constraints for i+1th transition will have been added.
		//so at end of last iteration with i=tracelenght-1, the constraints for traceLength-th transition would be added
		fullCNF.insert(fullCNF.end(),baseCopy.begin(),baseCopy.end());
	}
	printf("\n");
	unrollTime = cpuTimeTotal();
	printTimeTaken("Unroll",unrollTime-compileCNFTime,2);
	//write to file
	FILE* of = fopen(outCNFFile,"w");
	fprintf(of,"c unrolled  to have %d steps\n",traceLength);
	fprintf(of,"p cnf %u %lu\n",(traceLength+1)*nStateVars,fullCNF.size());
	for(uint32_t i = 0; i<fullCNF.size();i++){
		for (uint32_t j = 0; j<fullCNF[i].size();j++){
			fprintf(of,"%d ",fullCNF[i][j]);
		}
		fprintf(of,"0\n");
	}

	fclose(of);
	cout<<"Final numVars:"<<(traceLength+1)*nStateVars<<" numCls:"<<fullCNF.size()<<endl;
	writeTime = cpuTimeTotal();
	printTimeTaken("WriteCNF",writeTime-unrollTime,2);
}

int main(int argc, char** argv){
	double startTime = cpuTimeTotal();
	cout<< "Starting dd2cnf at ";
	print_wall_time(); cout<<std::endl;
	if (argc!=6){
		cout<<"USAGE: dd2cnf <in.bdd> <in.bdd.meta> <out.cnf> <traceLength> <useReachableStates=0/1/2>\n";
		cout<<"useReachStates: 0 = dont bother, 1 = set state 0 to 0, 2 = compute reachable set from bdd"
			"and restrict bdd to reachable set before computing CNF\n";
		exit(1);
	}
	uint32_t setState0;
	if (argv[5][0]=='0'){
		setState0 = 0;
	} else if (argv[5][0]=='1'){
		setState0 = 1;
	} else if (argv[5][0]=='2'){
		setState0 = 2;
	} else{
		cout<<"useReachableStates parameter not recognized (must be 0/1/2):"<<argv[5]<<"\n";
		exit(1);
	}
	char *inFile = argv[1], *mFile = argv[2], *outFile = argv[3];
	uint32_t traceLength_ = strtol(argv[4],NULL,10);
	if(traceLength_<=1){cout<<"Tracelength:"<<traceLength_<<" should be greater than 1. Exiting..\n";exit(0);}
	printDoubleLine();
	cout<<"dd2cnf invoked with in.bdd"<<string(inFile)<<" in.bdd.meta:"<<string(mFile)<<" traceLength:"<<traceLength_
	<<" out.cnf:"<<string(outFile)<<" useReachableStates:"<<setState0<<"\n";
	printDoubleLine();

	DD2CNF d(inFile, mFile, traceLength_, setState0, startTime);
	d.compileMinTermCNF();
	d.writeCNF(outFile);
	printDoubleLine();
	cout<<endl<<endl<< "dd2cnf ended at ";
	print_wall_time(); cout<<endl;
	printTimeTaken(string("Total"),(cpuTimeTotal()-startTime),1);
}
