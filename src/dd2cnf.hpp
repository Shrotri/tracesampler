#ifndef DD2CNF_H_
#define DD2CNF_H_

#include <iostream>
#include <vector>
#include "cudd.h"
#include "cuddInt.h"

using std::cout;
using std::vector;
class DD2CNF{
	public:
		DD2CNF(char* inDDFile_, char* inMetaFile_,uint32_t, uint32_t, double_t);
		uint8_t writeCNF(char*);
		void compileMinTermCNF();
	private:
		uint32_t traceLength, setState0;
		vector<vector<int>> baseClauses;
		bool ddHasPIs = false;
		char *inDDFile, *inMetaFile;
		DdManager* dd;
		DdNode* node, *initialSet;
		uint32_t numInputs, numLatches, nStateVars, varStartIndex;
		vector<vector<int>> startStateClauses;

		double_t readTime, startTime, reachComputeTime, compileCNFTime, unrollTime, writeTime;
};

#endif