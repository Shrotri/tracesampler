#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cassert>

#include "aig2dd.hpp"
#include "stimAIG.hpp"
#include "RandomBits.hpp"
#include "utils.hpp"

#ifndef DFAST
	#define DFAST 1
#endif

using std::cout; using std::endl;

ADDWrapper* ADDWrapper::And(DDWrapper* other){
	 return new ADDWrapper(node*(((ADDWrapper*) other)->node));
}

ADDWrapper* ADDWrapper::Complement(){
	return new ADDWrapper(node.Cmpl());
}

ADDWrapper* ADDWrapper::ExistAbstract(DDWrapper* cube){
	return new ADDWrapper(node.ExistAbstract(((ADDWrapper*)cube)->node));
}

void* ADDWrapper::getNode(){
	return node.getNode();
}

void ADDWrapper::printStats(uint8_t verbosity){
	printf("ADDStats: ");
	Cudd_PrintSummary(node.manager(),node.getNode(),Cudd_SupportSize(node.manager(),node.getNode()),1);
	printf("Support size: %d\n",Cudd_SupportSize(node.manager(),node.getNode()));
	if (verbosity==1) {
		cout<<"===================================================================================\n";
		Cudd_PrintInfo(node.manager(), stdout);
		cout<<"===================================================================================\n";
	}
}

int ADDWrapper::Traverse(vector<bool> inputVec, vector<bool> stateVec, vector<bool> resL, int ni, int nl){
	DdManager* m = node.manager();
	DdNode* n = (DdNode*)getNode();
	assert(ni+nl+nl==m->size);
	DdNode* temp = n;
	while(!Cudd_IsConstant(temp)){
		unsigned ind = Cudd_NodeReadIndex(temp);
		//cout<<"ind: "<<ind<<" temp: "<<temp<<endl;
		bool dec;
		if(ind < ni){
			dec = inputVec[ind];
		} else if (ind < ni+nl){
			//this is state0 (see the constructor for var ordering details) so use stateVec
			dec = stateVec[ind - ni];
		} else{
			//this is state1 (see the constructor for var ordering details) so use resL
			dec = resL[ind - (ni+nl)];
		}
		temp = (dec? cuddT(Cudd_Regular(temp)) : cuddE(Cudd_Regular(temp)));
	}
	//cout<<"Traverse reached value: "<<Cudd_V(temp)<<"\n";
	if(Cudd_V(temp)==0) return 0;
	else return 1;
}


int BDDWrapper::Traverse(vector<bool> inputVec, vector<bool> stateVec, vector<bool> resL, int ni, int nl){
	DdManager* m = node.manager();
	DdNode* n = (DdNode*)getNode();
	assert(ni+nl+nl==m->size);
	DdNode* temp = n;
	bool comp = false;
	while(!Cudd_IsConstant(temp)){
		unsigned ind = Cudd_NodeReadIndex(temp);
		//cout<<"ind: "<<ind<<endl;
		comp = Cudd_IsComplement(temp)?(!comp):comp;
		bool dec;
		if(ind < ni){
			dec = inputVec[ind];
		} else if (ind < ni+nl){
			//this is state0 (see the constructor for var ordering details) so use stateVec
			dec = stateVec[ind - ni];
		} else{
			//this is state1 (see the constructor for var ordering details) so use resL
			dec = resL[ind - (ni+nl)];
		}
		temp = (dec? Cudd_T(temp) : Cudd_E(temp));
	}
	comp = Cudd_IsComplement(temp)?(!comp):comp;
	//cout<<"Traverse reached value: "<<Cudd_V(temp)<<" and is temp complemented?"<<Cudd_IsComplement(temp)
	//	<<" and final complement value: "<<comp<<"\n";
	if(comp) return 0;
	else return 1;
}

BDDWrapper* BDDWrapper::And(DDWrapper* other){
	return new BDDWrapper(node.And(((BDDWrapper*) other)->node));
}

BDDWrapper* BDDWrapper::Complement(){
	return new BDDWrapper(!node);
}

BDDWrapper* BDDWrapper::ExistAbstract(DDWrapper* cube){
	return new BDDWrapper(node.ExistAbstract(((BDDWrapper*)cube)->node));
}

void* BDDWrapper::getNode(){
	 return node.getNode();
}

void BDDWrapper::printStats(uint8_t verbosity){
	printf("BDDStats: ");
	Cudd_PrintSummary(node.manager(),node.getNode(),Cudd_SupportSize(node.manager(),node.getNode()),1);
	printf("Support size: %d\n",Cudd_SupportSize(node.manager(),node.getNode()));
	if (verbosity==1) {
		printDoubleLine();
		Cudd_PrintInfo(node.manager(), stdout);
		printDoubleLine();
	}
}
//=========================================================================================================================
int CNFWrapper::Traverse(vector<bool> inputVec, vector<bool> stateVec, vector<bool> resL, int ni, int nl){
	
	CNFManager* m = node.cnfMgr; //cout<<"manager address:"<<m<<endl;
	//cout<<m->maxVar<<" "<<m->auxVars.size()<<" "<<m->currStateVars.size()<<" "<<m->nextStateVars.size()<<" "<<m->OPIs.size()<<"\n";
	//cout<<ni+nl+nl<<" "<<m->maxVar-m->auxVars.size()<<" "<<m->currStateVars.size()+m->nextStateVars.size()+m->OPIs.size()<<"\n";
	assert(ni+nl+nl==m->maxVar-m->auxVars.size());
	assert(ni+nl+nl==m->currStateVars.size()+m->nextStateVars.size()+m->OPIs.size());
	unordered_map<uint32_t,bool>& auxVals = m->auxVals;
	bool comp = false;
	uint32_t ind = abs(node.varNum)-1; //since cnfvars are 1 indexed
	bool dec;
	if (ind<ni+nl+nl){ // i.e. it is not an aux var
		if(ind < ni){
			dec = inputVec[ind];
		} else if (ind < ni+nl){
			//this is state0 (see the constructor for var ordering details) so use stateVec
			dec = stateVec[ind - ni];
		} else{
			//this is state1 (see the constructor for var ordering details) so use resL
			dec = resL[ind - (ni+nl)];
		}
		return node.varNum>0 ? dec : !dec;
	}
	uint32_t vN = abs(node.varNum);
	bool finRes;
	if(auxVals.find(vN)==auxVals.end()){
		//find left and right children
		std::pair<CNFLit,CNFLit> children = m->getANDChildren(node);
		// trav_rec left trav_rec right
		CNFWrapper left(children.first);
		CNFWrapper right(children.second);
		bool res1 = left.Traverse(inputVec,stateVec,resL,ni,nl);
		bool res2 = right.Traverse(inputVec,stateVec,resL,ni,nl);
		// res1 and res2 should be tru after appropiately negating
		auxVals[vN] = res1&&res2;
		finRes = node.varNum>0 ? res1&&res2 : !(res1&&res2);
	} else{
		finRes = auxVals.at(vN);
		finRes = node.varNum>0 ? finRes : !finRes;
	}
	//cout<<"Finres is: "<<finRes<<endl;
	return finRes;
	// base cases: 
	// 	if var is in opis currstate or next state, use value from vectors	
}

//store need not have been a function of cnfwrapper technically, but due to the old template of the wrapper class we 
//follow this structure. in a later version may factor out this functionality to utils to streamline with dd2cnf
//store will be called on the cnfwrapper for the literal for the output of the aig. we need to assert that literal in
//the final cnf which we do below
int CNFWrapper::store(char* outFile, bool setState0, uint32_t traceLength, vector<string> comments){

	CNFManager* m = node.cnfMgr;
	uint32_t nOPIs = m->OPIs.size(), nAuxVars = m->auxVars.size(), nStateVars = m->currStateVars.size();
	vector<int32_t> assertLit({node.varNum});
	m->clauses.push_back(assertLit); //assertlit is added to mclauses so that each copy of that literal after unrolling
	//also gets asserted. This should be equivalent to ANDing all the outputs of the unrolled Aig.
	//vector<vector<int32_t>> fullCNF (m->clauses);
	vector<vector<int32_t>> fullCNF; // we dont initialize with baseCNF, since we will be renaming variables to move 
	//all opis and aux to the end to facilitate sampling.
	vector<vector<int32_t>> startStateClauses;
	if(setState0){
		printf("Adding clauses setting state 0 to 0\n");	
		for (uint32_t i=0; i<nStateVars; i++){
			vector<int> clause0;
			clause0.push_back(-(i+1)); //set all state0 vars to false. we rename vars to make state vars come first 
			//and push all opis later. see loop below for details 
			startStateClauses.push_back(clause0);
		}
		assert(startStateClauses.size()>0);
		fullCNF.insert(fullCNF.end(),startStateClauses.begin(),startStateClauses.end());
	}
	cout<<"Unrolling:"<<std::flush;
	/*
		put all ip, followed by all aux at the end after statevars*t+1
		var<nip then var += (nsv)(t+1)+i*nip 
		nip<var<nip+2*nsv then var+= i*nsv - nOPI
		nip+2*nsv<var<nip+2nsv+naux then var += (t+1)*nsv + t*nip+ i*naux

		if var<2*nStateVars then var+= i*nStateVars
		else var+=totalstatevars+i*numAuxVars
	*/
	//we process baseCNF in this loop also since we are relabeling all variables
	for(uint32_t i = 0; i<traceLength; i++){
		printf("Iter %d ",i);
		vector<vector<int32_t>> baseCopy (m->clauses);
		for (auto& clause : baseCopy){
			for (uint32_t j = 0; j<clause.size();j++){
				uint32_t vn = abs(clause.at(j))-1;//0 indexed
				if(vn<nOPIs){
					clause.at(j) += clause.at(j) < 0? -nStateVars*(traceLength+1)-i*(nOPIs) : 
						nStateVars*(traceLength+1)+i*(nOPIs);
				} else if (vn<nOPIs+2*nStateVars){
					clause.at(j) += clause.at(j) < 0? -i*nStateVars + nOPIs: i*nStateVars - nOPIs;
				} else if (vn<nOPIs+2*nStateVars+nAuxVars){
					clause.at(j) += clause.at(j) < 0? nOPIs+2*nStateVars -nStateVars*(traceLength+1) - traceLength*(nOPIs) - i*nAuxVars : 
						-nOPIs-2*nStateVars+nStateVars*(traceLength+1) + traceLength*(nOPIs) + i*nAuxVars;
				} else{
					cout<<"WARNING: unexpected variable number "<< vn <<" (0 indexe) while unrolling cnf. ";
					cout<<"nOPI:"<<nOPIs<<" nStateVars:"<<nStateVars<<" nAux:"<<nAuxVars<<endl;
				}
			}
		}
		//i \in {1,...,traceLength-1}
		//at the end of ith iteration, constraints for i+1th transition will have been added.
		//so at end of last iteration with i=tracelenght-1, the constraints for traceLength-th transition would be added
		fullCNF.insert(fullCNF.end(),baseCopy.begin(),baseCopy.end());
	}
	printf("\n");
	
	//write to file
	FILE* of = fopen(outFile,"w");
	fprintf(of,"c CNF converted from Aiger file\n");
	
	//add c ind
	FILE* indF = fopen(strcat(outFile,".ind"),"w");
	fprintf(of,"c ind "); fprintf(indF,"v "); // dsharp requires file to be in v vars 0 format
	for (uint32_t i = 0; i<(traceLength+1)*nStateVars; i++){
		fprintf(of,"%u ",i+1); fprintf(indF,"%u ",i+1);
	}
	fprintf(of,"0\n"); fprintf(indF,"0\n");
	fclose(indF);
	//add c tracelength numOPIs nStateVars nauxVars  for 1-step cnf
	fprintf(of,"c %u %u %u %u\n",traceLength,nOPIs,nStateVars,nAuxVars); 
	fprintf(of,"p cnf %u %lu\n",(traceLength+1)*nStateVars+traceLength*(nOPIs+nAuxVars),fullCNF.size());
	for(uint32_t i = 0; i<fullCNF.size();i++){
		for (uint32_t j = 0; j<fullCNF[i].size();j++){
			fprintf(of,"%d ",fullCNF[i][j]);
		}
		fprintf(of,"0\n");
	}

	fclose(of);
	if(!comments.empty()){
		cout<<"Adding "<<comments.size()<<" comments to File "<<outFile<<" .."<<std::flush;
		std::ofstream out(string(outFile),std::ofstream::app);
		for (auto& comment: comments){
			out << "c "<<comment<<"\n";
		}
		out.close();
	}
}

CNFWrapper* CNFWrapper::And(DDWrapper* other){
	return new CNFWrapper(node.AND(((CNFWrapper*) other)->node));
}

CNFWrapper* CNFWrapper::Complement(){
	return new CNFWrapper(-node);
}

CNFWrapper* CNFWrapper::ExistAbstract(DDWrapper* cube){
	//nothing to do here since we will be adding the auxiliaries and OPIs to the non-ind support as 'c ind'...
	return new CNFWrapper(this);
}

void* CNFWrapper::getNode(){
	 return &node;
}

void CNFWrapper::printStats(uint8_t verbosity){

}

//=========================================================================================================================
CNFLit CNFManager::getFalse(){
	CNFLit c(false,this);
	return c;
}

bool CNFManager::isFalse(CNFLit c){
	if(c.varNum!=0) return false;
	return !c.oneZero;
}

bool CNFManager::isTrue(CNFLit c){
	if(c.varNum!=0) return false;
	return c.oneZero;
}

CNFLit CNFManager::getTrue(){
	CNFLit c(true,this);
	return c;
}

CNFLit CNFManager::getPosLit(uint32_t varNum_, uint32_t type){
	assert(varNum_!=0);
	if(varNum_ > maxVar){
		maxVar = varNum_;
		// cout<<"Maxvar: "<<maxVar<<"\n";
	}
	// cout<<"manager address:"<<this<<endl;
	switch (type){
		case CNFManager::OPI:
			OPIs.insert(varNum_);
			break;
		case CNFManager::CURRSTATEVAR:
			currStateVars.insert(varNum_);
			break;
		case CNFManager::NEXTSTATEVAR:
			nextStateVars.insert(varNum_);
			break;
		default:
			cout<<"Unrecognized var type in CNFManager getLit():"<<type<<" varNum:"<<varNum_<<". Exiting..\n";
			exit(0);
			break;
	}
	varMap.emplace(varNum_,type);
	CNFLit c((int32_t)varNum_,this);
	return c;
}

std::pair<CNFLit,CNFLit> CNFManager::getANDChildren(CNFLit parent){
	assert(!isFalse(parent) && !isTrue(parent));
	uint32_t startCls = varMap[abs(parent.varNum)];
	int32_t first = clauses.at(startCls).at(1);
	int32_t second = clauses.at(startCls+1).at(1);
	assert(clauses.at(startCls+2).at(1)==-first && clauses.at(startCls+2).at(2)==-second);
	return(std::make_pair(CNFLit(first,this),CNFLit(second,this)));
}

CNFLit CNFManager::AND(CNFLit first, CNFLit second){
	// cout<<"Doing AND in manager..\n";
	// cout<<"manager address:"<<this<<endl;
	if (isFalse(first) || isFalse(second)){
		return getFalse();
	}
	if (isTrue(first)) return second;
	if (isTrue(second)) return first;
	// cout<<"None of the lits are constants.. computing clauses..\n";
	// printf("%p\n",&(this->maxVar));
	maxVar++;
	// cout<<"a\n";
	varMap.emplace((uint32_t)abs(maxVar),(uint32_t)clauses.size());
	// cout<<"b\n";
	auxVars.insert(abs(maxVar));
	// cout<<"Adding clauses\n";
	clauses.push_back(vector<int32_t>{-maxVar, first.varNum});
	clauses.push_back(vector<int32_t>{-maxVar, second.varNum});
	clauses.push_back(vector<int32_t>{maxVar, -first.varNum,-second.varNum});
	// cout<<"Computed clauses.. returning\n";
	return CNFLit(maxVar,this);
}
//=========================================================================================================================
unsigned Aig2dd::lit2andInd(unsigned lit){
	// index into nodeHash. In binary format, inputs latches and ands are
	// sequentially numbered.
	return (aiger_lit2var(lit) - a->num_inputs - a->num_latches - 1); 
}
Aig2dd::Aig2dd(char* inFile_, char* outFile_, char* checkFile_, Cudd_ReorderingType reoType_, uint32_t outputType_,
	bool setState0_, uint32_t traceLength_,int contiguousAll, uint8_t interleaved_, double_t startTime_): 
	inFile(inFile_), outFile(outFile_), checkFile(checkFile_), traceLength(traceLength_), outputType(outputType_), setState0(setState0_),
	reoType(reoType_), mgr(0,0), interleaved(interleaved_), startTime(startTime_) {
	
	a = aiger_init();
	cout<<"Reading aiger model.. "<<std::flush;
	const char* rSuccess = aiger_open_and_read_from_file (a, inFile);
	if (rSuccess!=NULL){
		printf("%s\n",rSuccess);
		exit(1);
	}
	cout<<"Model read successfully:\n";
	cout<<" num_inputs:"<<a->num_inputs<<" num_latches:"<<a->num_latches;
	cout<<" Original inputs:"<<a->num_inputs-a->num_latches<<" state vars:"<<a->num_latches<<endl;
	readAigTime = cpuTimeTotal();
	comments.push_back(printTimeTaken(string("Read Aig Model"),readAigTime-startTime, 2));
	// create numPI + numLatches number of bdd vars
	
	// create array of size numAnds initialized to null which will hold the ddnode for output of each
	// and gate once it is created. This will store regular versions of all pointers. Caller will have to 
	// complement if needed.
	nodeHash.resize(a->num_ands); // all nodehash pointers are null
	nodeHashSize = a->num_ands; nodesFilled=0;
	vars.resize((a->num_inputs)+(a->num_latches));
	/*In the following, vars are so indexed to ensure that wrt index, variables are in order 
	  actualprimaryinputs-state0-state1 . The program was originally written with the order
	  actualprimaryinputs-state1-state0 in mind, but for the sampling program the previous ordering was assumed.
	  Therefore to ensure correct correspondence of vars, the jugglery below is required. During sanity check
	  (traversing) we proceed under the assumption that indices (a->num_inputs)-(a->num_latches) through (a->num_inputs)
	  are state 0 and a->num_inputs through (a->num_inputs)+(a->num_latches) are state 1*/
	if (outputType==0){
		one = new BDDWrapper(mgr.bddOne());
		zero = new BDDWrapper(mgr.bddZero());	
		for (int i = 0; i< (a->num_inputs)-(a->num_latches); i++){
			vars[i] = new BDDWrapper(mgr.bddVar(i));
		}
		for (int i = (a->num_inputs)-(a->num_latches); i< (a->num_inputs); i++){
			vars[i] = new BDDWrapper(mgr.bddVar(i+a->num_latches));
		}
		for (int i = a->num_inputs; i< (a->num_inputs)+(a->num_latches); i++){
			vars[i] = new BDDWrapper(mgr.bddVar(i-a->num_latches));
		}
	} else if (outputType==1){
		one = new ADDWrapper(mgr.addOne());
		zero = new ADDWrapper(mgr.addZero());
		for (int i = 0; i< (a->num_inputs)-(a->num_latches); i++){
			vars[i] = new ADDWrapper(mgr.addVar(i));
		}
		for (int i = (a->num_inputs)-(a->num_latches); i< (a->num_inputs); i++){
			vars[i] = new ADDWrapper(mgr.addVar(i+a->num_latches));
		}	
		for (int i = a->num_inputs; i< (a->num_inputs)+(a->num_latches); i++){
			vars[i] = new ADDWrapper(mgr.addVar(i-a->num_latches));
		}
	} else if (outputType==2){
		cout<<"Estimated # of clauses:"<<a->num_ands*3*traceLength_<<endl;
		if (a->num_ands*3*traceLength_>1000000){
			cout<<"Unrolled CNF will be too large (estimated #clauses > 1,000,000). Exiting..\n";
			exit(0);
		}
		contiguousAll=0; // since dealing with CNFs, no need to worry about ranges
		zero = new CNFWrapper(cnfMgr.getFalse());
		one = new CNFWrapper(cnfMgr.getTrue());
		for (int i = 0; i< (a->num_inputs)-(a->num_latches); i++){
			vars[i] = new CNFWrapper(cnfMgr.getPosLit(i+1,CNFManager::OPI));//+1 since vars in cnf are 1-indexed
		}
		for (int i = (a->num_inputs)-(a->num_latches); i< (a->num_inputs); i++){
			vars[i] = new CNFWrapper(cnfMgr.getPosLit(i+a->num_latches+1,CNFManager::NEXTSTATEVAR));
		}	
		for (int i = a->num_inputs; i< (a->num_inputs)+(a->num_latches); i++){
			vars[i] = new CNFWrapper(cnfMgr.getPosLit(i-a->num_latches+1,CNFManager::CURRSTATEVAR));
		}
	}

	dd = mgr.getManager();
	// /* set variable groups*/
	
	MtrNode* tr = Mtr_InitGroupTree(0,a->num_inputs+a->num_latches);
	cout<<"Creating groups.. "<<std::flush;
	
	// not trying to enforce group ordering for now
	//Mtr_MakeGroup(tr,0,a->num_inputs,MTR_DEFAULT);
	bool groupOPIs = false;
	if (groupOPIs){
		Cudd_MakeTreeNode(dd,0,a->num_inputs-a->num_latches,MTR_DEFAULT);
	}

	if(contiguousAll==2){
		cout<<" Adding contiguity constraints to actual primary inputs.. "<<std::flush;
		contiguityRanges.push_back(std::make_pair(std::make_pair(0,a->num_inputs-a->num_latches),MTR_DEFAULT));
		Mtr_MakeGroup(tr,0,a->num_inputs-a->num_latches,MTR_DEFAULT);

		cout<<" Adding contiguity constraints to state0.. "<<std::flush;
		Mtr_MakeGroup(tr,a->num_inputs-a->num_latches,a->num_latches,MTR_DEFAULT);
		contiguityRanges.push_back(std::make_pair(std::make_pair(a->num_inputs-a->num_latches,a->num_latches),MTR_DEFAULT));
	}
	if (contiguousAll>=1){
		cout<<" Adding contiguity constraints to state1.. "<<std::flush;
		Mtr_MakeGroup(tr,a->num_inputs,a->num_latches,MTR_DEFAULT);
		contiguityRanges.push_back(std::make_pair(std::make_pair(a->num_inputs,a->num_latches),MTR_DEFAULT));
		Mtr_PrintTree(tr);
		Cudd_SetTree(dd,tr);
	} else {
		cout<<" Skipped adding contiguity ranges.."<<std::flush;
		if(interleaved == 1){
			int* invperm = getInterleavedOrder(a->num_inputs-a->num_latches,a->num_latches);
			string inperm;
			for (uint32_t i = 0; i<a->num_inputs+a->num_latches; i++){ inperm += std::to_string(invperm[i]) + " ";}
			mgr.ShuffleHeap(invperm);
			comments.push_back(inperm);
			cout<<" Variable order set to interleaved. Enabling dynamic reordering.. "<<std::flush;
			free(invperm);
		}
	}
	mgr.AutodynEnable(reoType);
	cout<<" Done setting all groups and reorders!\n";
}

int* Aig2dd::getInterleavedOrder(uint32_t nOPIs, uint32_t nStateVars){
	int* invperm = (int*) malloc(sizeof(int)*(nOPIs+nStateVars*2));
	for (uint32_t i = 0; i< nOPIs; i++){
		invperm[i] = i;
	}
	for (uint32_t i = nOPIs, j = nOPIs; j< nOPIs+nStateVars; j++,i+=2){
		invperm[i] = j;
		invperm[i+1] = j+nStateVars;
	}
	return invperm;
}

void Aig2dd::sanity_check(DDWrapper* fullDD){
	StimAig s(checkFile);
	RandomBits rb;
	rb.SeedEngine();
	// ni nl,no are of original aig file before process_latches.
	int ni = s.getNumInputs();
	int nl = s.getNumLatches();
	int no = s.getNumPOs();
	//printf("NI %d NL %d NO %d\n",ni,nl,no);
	//printf("numInputs %d numLatches %d\n",a->num_inputs, a->num_latches);
	assert(ni+nl==a->num_inputs);
	assert(nl==a->num_latches);
	int i = 0, j=0, numPosTrials = 2000, numNegTrials = 200, numPerturbsPerNegTrial = 10;
	cout<<" Conducting "<<numPosTrials<<" positive trials .. "<<std::flush; 
	vector<bool> inputVec(ni), stateVec(nl), resL(nl), resO(no), perturbedResL(nl);
	while (i<numPosTrials){
		//cout<<ni<<" Inputs(0/1):"<<endl;
		string r = rb.GenerateRandomBits(ni);
		for (int j = 0; j<ni;j++) {inputVec[j]=(r[j]=='1');}
		//cout<<nl<<" Latches(0/1):"<<endl;
		r = rb.GenerateRandomBits(nl);
		for (int j = 0; j<nl;j++) {stateVec[j]=(r[j]=='1');}
		s.stimulate(inputVec,stateVec,resL, resO);
		cnfMgr.auxVals.clear();
		int acc = fullDD->Traverse(inputVec,stateVec, resL, ni, nl);
		//cout<<"in pos trial Acc: "<<acc<<endl;
		if (!acc){
			cout<<"Positive trial failed for the following:\n";
			printVecs(inputVec,stateVec,resL, ni, nl);
			cout<<"\nExiting..\n";
			exit(1);
		}
		i++;
		if(i==1){ printTimeTaken("Check 1 Pos-Sample",cpuTimeTotal()-convTime,0);}
		if(i==10){ printTimeTaken("Check 10 Pos Sample",cpuTimeTotal()-convTime,0);}
		if(i==100){ printTimeTaken("Check 100 Pos Sample",cpuTimeTotal()-convTime,0);}
		if(i==1000){ printTimeTaken("Check 1000 Pos Sample",cpuTimeTotal()-convTime,0);}
	}
	i=0;
	cout<<" Conducting "<<numNegTrials<<" x "<<numPerturbsPerNegTrial<<
	" negative trials.. "<<std::flush;
	while (i<numNegTrials){
		//cout<<ni<<" Inputs(0/1):"<<endl;
		string r = rb.GenerateRandomBits(ni);
		for (int j = 0; j<ni;j++) {inputVec[j]=(r[j]=='1');}
		//cout<<nl<<" Latches(0/1):"<<endl;
		r= rb.GenerateRandomBits(nl);
		for (int j = 0; j<nl;j++) {stateVec[j]=(r[j]=='1');}
		s.stimulate(inputVec,stateVec,resL, resO);
		j=0;
		while(j<numPerturbsPerNegTrial){
			perturbedResL = resL;
			string r2 = rb.GenerateRandomBits(nl);
			bool changed = false;
			for (int k = 0; k<nl; k++){
				if (r2[k]=='1'){
					perturbedResL[k].flip();
					changed = true;
				}
			}
			if (!changed) continue;
			cnfMgr.auxVals.clear();
			int acc = fullDD->Traverse(inputVec,stateVec, perturbedResL, ni, nl);
			if (acc){
				cout<<"Negative trial failed for the following:\n";
				printVecs(inputVec,stateVec,perturbedResL, ni, nl);
				cout<<"\nExiting..\n";
				exit(1);
			}
			j++;
		} 
		i++;
	}
	cout<<" done all trials! "<<std::flush;
}

void Aig2dd::printVecs(vector<bool> inputVec, vector<bool> stateVec, vector<bool> resL, int ni, int nl){
	cout<<"Input vec:\n";
	for (int k = 0; k< ni; k++){
		cout<<inputVec[k]<<" ";
	}
	cout<<"\nState vec:\n";
	for (int k = 0; k< nl; k++){
		cout<<stateVec[k]<<" ";
	}
	cout<<"Res vec / perturbed res vec (for negative trials):\n";
	for (int k = 0; k< nl; k++){
		cout<<resL[k]<<" ";
	}
}

void* Aig2dd::aig2dd(){
	unsigned outLit = a->outputs[0].lit;
	cout<<"Starting conversion (Output literal is: "<<outLit<<") .. \n";
	cout<<"NodeHashSize:"<<nodeHashSize<<" filled: "<<std::flush;
	//aiger_strip converts a lit to its even (non - negated form). This is required by aiger_is_and
	aiger_and* opAnd = aiger_is_and(a,aiger_strip(outLit));
	assert(("PO is not the result of an and gate\n", opAnd!=NULL));
	
	//recurse over the and gates left and right inputs
	aig2dd_rec(opAnd);
	DDWrapper* resW = 0;
	if(outLit%2){
		resW = nodeHash[lit2andInd(outLit)]->Complement();
	}  else{
		resW = nodeHash[lit2andInd(outLit)];
	}
	printf(" Successfully converted Aiger!\n");
	convTime = cpuTimeTotal();
	comments.push_back(printTimeTaken(string("Conversion"),convTime- readAigTime, 2));
	resW->printStats(0);
	#if DFAST == 2
	if (outputType!=2){   //for fast mode, dont do sanity check if outputType is CNF
	#endif
		cout<<"Performing sanity check.. "<<std::flush;
		sanity_check(resW);
		//exist abstract pis
		cout<<"Sanity check passed!\n";
	#if DFAST == 2
	}
	#endif
	sanCheckTime = cpuTimeTotal();
	comments.push_back(printTimeTaken(string("Sanity Check"), sanCheckTime - convTime, 2));
	cout<<"Constructing cube for exist abstract.."<<std::flush;
	DDWrapper* opi_temp;
	DDWrapper* opis = one;	
	if(outputType<2){
		for (int i=0; i<(a->num_inputs-a->num_latches); i++){
			opi_temp = opis->And(vars[i]);
			if(i!=0){ 			//don't delete 'one'
				delete opis;
			}
			opis = opi_temp;
		}
	}
	DDWrapper* ea = 0;
	cout<<" Abstracting out cube "<<opis<<" of "<<(a->num_inputs-a->num_latches)<<" variables on "<<resW<<std::flush;
	ea = resW->ExistAbstract(opis);	
	printf(" Done abstract!\n");
	abstrTime = cpuTimeTotal();
	comments.push_back(printTimeTaken(string("Abstraction"),abstrTime - sanCheckTime, 2));
	delete resW;
	ea->printStats(1);
	//write bdd to file
	cout<<"Writing DD/CNF to file.. "<<std::flush;
	if(outputType==1){
		//for state_seq info see constructor for statevar ordering details
		writeDDToFile(dd,inFile,(DdNode*)ea->getNode(),DDDMP_ADD,NULL,NULL,DDDMP_MODE_TEXT, DDDMP_VARIDS,outFile,
		a->num_inputs,a->num_latches,STATE_SEQ_01, contiguityRanges, comments);
	} else if (outputType==0){
		//for state_seq info see constructor for statevar ordering details
		writeDDToFile(dd,inFile,(DdNode*)ea->getNode(),DDDMP_BDD,NULL,NULL,DDDMP_MODE_BINARY, DDDMP_VARIDS,outFile,
		a->num_inputs,a->num_latches,STATE_SEQ_01, contiguityRanges,comments);
	} else {
		((CNFWrapper*)ea)->store(outFile,setState0,traceLength, comments);
	}
	cout<<" Done Writing!\n";
	writeTime = cpuTimeTotal();
	printTimeTaken(string("Write File"),writeTime - abstrTime, 2);
	return(ea->getNode());
}

DDWrapper* Aig2dd::parseAndGateIns(unsigned ilit, unsigned short* iRefFlag){
	unsigned short ic = ilit %2;
	DDWrapper* iNode = NULL;
	assert (*iRefFlag == 0); // must be initialized properly
	unsigned v, ii;
	switch (aiger_lit2tag(a,ilit)){
		case 0:
			iNode =  ( ic ? one:zero );
			// dont set irefflag to 1 since no new mem allocated
			break;
		case 1: 
			// see aiger_format-2007.pdf. In binary format, all inputs and lits are sequenitally
			//numbered. So we can use the variable number to index into the array of bdd vars.
			v = aiger_lit2var(ilit) - 1; //variables in bdd are 0 indexed
			assert(("input variable number greater than num_inputs\n",v<a->num_inputs));
			if (ic){
				iNode = vars[v]->Complement();
				*iRefFlag = 1; // new mem allocated so destroy later
			} else {
				iNode = vars[v];
			}
			break;
		case 2:
			v = aiger_lit2var(ilit) - 1;
			assert(("latch variable number not between numinputs and num_latches\n",v >= a->num_inputs
				&& v <= (a->num_inputs + a->num_latches) ));
			if (ic){
				iNode = vars[v]->Complement();
				*iRefFlag = 1; // new mem allocated so destroy later
			} else {
			 	iNode = vars[v];
			}
			break; 
		case 3:
			ii = lit2andInd(ilit);
			if (nodeHash[ii] == NULL){
				//aiger_strip converts a lit to its even (non - negated form). This is required by aiger_is_and
				aig2dd_rec(aiger_is_and(a,aiger_strip(ilit)));
			} 
			// res is Refed before returning from aig2bdd-rec. No need to ref again.
			if (ic){
				iNode = nodeHash[ii]->Complement();
				*iRefFlag = 1; // new mem allocated so destroy later
			} else {
			 	iNode = nodeHash[ii];
			}
			break;
		default:
			printf("Unexpected output %d of lit2tag. Exiting..\n",aiger_lit2tag(a,ilit));
			exit(1);
	}
	return (iNode);
}

void Aig2dd::aig2dd_rec(aiger_and* andNode){
	// printf("Doing rec\n");
	unsigned andIndex = lit2andInd(andNode->lhs);
	// printf("%u %u %u %p\n",andNode->lhs,andIndex, a->num_ands,nodeHash[andIndex]);
	// assert(("Check for whether ddnode exists in nodehash should have been done before calling \
	aig2bdd_rec\n",(nodeHash[andIndex] == NULL)));
	
	unsigned llit = andNode->rhs0 ; unsigned short lRefFlag = 0;
	unsigned rlit = andNode->rhs1 ; unsigned short rRefFlag = 0;
	DDWrapper* lNode = parseAndGateIns(llit, &lRefFlag), *rNode = parseAndGateIns(rlit, &rRefFlag);
	// printf("Computing res from %p %p\n",lNode, rNode);
	DDWrapper* res;
	res = lNode->And(rNode);
	if (lRefFlag) delete lNode;
	if (rRefFlag) delete rNode;
	// printf("Res is %p\n",res);
	// printf("Computing nodehash\n");
	nodeHash[andIndex] = res;
	nodesFilled++;
	if((nodesFilled%(std::max(1u,nodeHashSize/10))==1)||(nodeHashSize-nodesFilled<10)){
		cout<<nodesFilled<<":"<<cpuTimeTotal()-readAigTime<<" "<<std::flush;
	}
}

int main(int argc, char** argv){
	double startTime = cpuTimeTotal();
	cout<< "Starting aig2dd at ";
	print_wall_time(); cout<<endl;
	if(argc!=8){
		cout<<"USAGE: aig2dd <infile> <outfile> <checkfile> <reordering heuristic> <outType cnf=2[0|1][traceLength]/add=1/bdd=0>" 
		" <contiguousAll=/0/1/2> <interleaved 0=off/1=on>\n";
		cout<<"if outType starts with 2 i.e. CNF, then you can specify restrictStartState (default=1/true) and"
		" traceLength (default=1) params after it\n";
		cout<<"For example, if outType param is 211024, in means that set output type to CNF, restrict start state"
		" and unroll 1024 times.\n";
		cout<<"interleaved currently can be used only if contiguousAll = 0, o/w ignored\n";
		cout<<"reordering types 0-none, 1-sift, 2-groupsift, 3-symmsift, 4-random\n";
		exit(1);
	}
	char *inFile_ = argv[1], *outFile_ = argv[2], *checkFile_ = argv[3], *outputTypeStr_ = argv[5];
	uint32_t outputType_, traceLength_; bool setState0_ = true;
	int contiguousAll_ = strtol(argv[6],NULL,10); 
	uint8_t interleaved_; Cudd_ReorderingType reoType_;
	string reoStr;
	switch (argv[4][0]){
		case '0':
			reoType_ = CUDD_REORDER_NONE;
			reoStr = "None";
			break;
		case '1':
			reoType_ = CUDD_REORDER_SIFT;
			reoStr = "sift";
			break;
		case '2':
			reoType_ = CUDD_REORDER_GROUP_SIFT;
			reoStr = "group";
			break;
		case '3':
			reoType_ = CUDD_REORDER_SYMM_SIFT;
			reoStr = "symm";
			break;
		case '4':
			reoType_ = CUDD_REORDER_RANDOM;
			reoStr = "random";
			break;
		default:
			cout<<"Unrecognized reordering type. Exiting..\n";
			exit(0);
			break;
	}
	if(argv[7][0]=='0') interleaved_ = 0;
	else if (argv[7][0]=='1') interleaved_ = 1;
	else {cout<<"Unrecognized interleaved:"<<interleaved_<<". Exiting..\n"; exit(0);}
	printDoubleLine();
	#if DFAST == 0
		cout<<"DFAST is 0 (Debug Mode).\n";
	#elif DFAST == 1
		cout<<"DFAST is 1 (Regular Mode).\n";
	#elif DFAST == 2
		cout<<"DFAST is 2 (Fast Mode).\n";
	#else
		int dfast_val = DFAST;
		cout<<"DFAST has unrecognized value:"<<dfast_val<<". Exiting..\n";
		exit(0);
	#endif
	cout<<"aig2dd invoked with inFile:"<<string(inFile_)<<" outFile:"<<string(outFile_)<<" checkFile:"<<string(checkFile_)
	<<" reorderingType:"<<reoStr<<"\n";
	if(outputTypeStr_[0]=='0'){
		outputType_ = 0;
		cout<<"OutputType: BDD\n";
	} else if (outputTypeStr_[0]=='1'){
		outputType_ = 1;
		cout<<"OutputType: ADD\n";
	} else if (outputTypeStr_[0]=='2'){
		outputType_ = 2;
		if (outputTypeStr_[1]=='0'){
			setState0_ = false;
		} else if (outputTypeStr_[1]=='1'){
			setState0_ = true;
		}
		else{
			if (outputTypeStr_[1]!=0){
				cout<<"outputType detected to be CNF (2) but next character neither 0 nor 1 nor" 
					"end-of-string: ",outputTypeStr_[1];
				cout<<" Exiting..\n";
				exit(0); 
			}
		}
		uint32_t length = strtol(&outputTypeStr_[2],NULL,10);
		traceLength_ = length>1? length: 1;
		cout<<"CNF requested as outputType. RestrictState0 set to "<<setState0_<<" traceLength set to "<<traceLength_<<"\n";
	}
	else{
		cout<<"Unrecognized value for parameter <outputType cnf=2[0|1][traceLength]/add=1/bdd=0>: "
			<<outputType_<<" exiting..\n";
		exit(0);
	}
	if(contiguousAll_ == 0){
		cout<<"contiguousAll is 0. Not adding any contiguity constraints at all.\n";
	} else if (contiguousAll_==1){
		cout<<"contiguousAll is 1. Adding contiguity constraints only for state1.\n";
	} else if (contiguousAll_==2){
		cout<<"contiguousAll is 2. Adding contiguity constraints for all (state0, state1 and PI vars).\n";
	} else{
		cout<<"contiguousAll pararmeter not recognized:"<<contiguousAll_<<". Exiting..\n";
		exit(1);
	}
	cout<<"Interleaved: "<<(uint32_t)interleaved_<<"\n";
	printDoubleLine();
	Aig2dd a(inFile_,outFile_,checkFile_, reoType_, outputType_, setState0_,traceLength_, contiguousAll_, interleaved_, startTime);
	a.aig2dd();
	printDoubleLine();
	cout<<endl<<endl<< "aig2dd ended at ";
	print_wall_time(); cout<<endl;
	printTimeTaken(string("Total"),(cpuTimeTotal()-startTime),1);
}

//TODO: store function. printstats for cnf