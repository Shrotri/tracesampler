all: RandomBits.o aig2dd.o aiger.o stimAIG_main.o sample_trace.o Tsample_trace.o testCudd.o utils.o dd2cnf.o SamplerNodeFactory.o SamplerNode.o TSamplerNodeFactory.o TSamplerNode.o NumType.o GMPNumType.o
	g++ -std=c++11 -O3 aig2dd.o aiger.o utils.o RandomBits.o stimAIG.o -o aig2dd -lcudd -L ~/Downloads/cudd/dddmp/.libs/ -ldddmp
	g++ -std=c++11 -O3 -o stimAIG stimAIG_main.o stimAIG.o aiger.o
	g++ -std=c++11 -o sample_trace RandomBits.o utils.o NumType.o GMPNumType.o SamplerNode.o SamplerNodeFactory.o sample_trace.o -lcudd -L ~/Downloads/cudd/dddmp/.libs/ -ldddmp -lgmp
	g++ -std=c++11 -o Tsample_trace RandomBits.o utils.o TSamplerNode.o TSamplerNodeFactory.o Tsample_trace.o -lcudd -L ~/Downloads/cudd/dddmp/.libs/ -ldddmp
	g++ -std=c++11 -o dd2cnf utils.o dd2cnf.o -lcudd -L ~/Downloads/cudd/dddmp/.libs/ -ldddmp
	g++ -std=c++11 -o testCudd RandomBits.o testCudd.o -lcudd
	
RandomBits.o: RandomBits.h RandomBits.cpp
	g++ -std=c++11 -O3 -c RandomBits.cpp

aig2dd.o: aig2dd.cpp aig2dd.hpp aiger.h
	g++ -std=c++11 -O3 -c aig2dd.cpp

stimAIG_main.o: stimAIG_main.cpp stimAIG.o
	g++ -std=c++11 -O3 -c stimAIG_main.cpp

stimAIG.o: stimAIG.cpp stimAIG.hpp
	g++ -std=c++11 -O3 -c stimAIG.cpp

aiger.o: aiger.c
	gcc -c -O3 aiger.c
	
utils.o: utils.cpp utils.hpp
	g++ -std=c++11 -c utils.cpp

sample_trace.o: sample_trace.cpp sample_trace.hpp SamplerNode.hpp
	g++ -std=c++11 -c sample_trace.cpp

Tsample_trace.o: Tsample_trace.cpp Tsample_trace.hpp TSamplerNode.hpp
	g++ -std=c++11 -c Tsample_trace.cpp

testCudd.o: testCudd.cpp RandomBits.h RandomBits.cpp
	g++ -std=c++11 -c testCudd.cpp

dd2cnf.o: dd2cnf.cpp dd2cnf.hpp
	g++ -std=c++11 -c dd2cnf.cpp

NumType.o: NumType.cpp NumType.hpp
	g++ -std=c++11 -c NumType.cpp

GMPNumType.o: GMPNumType.cpp GMPNumType.hpp NumType.hpp
	g++ -std=c++11 -c GMPNumType.cpp

SamplerNodeFactory.o: SamplerNodeFactory.cpp SamplerNodeFactory.hpp
	g++ -std=c++11 -c SamplerNodeFactory.cpp

SamplerNode.o: SamplerNode.cpp SamplerNode.hpp
	g++ -std=c++11 -c SamplerNode.cpp

TSamplerNodeFactory.o: TSamplerNodeFactory.cpp TSamplerNodeFactory.hpp
	g++ -std=c++11 -c TSamplerNodeFactory.cpp

TSamplerNode.o: TSamplerNode.cpp TSamplerNode.hpp
	g++ -std=c++11 -c TSamplerNode.cpp


clean:
	rm *.o
