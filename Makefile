LINKER_ALL = 
RP = 
INCLUDE_ALL = 

ifeq ($(CUDD),)
LINKER_ALL += -lcudd
else
CUDD_INCLUDE = -I$(CUDD) -I$(CUDD)/cudd -I$(CUDD)/epd -I$(CUDD)/mtr -I$(CUDD)/st -I$(CUDD)/cplusplus/ -I$(CUDD)/util/ -I$(CUDD)/dddmp/
INCLUDE_ALL += $(CUDD_INCLUDE)
CUDD_LIBS = $(CUDD)/cudd/.libs/
LINKER_ALL += -L $(CUDD_LIBS) -lcudd
RP += -Wl,-rpath,$(CUDD_LIBS)
endif

ifeq ($(DDDMP),)
LINKER_ALL += 
else
DDDMP_INCLUDE = -I$(DDDMP)
INCLUDE_ALL += $(DDDMP_INCLUDE)
DDDMP_LIBS = $(DDDMP)/.libs/
LINKER_ALL += -L $(DDDMP_LIBS) -ldddmp
RP += -Wl,-rpath,$(DDDMP_LIBS)
endif

ifeq ($(GMP),)
LINKER_ALL += -lgmp
else
GMP_INCLUDE = -I$(GMP)
INCLUDE_ALL += $(GMP_INCLUDE)
GMP_LIBS = $(GMP)
LINKER_ALL += -L $(GMP_LIBS) -lgmp
RP += -Wl,-rpath,$(GMP_LIBS)
endif

LINKER_ALL := $(RP) $(LINKER_ALL)
################################################################################

SRC_DIR = src

################################################################################

CXX = g++

CXX_DEFAULT_FLAGS = -std=c++11

.f_compile_option:
	@echo "Fast compile option selected"

.r_compile_option:
	@echo "Regular compile option (default) selected"

.d_compile_option:
	@echo "Fast compile option selected"

################################################################################

reg: CXXFLAGS=$(CXX_DEFAULT_FLAGS) -DDFAST=1
reg: .r_compile_option all

fast: CXXFLAGS=$(CXX_DEFAULT_FLAGS) -O3 -DDFAST=2
fast: .f_compile_option all

debug: CXXFLAGS=$(CXX_DEFAULT_FLAGS) -g -DDFAST=0
debug: .d_compile_option all

all: RandomBits.o aig2dd.o aiger.o stimAIG_main.o stimAIG.o sample_trace.o Tsample_trace.o testCudd.o utils.o dd2cnf.o SamplerNodeFactory.o SamplerNode.o TSamplerNodeFactory.o TSamplerNode.o NumType.o GMPNumType.o
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -o aig2dd aig2dd.o aiger.o utils.o RandomBits.o stimAIG.o $(LINKER_ALL)
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -o stimAIG stimAIG_main.o stimAIG.o aiger.o
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -o sample_trace RandomBits.o utils.o NumType.o GMPNumType.o SamplerNode.o SamplerNodeFactory.o sample_trace.o $(LINKER_ALL)
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -o Tsample_trace RandomBits.o utils.o TSamplerNode.o TSamplerNodeFactory.o Tsample_trace.o $(LINKER_ALL)
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -o dd2cnf utils.o dd2cnf.o $(LINKER_ALL)
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -o testCudd RandomBits.o testCudd.o $(LINKER_ALL)

RandomBits.o: $(SRC_DIR)/RandomBits.hpp $(SRC_DIR)/RandomBits.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -c $(SRC_DIR)/RandomBits.cpp

aig2dd.o: $(SRC_DIR)/aig2dd.cpp $(SRC_DIR)/aig2dd.hpp $(SRC_DIR)/aiger.h
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -c $(SRC_DIR)/aig2dd.cpp

stimAIG_main.o: $(SRC_DIR)/stimAIG_main.cpp stimAIG.o
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -c $(SRC_DIR)/stimAIG_main.cpp

stimAIG.o: $(SRC_DIR)/stimAIG.cpp $(SRC_DIR)/stimAIG.hpp
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -c $(SRC_DIR)/stimAIG.cpp

aiger.o: $(SRC_DIR)/aiger.c $(SRC_DIR)/aiger.h
	gcc $(INCLUDE_ALL) -c $(SRC_DIR)/aiger.c
	
utils.o: $(SRC_DIR)/utils.cpp $(SRC_DIR)/utils.hpp
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -c $(SRC_DIR)/utils.cpp

sample_trace.o: $(SRC_DIR)/sample_trace.cpp $(SRC_DIR)/sample_trace.hpp $(SRC_DIR)/SamplerNode.hpp
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -c $(SRC_DIR)/sample_trace.cpp

Tsample_trace.o: $(SRC_DIR)/Tsample_trace.cpp $(SRC_DIR)/Tsample_trace.hpp $(SRC_DIR)/TSamplerNode.hpp
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -c $(SRC_DIR)/Tsample_trace.cpp

testCudd.o: $(SRC_DIR)/testCudd.cpp $(SRC_DIR)/RandomBits.hpp $(SRC_DIR)/RandomBits.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -c $(SRC_DIR)/testCudd.cpp

dd2cnf.o: $(SRC_DIR)/dd2cnf.cpp $(SRC_DIR)/dd2cnf.hpp
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -c $(SRC_DIR)/dd2cnf.cpp

NumType.o: $(SRC_DIR)/NumType.cpp $(SRC_DIR)/NumType.hpp
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -c $(SRC_DIR)/NumType.cpp

GMPNumType.o: $(SRC_DIR)/GMPNumType.cpp $(SRC_DIR)/GMPNumType.hpp $(SRC_DIR)/NumType.hpp
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -c $(SRC_DIR)/GMPNumType.cpp

SamplerNodeFactory.o: $(SRC_DIR)/SamplerNodeFactory.cpp $(SRC_DIR)/SamplerNodeFactory.hpp
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -c $(SRC_DIR)/SamplerNodeFactory.cpp

SamplerNode.o: $(SRC_DIR)/SamplerNode.cpp $(SRC_DIR)/SamplerNode.hpp
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -c $(SRC_DIR)/SamplerNode.cpp

TSamplerNodeFactory.o: $(SRC_DIR)/TSamplerNodeFactory.cpp $(SRC_DIR)/TSamplerNodeFactory.hpp
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -c $(SRC_DIR)/TSamplerNodeFactory.cpp

TSamplerNode.o: $(SRC_DIR)/TSamplerNode.cpp $(SRC_DIR)/TSamplerNode.hpp
	$(CXX) $(CXXFLAGS) $(INCLUDE_ALL) -c $(SRC_DIR)/TSamplerNode.cpp

################################################################################

.PHONY: clean

clean:
	rm -rf *.o