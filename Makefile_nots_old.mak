CUDD_INCLUDE = -I$(CUDD) -I$(CUDD)/cudd -I$(CUDD)/epd -I$(CUDD)/mtr -I$(CUDD)/st -I$(CUDD)/cplusplus/
DDDMP_INCLUDE = -I$(CUDD)/dddmp/ -I$(CUDD)/util/
CUDD_LIBS = $(CUDD)/cudd/.libs/
DDDMP_LIBS = $(CUDD)/dddmp/.libs/
all: RandomBits.o aig2dd.o aiger.o stimAIG_main.o sample_trace.o testCudd.o utils.o dd2cnf.o
        g++ -std=c++11 -O3 $(CUDD_INCLUDE) $(DDDMP_INCLUDE) aig2dd.o aiger.o utils.o RandomBits.o stimAIG.o -o aig2dd -Wl,-rpath,$(CUDD_LIBS) -L $(CUDD_LIBS) -lcudd
        g++ -std=c++11 -O3 -o stimAIG stimAIG_main.o stimAIG.o aiger.o
        g++ -std=c++11 -O3 $(CUDD_INCLUDE) $(DDDMP_INCLUDE) -o sample_trace RandomBits.o utils.o sample_trace.o -Wl,-rpath,$(CUDD_LIBS) -L $(CUDD_LIBS) -lcudd
        g++ -std=c++11 -O3 $(CUDD_INCLUDE) $(DDDMP_INCLUDE) -o dd2cnf utils.o dd2cnf.o -Wl,-rpath,$(CUDD_LIBS) -L $(CUDD_LIBS) -lcudd
        g++ -std=c++11 -O3 $(CUDD_INCLUDE) $(DDDMP_INCLUDE) -o testCudd testCudd.o -Wl,-rpath,$(CUDD_LIBS) -L $(CUDD_LIBS) -lcudd
        
RandomBits.o: RandomBits.h RandomBits.cpp
        g++ -std=c++11 -O3 -c RandomBits.cpp

aig2dd.o: aig2dd.cpp aig2dd.hpp aiger.h
        g++ -std=c++11 -O3 $(CUDD_INCLUDE) $(DDDMP_INCLUDE) -c aig2dd.cpp

stimAIG_main.o: stimAIG_main.cpp stimAIG.o
        g++ -std=c++11 -O3 -c stimAIG_main.cpp

stimAIG.o: stimAIG.cpp stimAIG.hpp
        g++ -std=c++11 -O3 -c stimAIG.cpp

aiger.o: aiger.c
        gcc -O3 -c aiger.c
        
utils.o: utils.cpp utils.hpp
        g++ -std=c++11 -O3 $(CUDD_INCLUDE) $(DDDMP_INCLUDE) -c utils.cpp

sample_trace.o: sample_trace.cpp sample_trace.hpp
        g++ -std=c++11 -O3 $(CUDD_INCLUDE) $(DDDMP_INCLUDE) -c sample_trace.cpp

testCudd.o: testCudd.cpp
        g++ -std=c++11 -O3 $(CUDD_INCLUDE) $(DDDMP_INCLUDE) -c testCudd.cpp

dd2cnf.o: dd2cnf.cpp dd2cnf.hpp
        g++ -std=c++11 -O3 $(CUDD_INCLUDE) $(DDDMP_INCLUDE) -c dd2cnf.cpp

clean:
        rm *.o
