import sys, os, parallelLocking as PL, subprocess as subp

"""
note: run with ulimit -v

inputs: bdd/meta file folder tracelength range, checktrace, numtraces, usereachable set, samplefileDir, comm_world_rank
		outputfileDir
		timeout for each individual run, wait time/sleep interval for locking
		startfile and endfile. 
		
algo:	each instance of script will get list of all bdds in folder. it will then wait to get 
		a lock on the startfile. Once it has a lock, it will read the file to determine which bdds are already being 
		processed. It will then pick a bdd not already being processed and write its name to startfile. it will close
		the startfile and release the lock. 
		Then for each tracelength in range it will invoke sample_trace on the bdd with appropriate parameters.
		use subprocess.popen (preferred) with output redirected to correct outputfile. need to specify timeout command.
		read outputfile or return value to determine if timeout occurred. if not go to next tracelength.

		if tracelength limit reached or timeout occurred, obtain lock on endfile. write currentfilename to endfile and 
		close and release lock.  

"""
print 'Spawned an instance of runSampleTrace_slurm.py'
if len(sys.argv)!=15:
	print len(sys.argv)
	print 'USAGE: bdddir, metadir, tracelength low high, checktrace, numtraces, usereachset, sampledir, timeout, waittime, sleeptime, startfile, endfile, outputdir'
	sys.exit(0)

bddDir = sys.argv[1]
metaDir = sys.argv[2]
tLLow = int(sys.argv[3])
tLHi = int(sys.argv[4])
cT = int(sys.argv[5])
nT = int(sys.argv[6])
uRS = int(sys.argv[7])
sampleDir = sys.argv[8]
tout = int(sys.argv[9])
waitT = int(sys.argv[10])
sleepT = int(sys.argv[11])
sFile = sys.argv[12]
eFile = sys.argv[13]
oDir = sys.argv[14]

assert(os.environ.has_key('STS'))
stPath = os.environ['STS']+'/'+'sample_trace'

allFiles = set()
for root, subdirs, files in os.walk(bddDir):
	for f in files:
		allFiles.add((os.path.relpath(root,bddDir),f))
#print 'AllFiles:', allFiles
while True:
	sf = PL.acquire_lock(sFile,'r+',waitT,sleepT)
	sfFiles = set()
	for line in sf:
		la = line.split(',')
		sfFiles.add((la[0],la[1][:-1]))
	#print 'StartFiles:',sfFiles
	diffFiles = allFiles - sfFiles
	if len(diffFiles) == 0:
		print 'Done processing all files!'
		break
	#print 'DiffFiles:',diffFiles
	fileToProcess = diffFiles.pop()
	sf.write(fileToProcess[0]+','+fileToProcess[1]+'\n')
	sf.close()
	print 'Processing file:',fileToProcess
	print 'Doing tracelength:'
	returncode = None
	for tL in range(tLLow,tLHi+1):
		if tL==tLHi:  #done to ensure that tL value is different if loop exits after timeout at last iteration
			break 
		print 2**tL,
		sys.stdout.flush()
		#TODO: mkdir -p sampledir, oDir
		arglist = ['timeout',str(tout),stPath,bddDir+'/'+fileToProcess[0]+'/'+fileToProcess[1],
		metaDir+'/'+fileToProcess[0]+'/'+fileToProcess[1]+'.meta', str(2**tL), str(cT), str(nT), str(uRS),
		sampleDir+'/'+fileToProcess[0]+'/'+fileToProcess[1]+'_'+str(2**tL)+'.samples']
		oHandle = open(oDir+'/'+fileToProcess[0]+'/'+fileToProcess[1]+'_'+str(2**tL)+'.out','w')		
		proc = subp.Popen(args=arglist,stdout=oHandle, stderr=subp.STDOUT)
		proc.communicate()
		returncode = proc.returncode
		oHandle.close()
		if returncode == 124:
			tmpF = open(oDir+'/'+fileToProcess[0]+'/'+fileToProcess[1]+'_'+str(2**tL)+'.out','a')
			tmpF.write('\nKILLLED BY TIMEOUT COMMAND\n')
			tmpF.close()
			break
	ef = PL.acquire_lock(eFile,'a',waitT,sleepT)
	ef.write(fileToProcess[0]+','+fileToProcess[1]+','+str(tL)+'\n')
	ef.close()
	print 'Done Processing file:',fileToProcess
