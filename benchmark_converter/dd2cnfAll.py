import sys, os, runOnAll

inDir = sys.argv[1]
mDir = sys.argv[2]
outDir = sys.argv[3]
t = int(sys.argv[4])
for tL in range (1,9):
	cmd = ['timeout',str(t),'../dd2cnf',None,None,None,str(2**tL),'2']
	for root, subdirs, files in os.walk(inDir):
		for fl in files:
			cmdc = list(cmd)
			cmdc[3] = root+'/'+fl
			relP = os.path.relpath(root,inDir)
			cmdc[4] = mDir+'/'+relP+'/'+fl+'.meta'
			cmdc[5] = outDir+'/'+relP+'/'+fl+'.cnf'
			cmdcs = ' '.join(cmdc)
			print 'Executing ',cmdcs
			os.system(cmdcs)