/**CFile****************************************************************

  FileName    [demo.c]

  SystemName  [ABC: Logic synthesis and verification system.]

  PackageName [ABC as a static library.]

  Synopsis    [A demo program illustrating the use of ABC as a static library.]

  Author      [Alan Mishchenko]
  
  Affiliation [UC Berkeley]

  Date        [Ver. 1.0. Started - June 20, 2005.]

  Revision    [$Id: demo.c,v 1.00 2005/11/14 00:00:00 alanmi Exp $]

***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

////////////////////////////////////////////////////////////////////////
///                        DECLARATIONS                              ///
////////////////////////////////////////////////////////////////////////

#if defined(ABC_NAMESPACE)
namespace ABC_NAMESPACE
{
#elif defined(__cplusplus)
extern "C"
{
#endif

// procedures to start and stop the ABC framework
// (should be called before and after the ABC procedures are called)
void   Abc_Start();
void   Abc_Stop();

// procedures to get the ABC framework and execute commands in it
typedef struct Abc_Frame_t_ Abc_Frame_t;

Abc_Frame_t * Abc_FrameGetGlobalFrame();
int    Cmd_CommandExecute( Abc_Frame_t * pAbc, const char * sCommand );

#if defined(ABC_NAMESPACE)
}
using namespace ABC_NAMESPACE;
#elif defined(__cplusplus)
}
#endif

////////////////////////////////////////////////////////////////////////
///                     FUNCTION DEFINITIONS                         ///
////////////////////////////////////////////////////////////////////////

/**Function*************************************************************

  Synopsis    [The main() procedure.]

  Description [This procedure compiles into a stand-alone program for 
  DAG-aware rewriting of the AIGs. A BLIF or PLA file to be considered
  for rewriting should be given as a command-line argument. Implementation 
  of the rewriting is inspired by the paper: Per Bjesse, Arne Boralv, 
  "DAG-aware circuit compression for formal verification", Proc. ICCAD 2004.]
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
int main( int argc, char * argv[] )
{
    // variables
    Abc_Frame_t * pAbc;
    char * inFile, *outFile;
    char Command[1000];
    int verify = 0, fraig = 0;
    time_t now;
    //////////////////////////////////////////////////////////////////////////
    // get the input file name
    if ( argc != 5 )
    {
        printf( "USAGE: clean <verify = 0/1> <fraig=0/1> <infile.aig> <outfile.aig>\n" );
        printf( "infile must be aig (binary) format not aag.\n" );
        printf( "Cleanup command destroys the symbol table. So cleanup part is commented out\n");
        return 1;
    }
    verify = atoi(argv[1]);
    fraig = atoi(argv[2]);
    inFile = argv[3];
    outFile = argv[4];
    //////////////////////////////////////////////////////////////////////////
    // start the ABC framework
    Abc_Start();
    pAbc = Abc_FrameGetGlobalFrame();
	 int fUseResyn2  = 0; // the other version optimizes too aggressively and failed to create
     // equivalent aig (cec command in abc). So fixed to 0.
    
	// read the file
    time(&now);
    printf("Reading input file at %s",ctime(&now));
    sprintf( Command, "read %s; strash; zero ", inFile);
    if ( Cmd_CommandExecute( pAbc, Command ) )
    {
        fprintf( stdout, "Cannot execute command \"%s\".\n", Command );
        return 1;
    }
   //print the numer of i/o latch and and gates.
    sprintf( Command, "print_stats " );
    if ( Cmd_CommandExecute( pAbc, Command ) )
    {
        fprintf( stdout, "Cannot execute command \"%s\".\n", Command );
        return 1;
    }
    //////////////////////////////////////////////////////////////////////////
    // balance and cleanup
    time(&now);
    printf("starting Balancing at %s",ctime(&now));
     sprintf( Command, "balance" );
    if ( Cmd_CommandExecute( pAbc, Command ) )
    {
        fprintf( stdout, "Cannot execute command \"%s\".\n", Command );
        return 1;
    }

// Remove dangling nodes
/* The next few commands try to reduce the size of the network, remove dangling nodes, refactor and fraig the network */
/*    sprintf( Command, "scleanup -ce " );
    if ( Cmd_CommandExecute( pAbc, Command ) )
    {
        fprintf( stdout, "Cannot execute command \"%s\".\n", Command );
        return 1;
    }
*/  /*
    time(&now);
    printf("starting cleanup at %s",ctime(&now));
    sprintf( Command, "cleanup " );
    if ( Cmd_CommandExecute( pAbc, Command ) )
    {
        fprintf( stdout, "Cannot execute command \"%s\".\n", Command );
        return 1;
    }
    */
    if ( fUseResyn2 ) //copied from demo.c file
    {   
        printf("WARNING: Unexpectedly starting Resyn2 which gave inequivalent output aig on intel013.aig benchmark.");
        printf(" Proceed with caution.\n");
        sprintf( Command, "balance; rewrite -l; refactor -l; balance; rewrite -l; rewrite -lz; balance; refactor -lz; rewrite -lz; balance" );
        if ( Cmd_CommandExecute( pAbc, Command ) )
        {
            fprintf( stdout, "Cannot execute command \"%s\".\n", Command );
            return 1;
        }
    }
    else
    {
        time(&now);
        printf("starting resyn at %s",ctime(&now));
        sprintf( Command, "balance; rewrite -l; rewrite -lz; balance; rewrite -lz; balance" );
        if ( Cmd_CommandExecute( pAbc, Command ) )
       {
            fprintf( stdout, "Cannot execute command \"%s\".\n", Command );
            return 1;
        }
    }
    if (fraig !=0){
        time(&now);
        printf("argument 'fraig' was non zero. starting fraig at %s",ctime(&now));
        sprintf( Command, "fraig " );
        if ( Cmd_CommandExecute( pAbc, Command ) )
        {
        fprintf( stdout, "Cannot execute command \"%s\".\n", Command );
            return 1;
        }
    }
    /*
    time(&now);
    printf("starting cleanup at %s",ctime(&now));
    sprintf( Command, "cleanup " );
    if ( Cmd_CommandExecute( pAbc, Command ) )
    {
        fprintf( stdout, "Cannot execute command \"%s\".\n", Command );
        return 1;
    }*/
    sprintf( Command, "print_stats " );
    if ( Cmd_CommandExecute( pAbc, Command ) )
    {
        fprintf( stdout, "Cannot execute command \"%s\".\n", Command );
        return 1;
    }
    time(&now);
    printf ("Successfully read and preprocessed the input specification at %s",ctime(&now));
    //////////////////////////////////////////////////////////////////////////
    sprintf( Command, "write_aiger -s %s",outFile);
    if ( Cmd_CommandExecute( pAbc, Command ) )
    {
       fprintf( stdout, "Cannot execute command \"%s\".\n", Command );
        return 1;
    }
    if (verify!=0){
    time(&now);
    printf("argument 'verify' not equal to 0. Checking equivalence at %s",ctime(&now));
    sprintf( Command, "cec %s %s",inFile, outFile );
        if ( Cmd_CommandExecute( pAbc, Command ) )
        {
            fprintf( stdout, "Cannot execute command \"%s\".\n", Command );
            return 1;
        }
    }
    time(&now);
    printf("Done! at %s",ctime(&now));
    //////////////////////////////////////////////////////////////////////////
    // stop the ABC framework
    Abc_Stop();
    return 0;
}

