import sys, os, runOnAll

inDir = sys.argv[1]
outDir = sys.argv[2]
aiger = os.environ['AIGER']

cmd = [aiger+'/aigtoaig']
runOnAll.runOnAll(inDir,'aig',cmd,1,outDir,'aag',2,'aig2aag.stdout')