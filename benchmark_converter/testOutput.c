#include <sys/types.h>
#include <sys/time.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include "cudd.h"
#include "dddmp.h"
#include "dddmpInt.h"

typedef struct dddmpVarInfo {
  /*
   *  Local Information
   */

  int nDdVars;       /* Local Manager Number of Variables */
  char **rootNames;  /* Root Names */

  /*
   *  Header File Information
   */

  Dddmp_DecompType ddType;

  int nVars;      /* File Manager Number of Variables */
  int nSuppVars;  /* File Structure Number of Variables */

  int varNamesFlagUpdate;      /* 0 to NOT Update */
  char **suppVarNames;
  char **orderedVarNames;

  int varIdsFlagUpdate;        /* 0 to NOT Update */
  int *varIds;                 /* File ids - nSuppVars size */
  int *varIdsAll;              /* ALL ids - nVars size */

  int varComposeIdsFlagUpdate; /* 0 to NOT Update */
  int *varComposeIds;          /* File permids - nSuppVars size */
  int *varComposeIdsAll;       /* ALL permids - nVars size */

  int varAuxIdsFlagUpdate;     /* 0 to NOT Update */
  int *varAuxIds;              /* File auxids - nSuppVars size */
  int *varAuxIdsAll;           /* ALL auxids  - nVars size */

  int nRoots;
} dddmpVarInfo_t;


/**
 * Print a dd summary
 * pr = 0 : prints nothing
 * pr = 1 : prints counts of nodes and minterms
 * pr = 2 : prints counts + disjoint sum of product
 * pr = 3 : prints counts + list of nodes
 * pr > 3 : prints counts + disjoint sum of product + list of nodes
 * @param the dd node
 */
void print_dd (DdManager *gbm, DdNode *dd, int n, int pr )
{
    printf("DdManager nodes: %ld | ", Cudd_ReadNodeCount(gbm)); /*Reports the number of live nodes in BDDs and ADDs*/
    printf("DdManager vars: %d | ", Cudd_ReadSize(gbm) ); /*Returns the number of BDD variables in existence*/
    printf("DdManager reorderings: %d | ", Cudd_ReadReorderings(gbm) ); /*Returns the number of times reordering has occurred*/
    printf("DdManager memory: %ld \n", Cudd_ReadMemoryInUse(gbm) ); /*Returns the memory in use by the manager measured in bytes*/
    Cudd_PrintDebug(gbm, dd, n, pr);  // Prints to the standard output a DD and its statistics: number of nodes, number of leaves, number of minterms.
}


/**
 * Writes a dot file representing the argument DDs
 * @param the node object
 */
void write_dd ()
{
    DdManager *gbm = Cudd_Init(4,0,CUDD_UNIQUE_SLOTS,CUDD_CACHE_SLOTS,0);
    DdNode** dd = (DdNode**) malloc(sizeof(DdNode*)*4);
    for (int i = 0; i<4;i++){
        dd[i] = Cudd_bddNewVar(gbm);
        Cudd_Ref(dd[i]);
    }
    DdNode* xdd = Cudd_bddXor(gbm,dd[0],dd[1]);
    Cudd_Ref(xdd);
    xdd = Cudd_bddXor(gbm,xdd,dd[2]);
    Cudd_Ref(xdd);
    xdd = Cudd_bddXor(gbm,xdd,dd[3]);
    char* filename = "xor.dot";
    FILE *outfile; // output file pointer for .dot file
    outfile = fopen(filename,"w");
	DdNode **ddnodearray = (DdNode**)malloc(sizeof(DdNode*)); // initialize the function array
    ddnodearray[0] = xdd;
	printf("Writing dot file..\n");
    DdNode * bAdd = Cudd_BddToAdd( gbm, xdd ); Cudd_Ref( bAdd );
    Cudd_DumpDot( gbm, 1, (DdNode **)&bAdd, NULL, NULL, outfile );
    Cudd_RecursiveDeref( gbm, bAdd );
	printf("Written dot file.\n");
	free(ddnodearray);
    fclose (outfile); // close the file */
}

void printPerm(DdManager* dd){
	printf("\nPerm:\n");
    for (int i = 0; i<dd->size; i++){
        Cudd_ReadPerm(dd,i);
        printf("%d ",dd->perm[i]);
    }
    printf("\n");
    printf("InvPerm:\n");
    for (int i = 0; i<dd->size; i++){
        printf("%d ",dd->invperm[i]);
    }
    printf("\nVar IDs:\n");
    for (int i = 0; i<dd->size; i++){
        printf("%d ",dd->vars[i]->index);
    }
    printf("\n");
}

// This program creates a single BDD variable
int main (int argc, char *argv[])
{   //write_dd();
    //exit(0);
    if (argc!=4){
        printf("USAGE: testOutput <mode> <in.bdd> <out.dot>\n");
        printf("MODE: A for Ascii B for binary\n");
        exit(1);
    }
    char * inFile, *outFile;
    int mode = argv[1][0];
    switch (mode){
        case 'a': mode = 'A';
        case 'A':
            break;
        case 'b': mode = 'B';
        case 'B':
            break;
        default:
            printf("Warning: BDD format not recognized. Setting to binary by default.\n");
            break;
    }
    inFile = argv[2];
    outFile = argv[3];
    printf("Opening file %s..\n", inFile);
	FILE* fp = fopen(inFile,"r");
    printf("File opened.\n");
	printf("Reading files now..\n");
    Dddmp_DecompType ddType;
    int retValue, nRoots, nVars, nSuppVars;
    int *tmpVarIds = NULL;
    int *tmpVarAuxIds = NULL;
    int *tmpVarComposeIds = NULL;
    char **tmpOrderedVarNames = NULL;
    char **tmpSuppVarNames = NULL;

    retValue = Dddmp_cuddHeaderLoad (&ddType, &nVars, &nSuppVars,
    &tmpSuppVarNames, &tmpOrderedVarNames, &tmpVarIds, &tmpVarComposeIds,
    &tmpVarAuxIds, &nRoots, inFile, fp);
    fclose(fp);
    fp = fopen(inFile,"r");
    if(retValue == 0){
        printf("Error reading header of file. Exiting..\n");
        exit(1);
    }
    else{
        printf("Read header of file. COnstructing bdd..\n");
    }
	DdManager *gbm = Cudd_Init(nVars,0,CUDD_UNIQUE_SLOTS,CUDD_CACHE_SLOTS,0); /* Initialize a new BDD manager. */
    printf("Initial permutation before reading the bdd..\n");
    printPerm(gbm);
    DdNode *bdd = NULL;
	dddmpVarInfo_t varInfo;
	Dddmp_VarMatchType varmatchmode = DDDMP_VAR_MATCHPERMIDS;
    bdd = Dddmp_cuddBddLoad (gbm, varmatchmode, varInfo.orderedVarNames,
    varInfo.varIdsAll, varInfo.varComposeIdsAll, mode,
    inFile, fp);
    if (bdd==NULL) {
        fprintf (stderr, "Dddmp Test Error : %s is not loaded from file\n",
            inFile);
    }
    FILE* fpo = fopen(outFile,"w");
    DdNode * bAdd = Cudd_BddToAdd( gbm, bdd ); Cudd_Ref( bAdd );
    printf("Writing dot file.. %s\n",outFile);
    if (bAdd==NULL) {
        printf("the converted add is NULL. Attempting to write the the bdd instead..\n");
        Cudd_DumpDot( gbm, 1, (DdNode **)&bdd, tmpSuppVarNames, NULL, fpo);
    }
    else{
        printf("the converted add is not NULL. Writing it..\n");
        //Cudd_DumpDot( gbm, 1, (DdNode **)&bAdd, tmpSuppVarNames, NULL, fpo);
        Cudd_DumpDot( gbm, 1, (DdNode **)&bAdd, NULL, NULL, fpo);
    }
  
    Cudd_RecursiveDeref( gbm, bdd );

    fclose(fpo);
    printf("\n\nPermutation after reading the bdd..\n");
    printPerm(gbm);
    return 0;
}

void test(){
	DdManager *gbm; /* Global BDD manager. */
    char filename[30];
    DdNode *bdd;
	
	gbm = Cudd_Init(0,0,CUDD_UNIQUE_SLOTS,CUDD_CACHE_SLOTS,0); /* Initialize a new BDD manager. */
    bdd = Cudd_bddNewVar(gbm); /*Create a new BDD variable*/
    Cudd_Ref(bdd); /*Increases the reference count of a node*/
    bdd = Cudd_BddToAdd(gbm, bdd); /*Convert BDD to ADD for display purpose*/
    print_dd (gbm, bdd, 2,4); /*Print the dd to standard output*/
    sprintf(filename, "graph.dot"); /*Write .dot filename to a string*/
    write_dd(gbm, bdd, filename);  /*Write the resulting cascade dd to a file*/
    Cudd_Quit(gbm);
}