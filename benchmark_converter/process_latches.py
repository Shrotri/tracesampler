import sys, os

inFname = sys.argv[1]
outFname = sys.argv[2]

def process_ands(andlist, newolit, outS, newm, newa):
	if len(andlist) == 2:
		newa += 1
		s = str(newolit)+' '+str(andlist[0])+ ' ' + str(andlist[1])
		outS.append(s+'\n')
		return newm,newa
	newandlist = []
	for ind in range(0,2*(len(andlist)/2),2):
		newm += 1
		newa += 1
		#print 'ind:',ind,' len andlist:',len(andlist)
		s = str(2*newm)+' '+str(andlist[ind])+ ' ' + str(andlist[ind+1])
		newandlist.append(2*newm)
		outS.append(s+'\n')
	if len(andlist)%2 == 1:
		newandlist.append(andlist[len(andlist)-1])
	return process_ands(newandlist,newolit,outS,newm,newa)		

def add_xnors(var1,var2,outS,newm,newa):
	newm += 1
	newa += 1
	s = str(2*newm)+' '+str(var1)+ ' ' + str(var2)
	outS.append(s+'\n')
	
	newm += 1
	newa += 1
	if var1 % 2 == 0:
		var1 += 1
	else:
		var1 -= 1
	if var2 % 2 == 0:
		var2 += 1
	else:
		var2 -= 1
	s = str(2*newm)+' '+str(var1)+ ' ' + str(var2)
	outS.append(s+'\n')

	newm += 1
	newa += 1
	s = str(2*newm)+' '+str(2*(newm-1)+1)+ ' ' + str(2*(newm-2)+1)
	outS.append(s+'\n')
	return 2*newm, newm, newa

def process_xnors(xnorlist,newolit,outS, newm, newa):
	andlist = []
	for ind in range(len(xnorlist)):
		newlit, newm, newa = add_xnors(xnorlist[ind][0],xnorlist[ind][1],outS,newm,newa)
		andlist.append(newlit+1)
	newm, newa = process_ands(andlist, newolit, outS, newm, newa)
	return newm, newa

inF = open(inFname,'r')
try:
	#print [int(el) if el!='aag' else el for el in inF.readline().split()]
	tag, m,i,l,o,a = (int(el) if el!='aag' else el for el in inF.readline().split())
except:
	print 'Header should have 6 elements starting with aag. Exiting..',tag, m,i,l,o,a
	sys.exit(1)
if l==0:
		print "Number of latches is 0 (not a sequential circuit). Exiting.."
		sys.exit(1)
if tag != 'aag':
	print 'Input file should be in text format with header aag. Exiting..'
	sys.exit(1)

newo = 1
newi = i + l
ilist = []
llist = []
olist = []
ovarlist = []
alist = []
newilist = []
newolist = [2*(m + 1)]
newa = a
newm = m + 1
for ind in range(i):
	ilist.append(int(inF.readline()))
for ind in range(l):
	llist.append(inF.readline().split())
exitIfPONotFound = True
for ind in range(o):
	olit = int(inF.readline())
	if olit == 0:
		print "Warning: PO has literal 0. Not exiting if PO not found in and gates.\n"
		exitIfPONotFound = False
	olist.append(olit)
	ovarlist.append(olist[-1]/2)
outputsFound = 0
for ind in range(a):
	ands = inF.readline().split()
	if int(ands[0])/2 in ovarlist:
		outputsFound += 1
		newa -= 1
		continue
	else:
		alist.append(' '.join(ands))
if outputsFound != o:
	print "Warning: not all POs found in and gates. OutputsFound:",outputsFound," numPOs:",o,"\n"
	if exitIfPONotFound == True:
		print " Exiting..\n"
		sys.exit(1)
lstline = inF.readline()
if len(lstline) != 0:
	print "Warning: some lines left in file after reading and gates:\n"
	print lstline
	if (lstline[0]!='c'):
		print "Line doesnt start with c (comment). Exiting..\n"
		sys.exit(1)

inF.close()
outS = []

for el in ilist:
	outS.append(str(el)+'\n')
for ind in range(i,newi):
	newm += 1
	newilist.append(newm*2)
	outS.append(str(2*newm)+'\n')

xnorlist = []
ind = 0
for el in llist:
	outS.append(el[0]+' '+el[1]+'\n')
	xnorlist.append([newilist[ind],int(el[1])])
	ind += 1
#print 'newilist:', newilist
#print 'xnorlist:', xnorlist
outS.append(str(newolist[0])+'\n')

for el in alist:
	outS.append(el+'\n')
print 'Constructing additional xors..',
newm, newa= process_xnors(xnorlist,newolist[0],outS, newm, newa)

print 'Constructing symbol table..',
#original inputs
for ind in range(i):
	outS.append('i'+str(ind)+' '+'origin'+str(ind)+'\n')
#new inputs (latch ins)
for ind in range(l):
	outS.append('i'+str(ind+i)+' '+'newin'+str(ind)+'\n')
#original latches
for ind in range(l):
	outS.append('l'+str(ind)+' '+'origlatch'+str(ind)+'\n')
#new output
outS.append('o0 newout0\n')

print ' Writing to file..',
oF = open(outFname,'w')
oF.write(' '.join(['aag',str(newm),str(newi),str(l),str(1),str(newa)])+'\n')
for el in outS:
	oF.write(el)
oF.close()
print ' Done writing to '+outFname+'!'