import os, sys, subprocess

def runOnAll(inDir, inExt, cmdList, inFIndex, outDir, outExt, outFIndex, cmdOutFile, skip=False):
	i = 1
	for d, subds, fnames in os.walk(inDir):
		for fname in fnames:
			if not fname.endswith('.'+inExt):
				continue
			fpath = os.path.join(d,fname)
			cmd = list(cmdList)
			cmd.insert(inFIndex,fpath)
			ofname = fname.split('.')[0]+'.'+outExt
			ofpath = os.path.join(outDir,ofname)
			if skip:
				if os.path.isfile(ofpath):
					print 'Not executing command ',cmd, ' as output file already exists.'
					continue
			cmd.insert(outFIndex,ofpath)
			print str(i)+': Executing command: ',' '.join(cmd)
			p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			out, err = p.communicate()
			f = open(cmdOutFile,'a')
			f.write('\n\n======================================\n')
			f.write('File: '+fpath+'\n')
			f.write('Out: \n')
			f.write(out)
			f.write('Err: \n')
			f.write(err)
			f.close()
			i += 1