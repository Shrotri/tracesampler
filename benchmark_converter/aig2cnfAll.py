import sys, os, runOnAll

inDir = sys.argv[1]
cnfDir = sys.argv[2]
checkDir = sys.argv[3]
outDir = sys.argv[4]
t = int(sys.argv[5])
i = 0
for tL in range (1,9):
	cmd = ['timeout',str(t),'../aig2dd',None,None,None,'2','21'+str(2**tL),'0','1','>',None,'2>&1']
	for root, subdirs, files in os.walk(inDir):
		for fl in files:
			cmdc = list(cmd)
			cmdc[3] = root+'/'+fl
			relP = os.path.relpath(root,inDir)
			cmdc[4] = cnfDir+'/'+relP+'/'+fl+'_'+str(2**tL)+'.cnf'
			cmdc[5] = checkDir+'/'+relP+'/'+fl
			cmdc[11] = outDir+'/'+relP+'/'+fl+'_'+str(2**tL)+'.out'
			cmdcs = ' '.join(cmdc)
			i += 1
			if i%10==2:
				print i,': Executing ',cmdcs
			os.system(cmdcs)