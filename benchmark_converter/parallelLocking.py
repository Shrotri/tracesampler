import sys,os, errno, time, fcntl as F


def acquire_lock(lock_file_path, mode, wait_time, sleep_interval):
    """
    try to acquire the lock, if the unable to get it, wait for sleep_interval
    secs, and retry. Until time out or get the locker.
    """
    times = int(wait_time/sleep_interval)
    for _ in range(times):
        try:
            lock_file = open(lock_file_path, mode)
            F.lockf(lock_file.fileno(), F.LOCK_EX|F.LOCK_NB)
        except IOError:
            print 'Another task is processing the current directory, waiting..'
            time.sleep(sleep_interval)
        else:
            #print 'Lock acquired'
            return lock_file
    print 'Failed to acquire lock within ',wait_time,' time'
    raise RuntimeError('Time out > %s' % wait_time) 

