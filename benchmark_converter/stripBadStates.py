import sys,os

iFile = sys.argv[1]
oFile = sys.argv[2]

inF = open(iFile,'r')
hdr = [int(el) if el!='aag' else el for el in inF.readline().split()]

if hdr[0] != 'aag':
	print 'Input file should be in text format with header aag. Exiting..'
	sys.exit(1)

if len(hdr) == 6:
	print 'File already in old format i.e. has 6 elements in header. Not making changes..'
	oF = open(oFile,'w')
	oF.write(' '.join(hdr)+'\n')
	for line in inF:
		oF.write(line)
	oF.close()
	inF.close()
	sys.exit(1)
elif len(hdr) == 7:
	oF = open(oFile,'w')
	hdrn = list(hdr[:6])
	newHdr = ' '.join(str(v) for v in hdrn)
	print newHdr
	oF.write(newHdr+'\n')
	for i in range (hdr[2]): #0 is aag, 1 is M, 2 is I
		oF.write(inF.readline())
	for i in range (hdr[3]): #3 is L
		oF.write(inF.readline())
	for i in range (hdr[4]): #4 is O
		oF.write(inF.readline())
	for i in range (hdr[6]): #6 is B
		inF.readline()
	for i in range (hdr[5]): #5 is A
		oF.write(inF.readline())
	oF.close()
	inF.close()	
else:	
	print 'Header should have at most 7 elements. This file had ',len(hdr),' elements. Exiting'
	sys.exit(1)	