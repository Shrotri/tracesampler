import sys, os, pysat.formula as pf, pysat.solvers as ps

cnfFName = sys.argv[1]
sampleFName = sys.argv[2]
numSamples = int(sys.argv[3])

cf = pf.CNF(from_file=cnfFName)
sf = open(sampleFName,'r')

sfHeader = sf.readline().split()
origFname, traceLength, nStateVars, numTraces = (sfHeader[i] if i==0 else int(sfHeader[i]) for i in range(len(sfHeader)))

print (traceLength+1)*nStateVars,cf.nv
#following assertion wont hold true for comparing against cnf with aux variables (from aig2dd). So commenting it out
#assert((traceLength+1)*nStateVars >= cf.nv) # >= and not == since nv is the largest var appearing in formula and not the number in the header.

i = 0
unsatList = []
for line in sf:
	i += 1
	if i>numSamples:
		break
	cfc = cf.copy()
	ls = line.split()
	sample_clauses = [[(-1)**(1-int(ls[j]))*(j+1)] for j in range(len(ls))] 
	cfc.extend(sample_clauses)
	with ps.Glucose3(cfc) as G:
		res = G.solve()
	if res==False:
		unsatList.append(i)

if len(unsatList) == 0:
	print 'Formula satisfied by all samples'
else:
	print '====================================================================================='
	print 'WARNING WARNING: Formula not satisfied by ',len(unsatList),'clauses.'
	print '====================================================================================='