import sys, os, math
import numpy
from scipy import stats
import pylab as pl

bddfpath = sys.argv[1]
bddmetafpath = sys.argv[2]
traceLength = int(sys.argv[3])
numTraces = int(sys.argv[4])
useReachableSet = int(sys.argv[5])
skipTo = None
skipList = [False]*5
if len(sys.argv) >= 7:
	skipTo = int(sys.argv[6])
	for i in range(min(4,skipTo)):
		skipList[i] = True
assert(os.environ.has_key('TMP'))
assert(os.environ.has_key('STSAMPLE'))
assert(os.environ.has_key('WAPS'))

bddfname = os.path.basename(bddfpath)
bddSampleFpath = os.environ['TMP']+'/'+bddfname+'.ddsamples'
cnffpath = os.environ['TMP']+'/'+bddfname+'.cnf'
cnfSampleFpath = os.environ['TMP']+'/'+bddfname+'.cnfsamples'

#generate samples from dd
if not skipList[0]:
	cmd = os.environ['STSAMPLE']+'/sample_trace '+bddfpath+' '+bddmetafpath+' '+str(traceLength)+' 1 '+str(numTraces)+' '+str(useReachableSet)+' '+bddSampleFpath
	print 'Generating samples from decision diagram:\n'+cmd
	os.system(cmd)

#create cnf
if not skipList[1]:
	if useReachableSet==3:
		useReachableSet=2
	cmd = os.environ['STSAMPLE']+'/dd2cnf '+bddfpath+' '+bddmetafpath+' '+cnffpath+' '+str(traceLength)+' '+str(useReachableSet)
	print 'Generating CNF file..\n'+cmd
	os.system(cmd)

#gen samples from cnf
if not skipList[2]: 
	cmd = 'python '+ os.environ['WAPS']+'/waps.py '+cnffpath+' --outputfile '+cnfSampleFpath+' --samples '+str(numTraces)
	print 'Generating CNF samples..\n'+cmd
	os.system(cmd)

#check dd samples against cnf
if not skipList[3]:
	cmd = 'python '+os.environ['STSAMPLE']+'/experiments/checkDDSamplesOnCNF.py '+ cnffpath + ' '+ bddSampleFpath + ' 2000'
	print 'Checking DD samples against CNF:\n'+cmd
	os.system(cmd)

if skipTo==6:
	print 'Skip to argument is 6. So assuming bdd and meta bdd files are actually sample files and computing their distributions..'
	bddSampleFpath = bddfpath
	cnfSampleFpath = bddmetafpath

def processSampleTraceSampleFile(sf_):
	bddmodels={}
	sfHeader = sf_.readline().split()
	origFname, traceLength, nStateVars, numTraces = (sfHeader[i] if i==0 else int(sfHeader[i]) for i in range(len(sfHeader)))
	for line in sf_:
		model = ''.join(line.split())
		if model in bddmodels:
			bddmodels[model] += 1
		else:
			bddmodels[model] = 1
	sf_.close()
	return bddmodels

def processWAPSSampleFile(cf_):
	cnfmodels = {}
	i = 1
	for line in cf_:
		ls = line.split()
		assert(ls[0]==str(i)+',')
		model = ''.join('0' if int(ls[j])==-j else '1' if int(ls[j])==j else None for j in range(1,len(ls)))
		if model in cnfmodels:
			cnfmodels[model] += 1
		else:
			cnfmodels[model] = 1
		i += 1
	cf_.close()
	return cnfmodels

#compute model counts of dd samples 
sf = open(bddSampleFpath,'r')
bddmodels = None
firstline = sf.readline()
if (firstline.startswith('1,')):
	sf.seek(0,0)
	bddmodels = processWAPSSampleFile(sf)
else:
	sf.seek(0,0)
	bddmodels = processSampleTraceSampleFile(sf)


#compute model counts of cnf samples
cf = open(cnfSampleFpath,'r')
cnfmodels = None
firstline = cf.readline()
if (firstline.startswith('1,')):
	cf.seek(0,0)
	cnfmodels = processWAPSSampleFile(cf)
else:
	cf.seek(0,0)
	cnfmodels = processSampleTraceSampleFile(cf)

#compute frequencies
sk = bddmodels.keys()
print len(sk),' models found in bdd samples'
ck = cnfmodels.keys()
print len(ck),' models found in cnf samples'
print 'Example models:\n'
print 'BDD:',sk[0],'\n',sk[1]
print '\nCNF:',ck[0],'\n',ck[1]
s1 = set(sk)
s2 = set(ck)
s3 = s2.union(s1)
print len(s3),' models found in union'

counts = [[0,0]]
for key in s3:
	#for bdd
	if key not in sk:
		counts[0][0] += 1
	else:
		freq = bddmodels[key]
		while len(counts) < freq + 1:
			counts.append([0,0])
		counts[freq][0] += 1
	#for cnf
	if key not in ck:
		counts[0][1] += 1
	else:
		freq = cnfmodels[key]
		while len(counts) < freq + 1:
			counts.append([0,0])
		counts[freq][1] += 1
'''
#Switched counts[i][0] and counts[i][1] to test if computed Jensen Shannon is symmetric
for key in s3:
	#for bdd
	if key not in sk:
		counts[0][1] += 1
	else:
		freq = bddmodels[key]
		while len(counts) < freq + 1:
			counts.append([0,0])
		counts[freq][1] += 1
	#for cnf
	if key not in ck:
		counts[0][0] += 1
	else:
		freq = cnfmodels[key]
		while len(counts) < freq + 1:
			counts.append([0,0])
		counts[freq][0] += 1
'''
# normalize to get probs
sum0 = sum(counts[i][0] for i in range(len(counts))) + 0.0
sum1 = sum(counts[i][1] for i in range(len(counts))) + 0.0
#print 'Sum0:',sum0,'Sum1:',sum1
#datapoints check
dp1 = sum(i*counts[i][0] for i in range(len(counts)))
dp2 = sum(i*counts[i][1] for i in range(len(counts)))

print "number of datapoints1:",dp1,"datapoints2:",dp2,"numTraces:",numTraces

for i in range(len(counts)):
	counts[i][0] = counts[i][0] / sum0
	counts[i][1] = counts[i][1] / sum1
#compute jensen shannon
# calculate the kl divergence
def js_divergence(p):
	return                                           0.5*sum(p[i][0] * math.log(p[i][0]/((p[i][0]+p[i][1])/2.0),2) 
	if p[i][0] != 0 else 0 for i in range(len(p))) + 0.5*sum(p[i][1] * math.log(p[i][1]/((p[i][0]+p[i][1])/2.0),2) 
	if p[i][1] != 0 else 0 for i in range(len(p)))
print 'Jensen Shannon Distance is',js_divergence(counts),'bits'

#plot graphs
colors=['green','blue','brown','orange']
mkrs= ['o','^','*','s','D']
patterns = ('-', '\\\\', 'o', 'x', '*', '+', '.', 'O')
mss=10
bar_width = 1.0

#lb = 70
#ub = 170
lb=0
ub=len(counts)
counts0 = [sum0*counts[i][0] for i in range(lb,ub)]
counts1 = [sum1*counts[i][1] for i in range(lb,ub)]
p1 = pl.scatter([i for i in range(lb,ub)],counts0,color='g',marker='x')
p2 = pl.scatter([i for i in range(lb,ub)],counts1,color='r',marker='+')
pl.rcParams['pdf.fonttype'] = 42
pl.rcParams['ps.fonttype'] = 42			
pl.legend((p1,p2),('Trace Sampler', 'Ideal Sampler'),loc='best',fontsize=12)
pl.tick_params(labelsize=16)
#pl.ylim(0,505)
#pl.xlim(0,505)
pl.xlabel('Count', fontsize=20)
pl.ylabel('# of Solutions', fontsize=20)
#pl.yscale('log')
#pl.xscale('log')
#pl.title('Dense Matrices', fontsize=24)
#pl.plot(pl.xlim(), pl.ylim(),color='r', ls="--")
#pl.savefig('graphs/dense_scatter.eps',dpi=fig.dpi,bbox_inches='tight')
pl.savefig('experiments/graphs/scatter.eps',bbox_inches='tight')
pl.show()
